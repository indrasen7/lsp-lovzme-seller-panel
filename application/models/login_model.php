<?php
	class login_model extends CI_Model
	{	
	    // This function is used to seller login	
		public function login($data)
		{
			$this->db->select('seller.business_email,seller.seller_customer_id,seller.id_seller,customer.passwd,customer.active');
            $this->db->from('ps_wk_mp_seller as seller');
			$this->db->join('ps_customer as customer', 'seller.seller_customer_id = customer.id_customer');
			$this->db->where(array('business_email' => $data['business_email']));			
			$query = $this->db->get();
			
			if ($query->num_rows() == 0)
			{
				return false;
			}
			else
			{
				//Compare the password attempt with the password we have stored.
			$result = $query->row_array();
			$validPassword = password_verify($data['password'], $result['passwd']);
				if($validPassword)
				{
					return $result = $query->row_array();
				}				
			}
		}
	}

?>