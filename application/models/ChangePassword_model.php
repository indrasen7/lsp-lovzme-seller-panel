<?php
	class ChangePassword_model extends CI_Model
	{
				
		//Get the seller name form database table
		public function getSeller($seller_customer_id)
		{
			$this->db->where('seller_customer_id',$seller_customer_id);
			$this->db->select('CONCAT(seller_firstname," "'.' , seller_lastname) AS name');
			$this->db->from('ps_wk_mp_seller');
			$query = $this->db->get();
			return $query->result()[0]->name;
		}

		//Get the seller gender form database table
		public function getGender($id_customer)
		{
			$this->db->where('id_customer',$id_customer);
			$this->db->select('id_gender');
			$this->db->from('ps_customer');
			$query = $this->db->get();
			$data =$query->result();
			//print_r($data);
			return $data[0]->id_gender;
		}

        //This function is used to match seller password for change password
		public function matchOldPassword($seller_id,$oldPassword)
		{   
			$this->db->select('customer.id_customer,seller.seller_customer_id,customer.passwd');
            $this->db->from('ps_wk_mp_seller as seller');
			$this->db->join('ps_customer as customer', 'seller.seller_customer_id = customer.id_customer');
			$this->db->where(array('id_customer' => $seller_id));			
			$query = $this->db->get();
			$result = $query->result();

			if ($query->num_rows() == 0)
			{
				return false;
			}
			else
			{
			//Compare the password attempt with the password we have stored.
			$result = $query->row_array();
			$validPassword = password_verify($oldPassword, $result['passwd']);
		    if($validPassword)
			{
			    return $result = $query->row_array();
			}				
			}
		}
				
	    //Match with the current seller name with id
		public function ChangePassword($seller_id,$data)
		{
			$this->db->where('id_customer', $seller_id);
			$this->db->update('ps_customer', $data);
			return true;
		}

	}
 