<div class="content">
    <section class="content-header">
      <h1>
      <i class="fa fa-key"></i> Change Password
      <small>Set new password for your account</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>Dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Change Password</li>
      </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-10">
              <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Details</h3>
                    </div><!-- /.box-header -->
                     <div class="col-md-9">
              					<?php if(validation_errors() !== ''): ?>
                                     <div class="alert alert-warning alert-dismissible">
              						  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>   					 
              						  <?= validation_errors();?>
              					  </div>
              					<?php endif; ?>
                        <?php if($this->session->flashdata('success')): ?>
                          <div class="alert alert-danger" style="border-color: #fff;">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <?=$this->session->flashdata('success')?>
                          </div>
                        <?php endif; ?>
                        <?php if($this->session->flashdata('warning')): ?>
                          <div class="alert alert-warning">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <?=$this->session->flashdata('warning')?>
                          </div>
                        <?php endif; ?>
                     </div>
                    <!-- form start -->
                    <?php echo form_open(base_url('ChangePassword'));  ?> 
                        <div class="box-body">
                        <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="oldpassword">Old Password</label>
                                        <input type="password" class="form-control" id="oldPassword"   name="oldPassword" placeholder="Old password" maxlength="10" required>
                                    </div>
                                </div>
                            </div>
                            <hr/>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="newpassword">New Password</label>
                                        <input type="password" class="form-control" id="newpassword"  name="newpassword" placeholder="New password" maxlength="10" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="inputPassword2">Confirm New Password</label>
                                        <input type="password" class="form-control" id="confirm_pwd"  name="confirm_pwd"  placeholder="Confirm new password" maxlength="10" required>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" name="submit" id="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
 $("#ChangePassword").addClass('active');
</script>