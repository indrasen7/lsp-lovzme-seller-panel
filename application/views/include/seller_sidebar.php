<?php 
$cur_tab = $this->uri->segment(1)==''?'dashboard': $this->uri->segment(1);  
?>  
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
                <?php 
                  if ($seller_gender==1) 
                  {
                    echo ' <img  class="img-circle" src=" '.base_url().'assets/dist/img/male.png"/>';
                  } 
                  else 
                  {
                    echo ' <img  class="img-circle" src=" '.base_url().'assets/dist/img/girl.png"/>';
                  }
                ?> 
        </div>
        <div class="pull-left info">     
             <p><?=$seller_name;?></p>                    
            <a><i class="fa fa-circle text-success"></i>Online</a>
        </div>
      </div>
	   <!-- search form -->
      <form  class="sidebar-form">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" class="btn btn-flat">
                  <i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <ul class="sidebar-menu">
  		 <li class="treeview"><a href="<?= base_url('dashboard'); ?>"><i class="fa fa-home"></i><span>Dashboard</span></i></a></li>
			 <li class="treeview"><a href="<?= base_url('sellers'); ?>"><i class="fa fa-user"></i><span>Profile</span></a></li>
			 <li class="treeview"><a href="<?= base_url('shop'); ?>"><i class="fa fa-shopping-cart"></i><span>Shop</span></a></li>
			 <li class="treeview"><a href="<?= base_url('products'); ?>"><i class="fa fa-delicious"></i><span>Product</span></a></li>
			 <li class="treeview"><a href="<?= base_url('transaction'); ?>"><i class="fa fa-refresh fa-spin fa-fw"></i><span>Order & Transaction</span></a></li>       
		  <!--  <li class="treeview"><a href=""><i class="fa  fa-inr"></i><span>Payment Details</span></a></li> -->
       <li class="treeview"><a href="<?= base_url('sales');?>"><i class="fa fa-bar-chart"></i><span>Sales Report</span></a></li>
		   <li class="treeview"><a href="<?= base_url('remittance');?>"><i class="fa fa-fw fa-google-wallet"></i><span>Remittance</span></a></li>
       <li class="treeview"><a href="<?= base_url('stock');?>"><i class="fa fa-line-chart"></i><span>Update Stock</span></a></li>
       <li class="treeview"><a href="<?= base_url('discount');?>"><i class="fa fa-align-center"></i><span>Update Discount</span></a></li>
        <!-- <li class="treeview"><a href="<?= base_url('backup');?>"><i class="fa fa-hdd-o"></i><span>Database Backup</span></a></li> -->
      </ul>     
    </section>
    <!-- /.sidebar -->
<script>
  $("#<?= $cur_tab ?>").addClass('active');
</script>
