  <header class="main-header">
    <!-- Logo -->
	      <a href="<?php echo base_url(); ?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>LSP</b></span>
          <!-- logo for regular state and mobile devices -->
           <span class="logo-lg"><img src="<?php echo base_url(); ?>assets/images/lovzme-logo-1518760531.png" style="vertical-align: inherit";/></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                     <?php 
                        if ($seller_gender==1) 
                        {
                          echo ' <img  class="user-image" src=" '.base_url().'assets/dist/img/male.png" alt="User Image"/>';
                        } 
                        else 
                        {
                          echo ' <img  class="user-image" src=" '.base_url().'assets/dist/img/girl.png" alt="User Image"/>';
                        }
                      ?> 
                   <span  class="hidden-xs"><?=$seller_name;?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header" style="border: 10px solid aliceblue;" >
                     <?php 
                      if ($seller_gender==1) 
                        {
                          echo ' <img  class="img-circle" src=" '.base_url().'assets/dist/img/male.png" alt="User Image"/>';
                        } 
                        else 
                        {
                          echo ' <img  class="img-circle" src=" '.base_url().'assets/dist/img/girl.png" alt="User Image"/>';
                        }
                      ?> 
                    <p><?=$seller_name;?></p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?= base_url('ChangePassword'); ?>" class="btn btn-default btn-flat"><i class="fa fa-key"></i> Change Password</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?= site_url('logout'); ?>" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>				
  </header>
 