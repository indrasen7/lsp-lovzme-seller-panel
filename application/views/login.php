<!DOCTYPE html>
<html lang="en">
    <head>
          <title><?=isset($title)?$title:'LOVZme | Seller Log in' ?></title>
          <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
          <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
          <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
          <link href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
          <!-- Favicon  -->
          <link rel="icon" href="<?= base_url() ?>assets/images/favicon.ico" type="image/gif">           
    </head>
	 <body class="login-page">
    <div class="login-box">
      <div class="login-logo">
        <img src="<?php echo base_url(); ?>assets/images/lovzme-logo-1518760531.png"/><br>Seller Panel
      </div><!-- /.login-logo -->
      <div class="login-box-body">
       <br/>
                   <?php if(isset($msg) || validation_errors() !== ''): ?>
                    <div class="alert alert-warning alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?= validation_errors();?>
                        <?= isset($msg)? $msg: ''; ?>
                    </div>
                    <?php endif; ?>
                    <?php if($this->session->flashdata('warning')): ?>
                          <div class="alert alert-warning">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                          <?=$this->session->flashdata('warning')?>
                        </div>
                    <?php endif; ?>
                    <?php if($this->session->flashdata('success')): ?>
                          <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            <?=$this->session->flashdata('success')?>
                          </div>
                    <?php endif; ?>                   
        
                    <?php echo form_open(base_url('login')); ?>
                        <div class="form-group has-feedback">
                          <input type="email" class="form-control" placeholder="Email" name="business_email" id="business_email" required />
                          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                          <input type="password" class="form-control" placeholder="Password" name="password" id="password" required />
                          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                        <div class="row">
                          <div class="col-xs-8">                                 
                          </div>
                          <div class="col-xs-4">
                            <input type="submit"  name="submit" id="submit" class="btn btn-primary btn-block btn-flat" style="background-color: #ff75ab;border-color: #ff4f94;"value="Sign In" />
                          </div><!-- /.col -->
                        </div>
                    <?php echo form_close(); ?>
                      
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <script src="<?php echo base_url(); ?>assets/js/jQuery-2.1.4.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
  </body> 
</html>