<!DOCTYPE html>
<html lang="en">
	<head>
		  <title><?=isset($title)?$title:'Lovzme - Sellers Panel' ?></title>
		  <!-- Tell the browser to be responsive to screen width -->
		  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">		 
		  <!-- Bootstrap 3.3.6 -->
		  <link href="<?= base_url() ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" /> 
		  <!-- Font Awesome -->
		  <!-- FontAwesome 4.3.0 -->
          <link href="<?= base_url() ?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		  <!-- Ionicons 2.0.0 -->
          <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
		  <!-- Theme style -->
          <link href="<?= base_url() ?>assets/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
		  <!-- DataTables -->
          <link rel="<?php echo base_url(); ?>assets/js/datatables.net-bs/css/dataTables.bootstrap.min.css">   		  
		  <!-- AdminLTE Skins. Choose a skin from the css/skins
			   folder instead of downloading all of them to reduce the load. -->
		   <link href="<?= base_url() ?>assets/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
		  <!-- jQuery 2.1.4 -->
          <script src="<?= base_url() ?>assets/js/jQuery-2.1.4.min.js"></script>          		 
          <!-- Favicon  -->
           <link rel="icon" href="<?= base_url() ?>assets/images/logo.gif" type="image/gif">		
	</head>
	<body class="skin-blue sidebar-mini">
		<div class="wrapper">
			<?php if($this->session->flashdata('msg') != ''): ?>
			    <div class="alert alert-warning flash-msg alert-dismissible">
			      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			      <h4> Success!</h4>
			      <?= $this->session->flashdata('msg'); ?> 
			    </div>
			<?php endif; ?>

				<!--header start-->
				<header class="main-header">
					<?php include('include/navbar.php'); ?>
				</header>
				<!--header end-->
				<!--sidebar start-->				
				<aside class="main-sidebar">					
					<?php include('include/seller_sidebar.php'); ?>
				</aside>							
				<!--sidebar end-->
				<!--main content start-->				
				<div class="content-wrapper">
					<!-- page start-->
					<?php $this->load->view($view);?>
					<!-- page end-->
				</div>									
				<!--main content end-->					
                 <!--footer start-->
				<footer class="main-footer">
					 <div class="pull-right hidden-xs">
                       <b>LOVZme</b>
                     </div>
                     <strong>Copyright &copy; 2019 <a href="https://www.lovzme.com/" target="_blank">LOVZme</a>.</strong> All rights reserved.
				</footer>
				<!--footer end-->				
		</div>
		       						
  	<!-- Bootstrap 3.3.2 JS -->
    <script src="<?= base_url() ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/dist/js/app.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/js/jquery.validate.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/js/validation.js" type="text/javascript"></script>
   
    <script type="text/javascript">
        var windowURL = window.location.href;
        pageURL = windowURL.substring(0, windowURL.lastIndexOf('/'));
        var x= $('a[href="'+pageURL+'"]');
            x.addClass('active');
            x.parent().addClass('active');
        var y= $('a[href="'+windowURL+'"]');
            y.addClass('active');
            y.parent().addClass('active');
    </script>

	<!-- page script -->
	<script type="text/javascript">
	  $(".flash-msg").fadeTo(2000, 500).slideUp(500, function(){
	    $(".flash-msg").slideUp(500);
	});
	</script>

	</body>
</html>