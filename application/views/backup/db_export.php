<section class="content">
	<div class="row">
	    <div class="col-md-12">
	      <div class="box box-body with-border">
	      	<h3><i class="fa fa-hdd-o fa-2x" aria-hidden="true"></i> Backup database</h3><hr/>
			<p>Use this section to backup the database of your online store data.</p>
			<p><b>Note</b> - Database backup procedure can take up to several minutes.</p>
			
	      	<a href="<?= base_url('backup/dbexport'); ?>" class="btn btn-primary"><img src="<?=base_url();?>assets/images/shopbackup.svg"> &nbsp; Download & Create Backup</a>  
	      </div>
	  </div>
	</div>
</section>

<script>
    $("#export").addClass('active');
</script>
