<?php defined('BASEPATH') OR exit('No direct script access allowed');

class login extends CI_Controller
{	  	
        // This is default constructor of the class        
		public function __construct()
		{
			parent::__construct();
			$this->load->model('login_model');
		}
				          
        //Index Page for this controller.
		public function index()
		{			
                $this->is_seller_login();			
		}	
		         
        // This function used to check the seller is logged in or not
		public function is_seller_login()
		{
			$is_seller_login = $this->session->userdata('is_seller_login');
			
			if(!isset($is_seller_login) || $is_seller_login != TRUE)
			{
				$this->login();
			}
			else
			{
				redirect('/Dashboard');
			}
		}
	
		// This function used to logged in seller
		public function login()
		{
			if($this->input->post('submit'))
			{
							$this->form_validation->set_rules('business_email', 'business_email', 'trim|required|valid_email');
							$this->form_validation->set_rules('password', 'Password', 'trim|required|max_length[32]');

				if ($this->form_validation->run() == FALSE) 
				{
					        $this->index();
				}
				else 
				{
							$data = array(
									'business_email' => strip_tags($this->input->post('business_email')),
									'password' => strip_tags($this->input->post('password'))
				                         );
					$result = $this->login_model->login($data);
				
					if($result['active'] == 1)
					{
							$admin_data = array('active'             => $result['active'],
												'business_email'     => $result['business_email'],
												'seller_customer_id' => $result['seller_customer_id'],
												'id_seller'          => $result['id_seller'],
												'is_seller_login'     => TRUE
							                   );
							$this->session->set_userdata($admin_data);
							redirect('Dashboard');
							
					}																	
					else
					{
						$data['msg'] = 'Invalid Username or Password!';
						$this->load->view('login', $data);
					}
				}
			}
			else
			{
				$this->load->view('login');
			}
		}
		
		 //This function is used to logged out seller from system
		 public function logout()
		 {  
			//   $session_items = array('business_email', 'seller_customer_id');
			 $this->session->sess_destroy();        
			 redirect('login');
		 }	
			
	}  // end class
?>