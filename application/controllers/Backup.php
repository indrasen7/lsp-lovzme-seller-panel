<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class Backup extends MY_Controller 
	{				
		public function __construct()
		{
			parent::__construct();
        	$this->load->helper('download');
			$this->load->library('zip');
			$this->load->model('Backup_model');
		}

		public function index()
		{   	
			$seller_customer_id     = $this->session->userdata('seller_customer_id');
						   		
			$data['title']          = 'Backup Data';
			$data['seller_name']    =  $this->Backup_model->getSeller($seller_customer_id);
			$data['seller_gender'] 	= $this->Backup_model->getGender($seller_customer_id);
			$data['view']           = 'backup/db_export';

			$this->load->view('layout', $data);
		}

		public function dbexport()
		{

			$this->load->dbutil();
			$db_format = array(
								'ignore' => array($this->ignore_directories),
								'format'=> 'zip',
								'filename'=> 'my_db_backup.sql',
								'add_insert' => TRUE,
								'newline' => "\n"
			                  );
			$backup = & $this->dbutil->backup($db_format);
			$dbname = 'Backup-on-'.date('Y-m-d').'.zip';
			$save = 'uploads/db_backup/'.$dbname;
			write_file($save, $backup);
			force_download($dbname, $backup);
		}

	}

?>		