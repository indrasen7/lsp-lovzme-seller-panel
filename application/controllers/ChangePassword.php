<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ChangePassword extends MY_Controller 
{   
	// This is default constructor of the class
	public function __construct()
	{
					parent::__construct();
					$this->load->model('ChangePassword_model');
					$this->is_seller_login();
	}

	//Index Page for this controller.
	public function index()	
	{		
				    $seller_customer_id = $this->session->userdata('seller_customer_id');

				    $data['title']            = 'LOVZme : Change Password';
					$data['seller_name']      = $this->ChangePassword_model->getSeller($seller_customer_id);
					$data['seller_gender']    = $this->ChangePassword_model->getGender($seller_customer_id);		
					$data['view']             = 'ChangePassword';

				    $this->load->view('layout', $data);	
	}
	
    // This function used to check the seller is logged in and change pass with session id
	public function is_seller_login()
		{
			$is_seller_login = $this->session->userdata('is_seller_login');
			
			if(!isset($is_seller_login) || $is_seller_login != TRUE)
			{
				    $this->login();
			}
			else
			{				
			        $this->sellerId = $this->session->userdata( 'seller_customer_id' );			   
			}
		}
 
	//This function is used to change the password of the Seller
	public function changeSellerPassword()
	{ 
		if($this->input->post('submit'))
		{        
				    $this->form_validation->set_rules('oldPassword', 'Old Password', 'trim|required|max_length[30]');
				    $this->form_validation->set_rules('newpassword', 'New Password', 'trim|required|max_length[30]');
					$this->form_validation->set_rules('confirm_pwd','Confirm Password','trim|required|matches[newpassword]|max_length[20]');
				
			if ($this->form_validation->run() == FALSE) 
			{
				    $this->index();
			}
			else
			{      
					$oldPassword = $this->input->post('oldPassword');
					$newPassword = $this->input->post('newpassword');
					$seller_id = $this->sellerId;

				    $resultPas = $this->ChangePassword_model->matchOldPassword($seller_id,$oldPassword);
				
				if(empty($resultPas))
				{
					$this->session->set_flashdata('warning', 'Your old password not correct');
					$this->index();
				}
				else
				{
					$options = ['cost' => 10];
					$data = array(
									'passwd'=>password_hash($newPassword, PASSWORD_BCRYPT , $options),
								  	'date_upd'=>date('Y-m-d H:i:s')
								);
					
					$result = $this->ChangePassword_model->changePassword($seller_id, $data);
					
					if($result > 0)
					{    
						$this->session->set_flashdata('success', 'Password has been changed successfully!');
					}
					else
					{ 
						$this->session->set_flashdata('warning', 'Password change failed'); 
					}
						$this->index();				    
				}
			}
		}
		else
		{
			$this->index();
		}
	}
}

?>	