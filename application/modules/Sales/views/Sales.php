<!-- Main content -->
    <section class="content-header">
      <h1><i class="fa fa-bar-chart"></i>
        Sales Report
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>Dashboard"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Sales</li>
      </ol>     
    </section>
    <section class="content">
                <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs">
                    <li class="active"><a href="#settings" data-toggle="tab">Select Date </a></li>                   
                  </ul>
                  <div class="tab-content">
                    <div class="active tab-pane" id="settings">
                      <div class="box-body">
                         <div class="col-md-3">
                            <div class="form-group">
                              <div class="input-group">
                                <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control datepicker" placeholder="Date FROM" name="startRDate" id="startRDate">
                              </div>
                            </div>
                          </div>
                          <div class="col-md-3">
                              <div class="form-group">
                               <div class="input-group">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>          
                                  <input type="text" class="form-control datepicker" placeholder="Date TO" name="endRDate" id="endRDate">
                                </div>
                              </div>
                          </div>
                          <div class="col-md-2">
                              <div class="form-group">
                                  <button type="button" id="salesdatecustom" class="btn btn-primary">Export Sales Report</button>
                              </div>
                          </div>
                       </div>
                    </div>                           
                  </div>                 
                </div>                            
                                       
              <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
              <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  
              <script>
                $(function() {
                    $( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
                    $("#salesdatecustom").click(function() {                    
                          var startD = $("input[name=startRDate]").datepicker({ dateFormat: 'yy-mm-dd' }).val();
                          var endD   = $("input[name=endRDate]").datepicker({ dateFormat: 'yy-mm-dd' }).val(); 
                    $.ajax({

                                  url:'<?= base_url() ?>/Sales/salescustomdate',
                                  data:{                                               
                                            startRDate : startD,
                                            endRDate   : endD
                                        },
                                  method:"GET",
                                  success:function(data)
                                  {
                                       
                                          window.open('Sales/salescustomdate?startRDate='+startD+'&endRDate='+endD);
                                        
                                  }                                     
                            });
                      });
                });
              </script>            
          </div>
        </div>
      </div>
    </section>
<script>
  $("#Sales").addClass('active');
</script>
