<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Sales extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Sales_model');
    }

    public function index()
    {
            $seller_customer_id = $this->session->userdata('seller_customer_id');
                 
            $data['title']             = 'LOVZme : Sales Report';
            $data['view']              = 'Sales';          
            $data['customer_name']     = $this->Sales_model->get_customer_name($seller_customer_id)->customer_name;
            $data['seller_name']       = $this->Sales_model->getSeller($seller_customer_id);
            $data['seller_gender']     = $this->Sales_model->getGender($seller_customer_id);
                                                     
            $this->load->view('layout', $data);
    }
    
    // Export order data in CSV format 
    public function salescustomdate()
    {
           
           $startRDate = $this->input->get('startRDate');
           $endRDate   = $this->input->get('endRDate');   

           $filename = 'Sales_'.$startRDate.'_To_'.$endRDate.'.csv'; 
           header('Content-Description: File Transfer'); 
           header('Content-Type: application/csv');
           header('Content-Type: application/force-download; charset=UTF-8');
           header('Content-Disposition: attachment; filename='.$filename);           
           header('Cache-Control: no-store, no-cache');

           $id_customer_seller = $this->session->userdata('seller_customer_id');
           
           // get data 
           $seller_data = $this->Sales_model->getSalesDatecustom($startRDate,$endRDate,$id_customer_seller);

           // file creation 
           $file = fopen('php://output', 'w');
         
           $header = array("ORDER ID",
                            "ORDER REFERENCE",
                            "ORDER DATE:TIME",
                            "INVOICE No",
                            "PAYMENT METHOD",
                            "PAYMENT REFERENCE",
                            "ORDER STATUS",
                            "FIRST NAME",
                            "LAST NAME",
                            "CUSTOMER EMAIL",
                            "CUSTOMER PHONE",
                            "ADDRESS 1",
                            "ADDRESS 2",
                            "CITY",
                            "STATE",
                            "PINCODE",
                            "PRODUCT ID",
                            "PRODUCT SKU(Parent)",
                            "CHILD SKU",
                            "PRODUCT NAME",
                            "QUANTITY",
                            "SELLING PRICE (Tax Exl)",
                            "SELLING PRICE (Tax Incl.)",
                            "TOTAL SHIPPING",
                            "REFUNDED QUANTITY",
                            "REFUNDED AMOUNT",
                            "SELLER ID",
                            "SELLER",
                            "SHIPMENT PROVIDER",
                            "AWB No.",
                            "PICKUP DATE",
                            "DELIVERY DATE",
                            "OFFER NAME",
                            "DISCOUNT VALUE",
                            "TAX TOTAL",
                            "TAX APPLIED"
                          );

           fputcsv($file, $header);

           foreach ($seller_data as $key=>$line)
           { 
             fputcsv($file,$line); 
           }
           fclose($file); 
           exit; 
       
    }

}
