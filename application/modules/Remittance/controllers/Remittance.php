<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Remittance extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        //$this->load->helper('download');
        $this->load->model('Remittance_model');
    }

    public function index()
    {
            $seller_customer_id = $this->session->userdata('seller_customer_id');
                 
            $data['title']             = 'LOVZme : Remittance';
            $data['view']              = 'remittance';          
            $data['customer_name']     = $this->Remittance_model->get_customer_name($seller_customer_id)->customer_name;
            $data['seller_name']       = $this->Remittance_model->getSeller($seller_customer_id);
            $data['seller_gender']     = $this->Remittance_model->getGender($seller_customer_id);
                                                     
            $this->load->view('layout', $data);
    }
    
    // Export order data in CSV format 
    public function remittancecustomdate()
    {
            //print_r($_POST);exit;
          //print_r($this->input->post() );
           $startRDate = $this->input->get('startRDate');
           $endRDate   = $this->input->get('endRDate');   

           $filename = 'Remittance_'.$startRDate.'_To_'.$endRDate.'.csv'; 
           header('Content-Description: File Transfer'); 
           header('Content-Type: application/csv');
           header('Content-Type: application/force-download; charset=UTF-8');
           header('Content-Disposition: attachment; filename='.$filename);           
           header('Cache-Control: no-store, no-cache');
           
           // get data 
           $seller_data = $this->Remittance_model->getRemittanceDatecustom($startRDate,$endRDate);

           // file creation 
           $file = fopen('php://output', 'w');
         
           $header = array("ID Order", "Order Reference", "Order Date", "Invoice No", "Payment Method", "Payment Reference", "Order Status" , "Customer Firstname" ,"Customer Lastname" ,"Customer Email" ,"Customer Phone","Address1","Address2","Customer City","Customer State","Pincode","Product ID","Product Reference","Child Reference","Product Name","Product Quantity","Product Selling Priceexl","Product Selling Priceinc","Totalshipping","PGCollectable Value","Refundedquantity","Refundedamount","Sellercustomerid","Sellerfirstname","Shipmentprovider","AWB No","Pickupdate","Deliverydate","Offername","Discountvalue","Taxtotal","Taxname","GSTrate","Admincommission","Netadmincommission","Grosscollectablevalue","Netcollectablevalue","GSTvalue","TCSvalue","Payabletovendor");

           fputcsv($file, $header);

           foreach ($seller_data as $key=>$line)
           { 
             fputcsv($file,$line); 
           }
           fclose($file); 
           exit; 
       
    }

}
