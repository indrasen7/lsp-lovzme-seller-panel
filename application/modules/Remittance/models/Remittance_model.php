<?php
	class Remittance_model extends CI_Model
	{		
		public function __construct()
		{
			parent::__construct();
		}

		//Get the seller name form database table
		public function getSeller($seller_customer_id)
		{
			$this->db->where('seller_customer_id',$seller_customer_id);
			$this->db->select('CONCAT(seller_firstname," "'.' , seller_lastname) AS name');
			$this->db->from('ps_wk_mp_seller');
			$query = $this->db->get();
			return $query->result()[0]->name;
		}

		//Get the seller gender form database table
		public function getGender($id_customer)
		{
			$this->db->where('id_customer',$id_customer);
			$this->db->select('id_gender');
			$this->db->from('ps_customer');
			$query = $this->db->get();
			return $query->result()[0]->id_gender;
		}

		//Get the Seller name from database   				
		public function get_customer_name($seller_customer_id)
		{
			$sql ="SELECT DISTINCT
			            
			             COUNT(CONCAT(cu.firstname,' ', cu.lastname)) AS customer_name
			            
					FROM
						ps_orders o
					LEFT JOIN ps_wk_mp_seller_order_history soh ON 
					     o.id_order = soh.id_order
                    LEFT JOIN ps_customer cu ON 
                     	 o.id_customer = cu.id_customer
					LEFT JOIN ps_order_state_lang osl ON  
						 o.current_state = osl.id_order_state 
					LEFT JOIN ps_wk_mp_seller wms ON 
						 wms.id_seller = soh.id_seller
			        WHERE 
			             wms.seller_customer_id = ".$seller_customer_id." ";
			 
			 $query = $this->db->query($sql);		
			 $data = $query->result();
			 return $data[0];
		}

		//Get the RemittanceDate Custom of Order (From date to date)
	    public function getRemittanceDatecustom($startRDate,$endRDate)
	    {
	        //print_r($startDate);
	       $sqlOrderDetails = "SELECT
						        o.`id_order`,
						        o.`reference` as orderreference,
						        o.`date_add` as orderdate,
						        o.`invoice_number` as invoiceno,
						        o.`payment` as paymentmethod,
						        op.`transaction_id` as paymentreference,
						        ods.`name` as orderstatus,
						        odc.`firstname` as customerfirstname,
						        odc.`lastname` as customerlastname,
						        odc.`email` as customeremail,
						        oda.`phone` as customerphone,
						        oda.`address1` as address1,
						        oda.`address2` as address2,
						        oda.`city` as customercity,
						        psos.`name` as customerstate,
						        oda.`postcode` as pincode,
						        od.`product_id` productid,
						        odp.`reference` as productreference,
						        pspa.`reference` as childreference,
						        od.`product_name` as productname,
						        od.`product_quantity` as productquantity,
						        od.`total_price_tax_excl` as productsellingpriceexl,
						        od.`total_price_tax_incl` as productsellingpriceinc,
						        od.`total_shipping_price_tax_incl` * od.`product_quantity` as totalshipping,
						        op.`amount` as pgcollectablevalue,
						        od.`product_quantity_refunded` as refundedquantity,
						        od.`product_quantity_refunded`*od.`unit_price_tax_incl` as refundedamount,
						        odsl.`seller_customer_id` as sellercustomerid,
						        odsl.`shop_name_unique` as sellerfirstname,
						        odmf.`shipment_provider` as shipmentprovider,
						        psocc.`tracking_number` as awbno,
						        odmf.`pickup_date` as pickupdate,
						        o.`delivery_date` as deliverydate,
						        psocr.`name` as offername,
						        CASE 
						            WHEN pscr.`reduction_tax`<1 
						            THEN ((od.`total_price_tax_incl`- psmpsod.`price_ti`)/(1+((od.`total_price_tax_incl`-od.`total_price_tax_excl`)/od.`total_price_tax_excl`))) 
						            ELSE ROUND(od.`total_price_tax_incl`- psmpsod.`price_ti`,2) 
						        END as discountvalue,
						        od.`total_price_tax_incl`-od.`total_price_tax_excl` as taxtotal,
						        GROUP_CONCAT(pstl.`name`) as taxname,
						        ROUND(((od.`total_price_tax_incl`- od.`total_price_tax_excl`)/(od.`total_price_tax_excl`))*100) as gstrate,
						        ROUND(psmpsod.`admin_commission`/psmpsod.`price_te`*100) as admincommission,
						        (ROUND(psmpsod.`admin_commission`/psmpsod.`price_te`*100)*(od.`total_price_tax_incl`- od.`product_quantity_refunded`*od.`unit_price_tax_incl`))/100 as netadmincommission,
						        (CASE
						            WHEN pscr.`reduction_tax`<1
						            THEN (od.`total_price_tax_incl`)-(((od.`total_price_tax_incl`- psmpsod.`price_ti`)/(1+((od.`total_price_tax_incl`-od.`total_price_tax_excl`)/od.`total_price_tax_excl`))))+(od.`total_shipping_price_tax_incl` * od.`product_quantity`)
						            WHEN pscr.`reduction_tax`=1
						            THEN (od.`total_price_tax_incl`)-(ROUND(od.`total_price_tax_incl`- psmpsod.`price_ti`,2))+(od.`total_shipping_price_tax_incl` * od.`product_quantity`)
						            ELSE (od.`total_price_tax_incl`)-(ROUND(od.`total_price_tax_incl`- psmpsod.`price_ti`,2))+(od.`total_shipping_price_tax_incl` * od.`product_quantity`)
						        END) as grosscollectablevalue,
						        (CASE 
						            WHEN pscr.`reduction_tax`<1 AND od.`product_quantity_refunded`=1 
						            THEN (od.`total_price_tax_incl`- od.`product_quantity_refunded`*od.`unit_price_tax_incl`)-(((((od.`total_price_tax_incl`- psmpsod.`price_ti`)/(1+((od.`total_price_tax_incl`-od.`total_price_tax_excl`)/od.`total_price_tax_excl`)))))-(((((od.`total_price_tax_incl`- psmpsod.`price_ti`)/(1+((od.`total_price_tax_incl`-od.`total_price_tax_excl`)/od.`total_price_tax_excl`))))))/od.`product_quantity`*od.`product_quantity_refunded`)+((od.`total_shipping_price_tax_incl` * od.`product_quantity`)-(od.`total_shipping_price_tax_incl`*od.`product_quantity_refunded`))
						            WHEN pscr.`reduction_tax`=1 AND od.`product_quantity_refunded`=0 
						            THEN (od.`total_price_tax_incl`- od.`product_quantity_refunded`*od.`unit_price_tax_incl`)-(ROUND(od.`total_price_tax_incl`- psmpsod.`price_ti`,2))+(od.`total_shipping_price_tax_incl` * od.`product_quantity`)
						            WHEN pscr.`reduction_tax`<1 AND od.`product_quantity_refunded`=0 
						            THEN (od.`total_price_tax_incl`- od.`product_quantity_refunded`*od.`unit_price_tax_incl`)-(((od.`total_price_tax_incl`- psmpsod.`price_ti`)/(1+((od.`total_price_tax_incl`-od.`total_price_tax_excl`)/od.`total_price_tax_excl`))))+(od.`total_shipping_price_tax_incl` * od.`product_quantity`)
						            WHEN pscr.`reduction_tax`=1 AND od.`product_quantity_refunded`=1 
						            THEN (od.`total_price_tax_incl`- od.`product_quantity_refunded`*od.`unit_price_tax_incl`)-(((ROUND(od.`total_price_tax_incl`- psmpsod.`price_ti`,2)))-((ROUND(od.`total_price_tax_incl`- psmpsod.`price_ti`,2)/od.`product_quantity`*od.`product_quantity_refunded`)))+((od.`total_shipping_price_tax_incl` * od.`product_quantity`)-(od.`total_shipping_price_tax_incl`*od.`product_quantity_refunded`))
						            ELSE (od.`total_price_tax_incl`- od.`product_quantity_refunded`*od.`unit_price_tax_incl`)-(ROUND(od.`total_price_tax_incl`- psmpsod.`price_ti`,2))+(od.`total_shipping_price_tax_incl` * od.`product_quantity`)
						        END) as netcollectablevalue,
						        (ROUND(psmpsod.`admin_commission`/psmpsod.`price_te`*100)*(od.`total_price_tax_incl`- od.`product_quantity_refunded`*od.`unit_price_tax_incl`)/100)*0.18 as gstvalue,
						        CASE 
						            WHEN od.`product_quantity_refunded`>0 
						            THEN (od.`total_price_tax_excl`-((od.`total_price_tax_excl`/od.`product_quantity`)*od.`product_quantity_refunded`))*0.01 
						            ELSE od.`total_price_tax_excl`*0.01 
						        END as tcsvalue,
						        CASE 
						            WHEN od.`product_quantity_refunded`>0 
						            THEN (od.`total_price_tax_incl`-od.`product_quantity_refunded`*od.`unit_price_tax_incl`)-((ROUND(psmpsod.`admin_commission`/psmpsod.`price_te`*100)*(od.`total_price_tax_incl`-od.`product_quantity_refunded`*od.`unit_price_tax_incl`))/100)-(ROUND(psmpsod.`admin_commission`/psmpsod.`price_te`*100)*(od.`total_price_tax_incl`- od.`product_quantity_refunded`*od.`unit_price_tax_incl`)/100)*0.18-(od.`total_price_tax_excl`-((od.`total_price_tax_excl`/od.`product_quantity`)*od.`product_quantity_refunded`))*0.01 
						            ELSE (od.`total_price_tax_incl`-od.`product_quantity_refunded`*od.`unit_price_tax_incl`)-((ROUND(psmpsod.`admin_commission`/psmpsod.`price_te`*100)*(od.`total_price_tax_incl`-od.`product_quantity_refunded`*od.`unit_price_tax_incl`))/100)-(((ROUND(psmpsod.`admin_commission`/psmpsod.`price_te`*100)*od.`total_price_tax_incl`)/100)*0.18)-((od.`total_price_tax_excl`)*0.01) 
						        END as payabletovendor
						        FROM
						        `ps_orders` o
						        LEFT JOIN `ps_order_detail` od ON o.`id_order` = od.`id_order`
						        LEFT JOIN `ps_order_state_lang` ods ON o.`current_state` = ods.`id_order_state`
						        LEFT JOIN `ps_product` odp ON od.`product_id` = odp.`id_product`
						        LEFT JOIN `ps_customer` odc ON o.`id_customer` = odc.`id_customer`
						        LEFT JOIN `ps_address` oda ON o.`id_address_delivery` = oda.`id_address`
						        LEFT JOIN `ps_wk_mp_seller_product` odsp ON odp.`id_product` = odsp.`id_ps_product`
						        LEFT JOIN `ps_wk_mp_seller` odsl ON odsp.`id_seller` = odsl.`id_seller`
						        LEFT JOIN `ps_orders_manifests` odmf ON o.`id_order` = odmf.`order_id`
						        LEFT JOIN `ps_state` psos ON oda.`id_state` = psos.`id_state`
						        LEFT JOIN `ps_product_attribute` pspa ON od.`product_attribute_id` = pspa.`id_product_attribute`
						        LEFT JOIN `ps_order_payment` op ON o.`reference` = op.`order_reference`
						        LEFT JOIN `ps_order_cart_rule` psocr ON o.`id_order` = psocr.`id_order`
						        LEFT JOIN `ps_order_detail_tax` psodt ON od.`id_order_detail` = psodt.`id_order_detail`
						        LEFT JOIN `ps_tax_lang` pstl ON psodt.`id_tax` = pstl.`id_tax`
						        LEFT JOIN `ps_order_carrier` psocc ON o.`id_order` = psocc.`id_order`
						        LEFT JOIN `ps_wk_mp_seller_order_detail` psmpsod ON od.`id_order` = psmpsod.`id_order` AND od.`product_attribute_id` = psmpsod.`product_attribute_id`
						        LEFT JOIN `ps_cart_rule` pscr ON psocr.`id_cart_rule` = pscr.`id_cart_rule`
						        WHERE o.`date_add` BETWEEN '$startRDate' AND '$endRDate'
						        GROUP BY od.`id_order_detail`
						        ORDER BY o.`id_order`";
	        //print_r($sql);
	        $query = $this->db->query($sqlOrderDetails);
	        return $query->result_array();

	    }

       
		public function dbtest()
		{
			$query = $this->db->select('firstname')->get('ps_customer');
			$result = $query->row_array();
			$result =$this->db2->count_all_results('ps_orders');
			return $result;
		}
		
	}

?>
