<?php
	class Products_model extends CI_Model
	{
		
		//Get the seller name form database table
		public function getSeller($seller_customer_id)
		{
			$this->db->where('seller_customer_id',$seller_customer_id);
			$this->db->select('CONCAT(seller_firstname," "'.' , seller_lastname) AS name');
			$this->db->from('ps_wk_mp_seller');
			$query = $this->db->get();
			return $query->result()[0]->name;
		}
		//Get the seller gender form database table
		public function getGender($id_customer)
		{
			$this->db->where('id_customer',$id_customer);
			$this->db->select('id_gender');
			$this->db->from('ps_customer');
			$query = $this->db->get();
			return $query->result()[0]->id_gender;
		}
				
		//Get the seller Products table form database table
		public function getSellerProductsTable($id_seller)
		{
			$SQL = "SELECT  DISTINCT                 
						pl.id_product,
	                    CONCAT('http://',
	                        -- get the shop domain
	                        IFNULL(conf.`value`, 'www.lovzme.com'),
	                        -- the path to the pictures folder
	                        '/img/p/',
	                        -- now take all the digits separetly as MySQL doesn't support loops in SELECT statements
	                        -- assuming we have smaller image id;)
	                        IF(CHAR_LENGTH(pi.`id_image`) >= 5, 
	                            -- if we have 5 digits for the image id split each digit into url i.e folder directory by name of each digit
	                            CONCAT(
	                                -- take the first digit
	                                SUBSTRING(pi.`id_image`, -5, 1),
	                                -- add a slash
	                                '/'),
	                            ''),
	                        -- repeat for the next digits
	                        IF(CHAR_LENGTH(pi.`id_image`) >= 4, CONCAT(SUBSTRING(pi.`id_image`, -4, 1), '/'), ''),
	                        IF(CHAR_LENGTH(pi.`id_image`) >= 3, CONCAT(SUBSTRING(pi.`id_image`, -3, 1), '/'), ''),
	                        if(CHAR_LENGTH(pi.`id_image`) >= 2, CONCAT(SUBSTRING(pi.`id_image`, -2, 1), '/'), ''),
	                        IF(CHAR_LENGTH(pi.`id_image`) >= 1, CONCAT(SUBSTRING(pi.`id_image`, -1, 1), '/'), ''),
	                        -- add the image id
	                        pi.`id_image`,
	                        -- put the image extension
	                        '.jpg') AS product_image,

	                    pl.name  AS name,
	                    ROUND(p.price ,0)AS price,
						wsp.quantity AS quantity, 
						p.active 
                   
                FROM  ps_wk_mp_seller_product wsp
                
                LEFT JOIN ps_product p ON 
					  p.id_product = wsp.id_ps_product
                         
				LEFT JOIN ps_product_lang pl ON 
                      wsp.id_ps_product = pl.id_product	
                         
                LEFT JOIN ps_image_shop image_shop ON 
                      (image_shop.`id_product` = p.`id_product` 
                AND    image_shop.`cover` = 1 
                AND    image_shop.`id_shop` = 1)

                LEFT JOIN ps_image pi ON 
                      (p.`id_product` = pi.`id_product`) 

                LEFT JOIN ps_configuration conf ON 
                      (conf.`name` = 'PS_SHOP_DOMAIN')
                WHERE 
                      pi.cover = 1
			    AND
                      wsp.id_seller = ".$id_seller."
			    AND   
			          description is not NULL";
		       
            return $this->db->query($SQL); 			
		}

		//Get the seller Products Details form database table
		public function getSellerProductsby_id($id_product)
		{
			$SQL = "SELECT  DISTINCT                 
						pl.`id_product`,
						ROUND(p.width ,1) AS width, 
                        ROUND(p.height ,1) AS height,
                        ROUND(p.weight ,1) AS weight,
                        ROUND(p.depth ,1) AS depth,
	                    CONCAT('http://',
	                        -- get the shop domain
	                        IFNULL(conf.`value`, 'www.lovzme.com'),
	                        -- the path to the pictures folder
	                        '/img/p/',
	                        -- now take all the digits separetly as MySQL doesn't support loops in SELECT statements
	                        -- assuming we have smaller image id;)
	                        IF(CHAR_LENGTH(pi.`id_image`) >= 5, 
	                            -- if we have 5 digits for the image id split each digit into url i.e folder directory by name of each digit
	                            CONCAT(
	                                -- take the first digit
	                                SUBSTRING(pi.`id_image`, -5, 1),
	                                -- add a slash
	                                '/'),
	                            ''),
	                        -- repeat for the next digits
	                        IF(CHAR_LENGTH(pi.`id_image`) >= 4, CONCAT(SUBSTRING(pi.`id_image`, -4, 1), '/'), ''),
	                        IF(CHAR_LENGTH(pi.`id_image`) >= 3, CONCAT(SUBSTRING(pi.`id_image`, -3, 1), '/'), ''),
	                        if(CHAR_LENGTH(pi.`id_image`) >= 2, CONCAT(SUBSTRING(pi.`id_image`, -2, 1), '/'), ''),
	                        IF(CHAR_LENGTH(pi.`id_image`) >= 1, CONCAT(SUBSTRING(pi.`id_image`, -1, 1), '/'), ''),
	                        -- add the image id
	                        pi.`id_image`,
	                        -- put the image extension
	                        '.jpg') AS product_image,

	                    pl.`name`  AS name,
	                    pl.`description` AS description,
	                    ROUND(p.`price` ,0)AS price,
						wsp.`quantity` AS quantity, 
						p.`active` AS status
                   
                FROM  ps_wk_mp_seller_product wsp
                
                LEFT JOIN ps_product p ON 
					  p.`id_product` = wsp.`id_ps_product`
                         
				LEFT JOIN ps_product_lang pl ON 
                      wsp.`id_ps_product` = pl.`id_product`	
                         
                LEFT JOIN ps_image_shop image_shop ON 
                      (image_shop.`id_product` = p.`id_product` 
                AND    image_shop.`cover` = 1 
                AND    image_shop.`id_shop` = 1)

                LEFT JOIN ps_image pi ON 
                      (p.`id_product` = pi.`id_product`) 

                LEFT JOIN ps_configuration conf ON 
                      (conf.`name` = 'PS_SHOP_DOMAIN')
                WHERE 
                      pi.`cover` = 1
                AND
                      pl.`id_product` = ".$id_product."

			    AND   description is not NULL";
		
            $query = $this->db->query($SQL);
			$result = $query->result_array()[0]; 	
			return $result;		
		}

		//Get the Image Display in product details of each product	
		public function getImageList($id_product)
		{
			$sql = " SELECT
						GROUP_CONCAT(DISTINCT(CONCAT('http://',
						       IFNULL(conf.`value`, 'www.lovzme.com'),
						       '/img/p/',
						       IF(CHAR_LENGTH(pi.`id_image`) >= 5,
						           CONCAT(
						               SUBSTRING(pi.`id_image`, -5, 1),
						               '/'),
						           ''),
						       IF(CHAR_LENGTH(pi.`id_image`) >= 4, CONCAT(SUBSTRING(pi.`id_image`, -4, 1), '/'), ''),
						       IF(CHAR_LENGTH(pi.`id_image`) >= 3, CONCAT(SUBSTRING(pi.`id_image`, -3, 1), '/'), ''),
						       if(CHAR_LENGTH(pi.`id_image`) >= 2, CONCAT(SUBSTRING(pi.`id_image`, -2, 1), '/'), ''),
						       IF(CHAR_LENGTH(pi.`id_image`) >= 1, CONCAT(SUBSTRING(pi.`id_image`, -1, 1), '/'), ''),
						       pi.`id_image`,
						       '.jpg') )) AS product_images
						           
						       FROM  `ps_product` p
						       LEFT JOIN ps_product_attribute pa ON
						   (
						    p.`id_product` = pa.`id_product`
						   )
						       LEFT JOIN `ps_product_attribute_shop` product_attribute_shop
						            ON (p.`id_product` = product_attribute_shop.`id_product`
						            AND product_attribute_shop.`default_on` = 1
						            AND product_attribute_shop.`id_shop` = 1 )
						       
						       LEFT JOIN `ps_product_lang` pl
						        ON ( p.`id_product` = pl.`id_product`
						            AND pl.`id_lang` = 1 )

						       LEFT JOIN `ps_image_shop` image_shop            
						        ON (image_shop.`id_product` = p.`id_product`
						        AND image_shop.`cover` = 1
						        AND image_shop.`id_shop` = 1)
						       LEFT JOIN `ps_image_lang` il
						        ON (image_shop.`id_image` = il.`id_image`
						        AND il.`id_lang` = 1 )

						LEFT JOIN `ps_image` pi
						ON (p.`id_product` = pi.`id_product`)

						LEFT JOIN `ps_configuration` conf
						ON (conf.`name` = 'PS_SHOP_DOMAIN')

						LEFT JOIN ps_feature_product fp
						ON (p.`id_product` = fp.`id_product`)

						LEFT JOIN ps_image pi2
						ON ( p.`id_product` = pi2.`id_product` and pi2.`position` = 2 )
						               
						WHERE p.`id_product` =  ".$id_product."";

			$query = $this->db->query($sql);
			$result = $query->result_array();
	        $result = explode(",",$result[0]['product_images']);

	        $resultCustom = [];
	        $i = 0;

	        foreach ( $result as $key => $value ) 
	        {
	        	$resultCustom[] = $value;
	        	$i++;
	        }
	        // print_r($resultCustom);
			return $resultCustom;
		}

}

?>