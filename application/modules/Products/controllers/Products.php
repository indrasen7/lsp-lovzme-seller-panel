<?php

 defined('BASEPATH') OR exit('No direct script access allowed');
 ini_set('max_execution_time', 0); 
 ini_set('memory_limit','2048M');
	class Products extends MY_Controller 
	{

		public function __construct()
		{
			parent::__construct();
			$this->load->model('Products_model');
			$this->load->library('pagination'); // loaded codeigniter pagination liberary
			$this->load->library('datatable'); // loaded my custom 'serverside datatable' library					
        }
        
        public function index()
        {   
			$seller_customer_id     = $this->session->userdata('seller_customer_id');
			$id_seller              = $this->session->userdata('id_seller');
             
            $data['title']          =  'LOVZme : Products';
            $data['view']           =  'Products';
			$data['seller_name']    =   $this->Products_model->getSeller($seller_customer_id);
			$data['seller_gender']  =   $this->Products_model->getGender($seller_customer_id);
			$data['products']       =   $this->Products_model->getSellerProductsTable($id_seller);                       
    
            $this->load->view('layout', $data);
        }

        // Server-side processing Datatable Example
		public function product_datatableJson()
		{	 

			// Datatables Variables
		    $start = intval($this->input->get("start"));
		    $length = intval($this->input->get("length"));

			$id_seller  = $this->session->userdata('id_seller');
	   					   
			$records = $this->Products_model->getSellerProductsTable($id_seller);
			$data = array();
	        foreach ($records->result()  as $row) 
			{  
				    $data[]= array(
									$row->id_product,
									'<img style="border-radius:50%;margin:0 auto;width:70px;height:70px;padding:2px;border:1px solid #ccc;" src="'.$row->product_image.'">',
				                    $row->name,
				                    $row->price,
									$row->quantity,
				                    '<center><span style="background-color:#00a65ac2;border-color:#4cbb6c;border-radius:3px;color:#fff;font-size:12px;padding:3px 5px;"">Approved</span></center>',                                                               
									'<a title="View" class="view btn btn-sm  pull-left" href="'.base_url('/Products/details/').$row->id_product.'"> <i class="fa fa-eye fa-lg"></i></a>'
				                );
	        }

	         $output = array(
			                 "recordsTotal" => $records->num_rows(),
			                 "recordsFiltered" => $records->num_rows(),
			                 "data" => $data
		                    );

	        echo json_encode($output);
	        exit();							   
		}
        
        //Get the product details from product page
        public function details($id_product)
		{
			
			$seller_customer_id      = $this->session->userdata('seller_customer_id');

			$data['view']            = 'Productsdetails';  
			$data['seller_name']     = $this->Products_model->getSeller($seller_customer_id);
			$data['seller_gender']   = $this->Products_model->getGender($seller_customer_id);
			$data['product']         = $this->Products_model->getSellerProductsby_id($id_product);
			$data['productimage']    = $this->Products_model->getImageList($id_product); 
				
			$this->load->view('layout', $data);
	
	    }							

	}

?>