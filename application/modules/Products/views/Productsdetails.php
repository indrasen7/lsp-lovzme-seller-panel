    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><i class="fa fa-delicious"></i>
        Products Details
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('Dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url('Products');?>"><i class="fa fa-delicious"></i> Products</a></li>
        <li class="active"> Products Details</li>
      </ol>
    </section>

     <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-4">
          <!-- Profile Image -->
          <div class="box box-primary" style="border-top-color: #8997c7;">
            <div class="box-body box-profile">
              <div id="slide">
                 <div>
                  <img class="profile-user-img img-responsive" style="margin: 0 auto;width:250px;height:300px; border-radius: 50%;" src="<?= $productimage['0']?>"/>  
                 </div>
                 <div>
                  <img class="profile-user-img img-responsive" style="margin: 0 auto;width:250px;height:300px; border-radius: 50%;" src="<?= $productimage['1']?>"/>  
                 </div>
                 <div>
                  <img class="profile-user-img img-responsive" style="margin: 0 auto;width:250px;height:300px; border-radius: 50%;" src="<?= $productimage['2']?>"/>  
                 </div>
                 <div>
                  <img class="profile-user-img img-responsive" style="margin: 0 auto;width:250px;height:300px; border-radius: 50%;" src="<?= $productimage['3']?>"/>  
                 </div>
                  <div>
                  <img class="profile-user-img img-responsive" style="margin: 0 auto;width:250px;height:300px; border-radius: 50%;" src="<?= $productimage['4']?>"/>  
                 </div>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
           <div class="box box-primary" style="border-top-color: #8997c7;">
            <div class="box-header with-border">
              <h3 class="box-title">Dimensions</h3>
            </div>
            <div class="box-body">
              Width :
              <p class="text-muted"> <span class="label bg-teal"><?= $product['width']?> </span></p>
              <hr>
              Height :
             <p class="text-muted"> <span class="label bg-teal"><?= $product['height']?> </span></p>
              <hr>
              Depth :                   
             <p class="text-muted"> <span class="label bg-teal"><?= $product['depth']?> </span></p>   
              <hr>
              Weight :
               <p class="text-muted"> <span class="label bg-teal"><?= $product['weight']?> </span></p>
                  
            </div>
            <!-- /.box-body -->
            <!-- /.box-header -->
            <div class="box-body">
                    
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->  
        </div>
        <!-- /.col -->
        <div class="col-md-8">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a  href="#1" data-toggle="tab"><strong> Details</strong></a>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="settings">
                <div class="box-body">
                    <div class="col-md-6">
                        <strong><i class="fa fa-delicious"></i></strong> Name :
                         <p class="text-muted">   <?= $product['name']?></p>
                        <hr>
                        <strong><i class="fa fa-delicious"></i></strong> Product ID :
                        <p class="text-muted">  <?= $product['id_product']?> </p>
                        <br/>
                    </div>
                    <div class="col-md-6"> 
                        <strong><i class="fa fa-inr"></i></strong> Price : 
                        <p class="text-muted"><span class="label label-danger"><?= $product['price']?></span> </p>
                        <hr>
                        <strong><i class="fa fa-shopping-cart"></i></strong> Quantity :
                        <p class="text-muted"> <?= $product['quantity']?></p> 
                        <br/>
                    </div>             
                 </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">Description</h4>
                </div>
                    <div class="panel-body">
                      <table class="table profile__table">
                        <tbody>
                          <tr>
                            <td> <?= $product['description']?></td>                             
                          </tr>                                                     
                        </tbody>
                      </table>
                    </div>
              </div>

            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
   <script>
    $('#Productsdetails').addClass('active');
  </script>
            <style>
            #slide 
            { 
                margin: 0 auto; 
                position: relative; 
                width: 250px; 
                height: 300px; 
                padding: 1px; 
                border:6px solid #d2d6de;
                border-radius: 50%;
            }
            #slide > div 
            { 
                position: absolute;    
            }
            </style>

  <script type="text/javascript">
    
      $("#slide > div:gt(0)").hide();
      setInterval(function() 
      { 
        $('#slide > div:first')
          .fadeOut(1000)
          .next()
          .fadeIn(1000)
          .end()
          .appendTo('#slide');
      },  5000);
      
  </script>