     <!-- Content Header (Page header) -->
		<section class="content-header">
		  <h1>
		  <i class="fa fa-delicious"></i> Products
		  </h1>
		  <ol class="breadcrumb">
			<li><a href="<?php echo base_url();?>Dashboard"><i class="fa fa-home"></i> Home</a></li>
			<li class="active">Products</li>
		  </ol>
		</section>       
      <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap.css">    
                <section class="content">                   
                    <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped table-hover"">
                      <thead>
                          <tr>
                            <th>ID</th>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Total</th>                                                            
                            <th>Quantity</th>
                            <th>Status</th>                                                                          
                            <th>Action</th> 
                          </tr>
                      </thead>                        
                    </table>
                    </div>
                  <!-- /.box-body -->
                </section>  
                 <!-- DataTables -->
               <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
                <script src="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>          
               <script type="text/javascript">                   
                        $('#example1').DataTable({
                            "lengthMenu": [10, 25],
                            "pageLength" : 10,
                            "ajax": "<?=base_url('Products/product_datatableJson') ?>",
                            "order": [[0,'asc']],
                                                                                  
                        });                   
               </script>

 <script>
    $('#Products').addClass('active');
 </script> 

 
	

  