
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><i class="fa fa-shopping-cart"></i>
        Order & Transaction Details
         <small>Seller</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('Dashboard');?>"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="<?php echo base_url('transaction');?>">Order & Transaction </a></li> <li class="active">Order & Transaction Details</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">

              <!-- Customer Address Detail -->
                <div class="box box-primary" style="background-color: #dee5f1;"> 
                  <div class="box-header with-border">
                   <div class="box-tools pull-right">
                      <button type="button" class="btn bg-warning btn-xs" data-widget="collapse" style="border: 1px solid #8997c7;color:#8997c7;"><i class="fa fa-minus"></i>
                      </button>                      
                    </div>
                    <h3 class="box-title">Customer Address Details</h3>
                  </div>             
                  <div class="nav-tabs-custom box-body">
                      <ul class="nav nav-tabs">
                        <li class="active"><a href="#settings" data-toggle="tab">Shipping</a></li>
                        <li><a href="#2" data-toggle="tab">Invoice</a></li>
                      </ul>
                      <div class="tab-content">
                        <div class="active tab-pane" id="settings">
                         <div class="box-body">
                           <span><?=$address['address1'];?></span><br>
                           <span><?=$address['address2'];?></span></br>
                           <span><?=$address['postcode'];?></span></br>
                           <span><?=$address['city'];?></span></br>
                           <span><?=$address['state'];?></span></br>  
                           <span><?=$address['phone'];?></span>              
                         </div>                                  
                        </div>
                        <div class="tab-pane" id="2">
                         <div class="box-body">
                           <span><?=$address['address1'];?></span><br>
                           <span><?=$address['address2'];?></span></br>
                           <span><?=$address['postcode'];?></span></br>
                           <span><?=$address['city'];?></span></br> 
                           <span><?=$address['state'];?></span></br>  
                           <span><?=$address['phone'];?></span>              
                         </div>    
                        </div>            
                     </div>
                  </div> 
                </div> 
                 <!-- Customer Address Detail// --> 

                 <!-- Shipping Status-->
                <div class="box box-primary" style="background-color: #dee5f1;">
                  <div class="box-header with-border">
                    <div class="box-tools pull-right">
                      <button type="button" class="btn bg-warning btn-xs" data-widget="collapse" style="border: 1px solid #8997c7;color:#8997c7;"><i class="fa fa-minus"></i>
                      </button>                    
                    </div>
                    <h3 class="box-title">Shipping Status</h3>
                  </div>
                  <div class="box-body">
                   <a class="btn bg" style="background-color: #25b7d3;color: #fff;"><?=$ordersdetails[0]->order_status;?></a>                   
                  </div>         
                </div> 
                 
                <!-------------------------------  Single Order Process (BOP) Shipdelight API ---------------------------------->
                <script type="text/javascript">
                  
                        $(document).ready(function(){
                        $(".spinner").hide();
                        $('#get_gen_awb').on('click',function(){
                            $('#get_gen_dawb').hide();
                            $.ajax({
                                url:'<?= base_url() ?>/Transaction/Shipdelight',
                                data:{                                
                                         id_order: $('#get_gen_awb').val(),
                                      },
                                dataType:'html',
                                method:'POST',
                                beforeSend: function(){ 
                                     location.reload(); 
                                    $('.spinner').show(); 
                                    },
                                success:function(){
                                    $('.ship-info').show();                                    
                                    },
                                complete: function(){ 
                                    $('.spinner').hide();
                                    $('#get_gen_awb').hide();
                                    }
                                });
                            });  
                        });

                // Single Order Process (BOP) Delhivery API 

                        $(document).ready(function(){
                        $(".spinner").hide();
                        $('#get_gen_dawb').on('click',function(){
                            $('#get_gen_awb').hide();
                            $.ajax({
                                url:'<?= base_url() ?>/Transaction/delhivery',
                                data:{
                                   
                                    id_order: $('#get_gen_dawb').val(),
                                },
                                dataType:'html',
                                method:'POST',
                                beforeSend: function(){ 
                                     //location.reload(); 
                                     $('.spinner').show(); 
                                     },
                                success:function(data){
                                    $('.ship-info').show();
                                    },
                                complete: function(){ 
                                    $('.spinner').hide();
                                    $('#get_gen_dawb').hide();
                                    }
                            });
                        }); 
                    });

                </script>

                 <!-- Shipping Generation-->
                <div class="box box-primary"  style="background-color: #dee5f1;">
                  <div class="box-header with-border">
                    <div class="box-tools pull-right">
                      <button type="button" class="btn bg-warning btn-xs" data-widget="collapse" style="border: 1px solid #8997c7;color:#8997c7;"><i class="fa fa-minus"></i>
                      </button>                      
                    </div>
                    <h3 class="box-title">Shipping Generation</h3>
                  </div>
                  <div class="box-body">
                          
                                <?php                   
                                 if ( isset($OrderManifestDetails['id']) && !empty($OrderManifestDetails['awb_no'])) 
                                  {
                                        $page_link = base_url().'Transaction/pdf?id_order='.$OrderManifestDetails['order_id'].'';  
                                    ?>    

                                          <div class="ship-info">
                                              Shipment Provider : <b><?=$OrderManifestDetails['shipment_provider']?></b><br/>
                                              AirWayBill number is : <b><?=$OrderManifestDetails['awb_no']?></b>
                                          </div><br/>                                            
                                          <a target="_blank" name="get_don_awb" class="btn btn-primary" href="<?=$page_link?>" target="_blank" />
                                              <span><i class="fa fa-download"></i> Download Invoice</span>
                                          </a>
                                          <a target="_blank" class="btn btn-primary" href="<?='https://erp.shipdelight.com/index.php/client_new/tracking/track_data?myid='.$OrderManifestDetails['awb_no']?>" >
                                              <span>Track No</span>
                                          </a>  
                                    <?php
                                    }
                                    else 
                                    { 
                                      ?>                                                                              
                                      <div class="ship-info">
                                      </div> 
                                     <?php 
                                     if($vendorDetails['delhivery_enable'] == 1 && $Shipdelightpincode == true)
                                     {
                                       ?>                         
                                          <button type="button" name="get_gen_awb" class="btn btn-primary" id="get_gen_awb" value="<?php echo $ordersdetails[0]->id_order;?>">
                                             <i class="fa fa-truck"></i> ShipDelight
                                          </button>
                                       <?php 
                                     }
                                     ?>
                                     <?php 
                                     if($vendorDetails['delhivery_enable'] == 1 && $Delhiverypincode == true)
                                     {
                                       ?>                         
                                          <button type="button" name="get_gen_dawb" class="btn btn-primary" id="get_gen_dawb" value="<?php echo $ordersdetails[0]->id_order;?>">
                                             <i class="fa fa-truck"></i> Delhivery
                                          </button>
                                       <?php 
                                     }
                                     ?>
                                          <br/>
                                          <div class="spinner" style="margin: 0px; padding: 0px; position: fixed; right: 0px; top: 0px; width: 100%; height: 100%; background-color: #fff; z-index: 99999; opacity: 0.9;">
                                               <h3 style="position: absolute; color: Black; top: 38%; left: 43%;"><em>Generating shipping ! 
                                                 <h5>Please wait. . .</h5></em>&nbsp;
                                                 <img src="<?=base_url();?>assets/images/shop1.svg" height="72" width="72"> 
                                               </h3>           
                                          </div>                                   
                                      <?php
                                    }
                                    ?>
                  </div>                                
                </div>
                <!-- //Shipping Generation-->                              
        </div>


        <div class="col-md-9">
           <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#settings" data-toggle="tab">Order Details</a></li>
                </ul>
                <div class="tab-content">                                                                                       
                            <h2 class="page-header" style="border-bottom: 1px solid #00c0ef;">
                                <i class="fa fa-globe"></i> Products - <?=$Reference['reference'];?>
                                <small class="pull-right"><a onclick="window.print()" style="cursor: pointer;"><i class="fa fa-print"></i> Print</a></small>
                            </h2>                                                                         
                            <div class="table-responsive">
                              <table class="table table-striped">
                                <thead>
                                <tr>                            
                                  <th>Product</th>
                                  <th>Unit_price <em>(tax_excl)</em></th>
                                  <th>Tax Rate</th>
                                  <th>Unit_price <em>(tax_incl)</em></th>
                                  <th>Quantity</th>                                                                                       
                                  <th>Total <em>(tax_excl)</em></th>
                                </tr>
                                </thead>
                                <tbody>
                                  <?php foreach($ordersdetails as $row): ?>                                 
                                    <tr>
                                      <td><?= $row->product_name; ?></td>                                                               
                                      <td><?= $row->unit_price_tax_excl;?></td>
                                      <td><?= $row->TaxPercentage;?> %</td>
                                      <td><?= $row->unit_price_tax_incl;?></td>
                                      <td><?= $row->product_quantity;?></td>
                                      <td><?= $row->total_price_tax_excl;?></td>                                                        
                                    </tr>
                                  <?php endforeach; ?>                                
                                </tbody>
                              </table>
                            </div> 
                            <div class="col-xs-6">
                            </div>
                            <div class="col-xs-6">
                              <br/>
                              <div class="table-responsive">
                                <table class="table table-striped" >                                 
                                    <tr>
                                      <th style="width:100%;border-bottom: 1px solid #00c0ef;">Discount Name</th>
                                      <th style="width:100%;border-bottom: 1px solid #00c0ef;">Value</th>
                                      <th style="width:100%;border-bottom: 1px solid #00c0ef;">Total</th>
                                    </tr>
                                    <tr>
                                      <td><?=$DiscountOffer['discount_offer'];?></td>
                                      <td>₹<?=$DiscountOffer['DiscountPrice'];?></td>
                                      <td>₹<?=$DiscountOffer['DiscountPrice'];?></td>
                                    </tr>                                                            
                                </table>
                              </div>
                            </div> 

                            <div class="row">
                            <div class="col-xs-12">
                            <br/>                             
                              <div class="table-responsive">
                                <table class="table" style="border-color:#e5f3f0;border-style: inset;">
                                  <tr>
                                    <th style="width:100%">Products:</th>
                                    <td>₹<?=$ActualTotal['total_paid_actual'];?></td>
                                  </tr>
                                  <tr>
                                    <th>Discount:</th>
                                    <td><b>-</b> <?=$DiscountOffer['DiscountPrice'];?></td>
                                  </tr>
                                  <tr>
                                    <th>Total Tax (<?=$DiscountOffer['TaxPercentage'];?>%):</th>
                                    <td>₹<?=$TotalTax['Total_tax'];?></td>
                                  </tr>
                                  <tr>
                                    <th style="width:100%;border-bottom: 1px solid #00c0ef;">Shipping:</th>
                                    <td style="width:100%;border-bottom: 1px solid #00c0ef;">₹<?=$DiscountOffer['Shipping'];?></td>
                                  </tr>
                                  <tr>
                                    <th style="width:100%">Total:</th>
                                    <td><b>₹<?=$DiscountTotal['total_paid_discount'];?></b></td>
                                  </tr>
                                </table>
                              </div>
                            </div>
                            <!-- /.col -->
                          </div>
                          <!-- /.row -->                                                                                              
                </div>    
           </div>            
      </div>  
    </section>
    
   <script>
      $('#Transactiondetails').addClass('active');
   </script>
