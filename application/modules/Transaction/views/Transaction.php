    <script type="text/javascript">
      $(document).ready(function(){
         $("#spinner").hide();
      })  
    </script>
    
    <section class="content-header">
      <h1>
      <i class="fa fa-sort"></i> Order & Transaction
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>Dashboard"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Order & Transaction</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
            <div class="box-body">
                  <a class="btn btn-app">
                    <span class="badge bg-aqua"><?= $total_customer;?></span>
                    <i class="fa fa-users"></i> Happy Customers
                  </a>               
                  <a class="btn btn-app">
                    <span class="badge bg-red"><?= $total_earn;?></span>
                    <i class="fa fa-inr"></i> Your Earning
                  </a>
                  <a class="btn btn-app">
                    <span class="badge bg-green"><?=$total_products;?></span>
                    <i class="fa fa-barcode"></i> Products
                  </a>
                  <a class="btn btn-app">
                    <span class="badge bg-teal"><?= $total_orders;?></span>
                    <i class="fa fa-inbox"></i> Orders
                  </a>                                           
            </div> 
            <div class="col-md-2" >
              <div class="form-group">
                <select class="form-control" id="OrderOperation">
                    <option value = 'bulk_action'>Bulk Action</option>
                    <option value = 'awb_generation'>Generate AWB</option>
                    <option value = 'invoice_order'>Download Invoice</option>
                    <option value = 'shipping_Pack_Slip'>Download Pack Slip</option>
                    <option value = 'manifest_slip'>Download Manifest</option>
                </select>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">    
                    <button type="button" id="bulkbutton" name="bulkbutton" class="btn btn-primary" value="Apply" >Apply</button>
              </div>
            </div>
            <div class="col-md-2">
            </div>
        
             <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>  
            <div class="col-md-2">
                <div class="form-group">                                              
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control datepicker" placeholder="Date FROM" name="startDate" id="startDate">
                  </div>
                </div>   
            </div>
            <div class="col-md-2">
                <div class="form-group">                                              
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control datepicker" placeholder="Date TO" name="endDate" id="endDate">
                  </div>
                </div>           
            </div>
            <div class="col-md-2"> 
                <div class="form-group">    
                    <button type="button" id="manifestdatecustom" name="manifestdatecustom" class="btn btn-primary"  value="manifestdatecustom">ManiFest</button>
                </div>                  
            </div>
          
            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>  
      
            <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap.css">
                                
            <section class="content">
                <div class="box-body table-responsive"> 
                <body>                     
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                      <tr>
                        <th><input type="checkbox" id="selectAll" style="width: 15px;height: 15px;cursor: pointer;"></th>
                        <th>OrderID</th>
                        <th>Reference</th>
                        <th>Customer</th>
                        <th>Total</th>
                        <th>Status</th>
                        <th>Payment</th>
                        <th>O-Date</th> 
                        <th>Action</th>                           
                      </tr>
                  </thead>
                  <tbody>                            
                  <?php foreach($orders as $row): ?>                  
                      <tr>
                        <td><input type="checkbox" name="orderselect" value="<?php echo $row->id_order; ?>" <?= $row->checkbox_status; ?> style="width: 15px;height: 15px;cursor: pointer;" ></td>
                        <td><?= $row->id_order; ?></td>
                        <td><span class="label label-danger"><?= $row->reference;?><span></td>
                        <td><?= $row->customer_name; ?></td>
                        <td><i class="fa fa-inr"></i>&nbsp;<?= $row->total_paid; ?></td>
                        <td><?= $row->order_status; ?></td>
                        <td><?= $row->payment; ?></td>
                        <td><?= Date('m/d/Y',strtotime($row->order_date)); ?></td>
                        <td><a title="View" class="view btn btn-sm btn-pink pull-left" href="<?=base_url().'Transaction/details/'.$row->id_order; ?>">VIEW<!-- <i class="fa fa-eye"></i> --></a></td>
                      </tr>
                  <?php endforeach; ?>
                  </tbody> 
                </table>
                <link rel="stylesheet" href="<?= base_url() ?>assets/bootstrap/css/loader.css">
                  <div id="spinner">
                          <h3 style="position: absolute; color: Black; top: 29%; left: 45%;">
                            <img src="<?=base_url();?>assets/images/logo.gif" height="150" width="150"> 
                          </h3>           
                  </div>  
                </body>
                <p><b>NOTE -</b> Please process 10 order at a time only.</p>      
                </div>
              <!-- /.box-body -->
            </section>          

              <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
              <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  
               <script>
                $(function() {
                  $( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
                    //Select the button and add a click button
                  $("#manifestdatecustom").click(function() {
                    //alerting the value inside the textbox
                    var startD = $("input[name=startDate]").datepicker({ dateFormat: 'yy-mm-dd' }).val();
                    var endD   = $("input[name=endDate]").datepicker({ dateFormat: 'yy-mm-dd' }).val(); 
                    //alert($.datepicker.format("yy-mm-dd", startD));
                     $.ajax({

                                  url:'<?= base_url() ?>/Transaction/manifestcustomdate',
                                  data:{                                               
                                            startDate : startD,
                                            endDate   : endD
                                        },
                                  method:'GET',
                                  success:function(){
                                        
                                           window.open('Transaction/manifestcustomdate?startDate='+startD+'&endDate='+endD);
                                          }                                     
                              });
                      });
                });
                </script>
      </div>      
    </section>
             <!-- DataTables -->
              <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
              <script src="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
                    <script>
                        $(document).ready(function () {                                    
                              var oTable = $('#example1').dataTable({
                              "lengthMenu": [10, 25],
                                  stateSave: true
                              });
                                
                              var allPages = oTable.fnGetNodes();

                              $('body').on('click', '#selectAll', function () {

                                  if ($(this).hasClass('allChecked')) 
                                  {
                                      $('input[type="checkbox"]', allPages).not(':disabled').prop('checked', false);
                                  } else 
                                  {
                                      $('input[type="checkbox"]', allPages).not(':disabled').prop('checked', true);
                                  }

                                  $(this).toggleClass('allChecked');
                              })
                          });                        
                    </script>
                    
                    <!---------------------------- Dropdown text change on selected option & Order checkbox selection ---------------------------->
                    <script type="text/javascript">                     
                         $(document).ready(function(){                           
                            $("select#OrderOperation").change(function(){
                                var OrderOperation = $('#OrderOperation').find(":selected").val();

                                  if( OrderOperation == 'bulk_action')
                                  {  
                                     $('button#bulkbutton').text('Apply');
                                     $('#bulkbutton').attr('name','bulk_action');
                                  }
                                  else
                                  if ( OrderOperation == 'awb_generation') 
                                  {  

                                     $('input[type="checkbox"]').prop('checked' ,!$('input[type="checkbox"]:checked').prop('disabled',true));
                                     $('button#bulkbutton').text('Generate AWB');
                                     $('#bulkbutton').attr('name','awb_generation');                                    
                                    
                                  } 
                                  else
                                  if( OrderOperation == 'invoice_order')
                                  {
                                    
                                     $('input[type="checkbox"]').prop('disabled' ,!$('input[type="checkbox"]:disabled').prop('checked',true));        
                                     $('button#bulkbutton').text('Download Invoice');
                                     $('#bulkbutton').attr('name','invoice_order');

                                  }
                                  else 
                                  if (OrderOperation == 'shipping_Pack_Slip') 
                                  {
                              
                                     $('input[type="checkbox"]').prop('disabled' ,!$('input[type="checkbox"]:disabled').prop('checked',true));
                                     $('button#bulkbutton').text('Download Pack-Slip');
                                     $('#bulkbutton').attr('name','shipping_Pack_Slip');

                                  } 
                                  else 
                                  if(OrderOperation == 'manifest_slip')
                                  {
                                     $('input[type="checkbox"]').prop('disabled' ,!$('input[type="checkbox"]:disabled').prop('checked',true));
                                     $('button#bulkbutton').text('Download Manifest');
                                     $('#bulkbutton').attr('name','manifest_slip');
                                  }

                            });
                        });
                    </script>
                    
                    <!-------------------------------  Bulk Order Process (BOP) Shipdelight API ---------------------------------->
                    <script type="text/javascript">                      
                         $(document).on("click","#bulkbutton" ,function(){                       
                           var array = [];
                                    $.each($("input[name='orderselect']:checked"), function(){            
                                        array.push($(this).val());                                       
                                      
                                    });
                           var dataOrderID = array.join(", ");

                           var buttonname = $(this).attr('name');

                          if (array.length >0)
                          {

                           switch(buttonname)
                           {
                            case "awb_generation":
                               $.ajax({
                                      url:'<?= base_url() ?>/Transaction/shipdelightbulk',
                                      data:{                                
                                               id_order: dataOrderID
                                            },
                                      dataType:'html',
                                      method:'POST',
                                      beforeSend: function(){ 
                                            location.reload(); 
                                            $('#spinner').show();
                                              },
                                      success:function(data){
                                            $('#spinner').hide();
                                            alert('AWB Number Successfully Generated for order id ' +dataOrderID);
                                          },
                                      });
                              break;
                    
                            case "invoice_order":                          
                                var bulkURL = "<?php echo base_url().'Transaction/invoicebulkpdf?id_order=';?>"+dataOrderID;
                                window.open(bulkURL);                           
                              break;

                            case "shipping_Pack_Slip":
                                var packslipURL = "<?php echo base_url().'Transaction/packslipbulkpdf?id_order=';?>"+dataOrderID;
                                window.open(packslipURL);                                
                              break;

                            case "manifest_slip":
                                var manifestURL = "<?php echo base_url().'Transaction/manifestbulkpdf?id_order=';?>"+dataOrderID;
                                window.open(manifestURL);
                              break;

                            default:
                              alert("Please select valid option");
                            }
                          }
                          else
                          {
                              alert("No order selected!");
                          }
                          
                         });

                    </script>
      </div>      
    </section>
   
  
  <script>
    $("#Transaction").addClass('active');
  </script>
  