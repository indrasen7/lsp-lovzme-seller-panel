<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Transaction extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('pagination'); // loaded codeigniter pagination liberary
        $this->load->library('TCpdflib');
        $this->load->library('PhpMailerLib');
       
        $this->load->model('Transaction_model');
        $this->load->model('OrderreferenceGroup_model');
        $this->load->model('Discount_model');
        $this->load->model('Tax_model');
        $this->load->model('Delhivery_model');
        $this->load->model('ShipDelight_model');
        $this->load->model('OrderManifestDetails_model');
        $this->load->model('Vendor_model');
        $this->load->model('Address_model');
        $this->load->model('pdf_model');
        $this->load->model('Cron_model');
        $this->load->model('Lovzmemail_model');
        $this->load->model('Lovzmesms');
        $this->load->model('Shop');


    } 
	
    public function index()
    {    
        $seller_customer_id = $this->session->userdata('seller_customer_id');
        
        $data['title']                   = 'LOVZme : Transaction';
        $data['view']                    = 'Transaction';
        $data['seller_name']             = $this->Transaction_model->getSeller($seller_customer_id);
        $data['seller_gender']           = $this->Transaction_model->getGender($seller_customer_id);
        $data['total_customer']          = $this->Transaction_model->getTotalcustomer($seller_customer_id);
        $data['total_orders']            = $this->Transaction_model->getTotalorder($seller_customer_id)->total_orders;
        $data['total_earn']              = round($this->Transaction_model->getSellerTotalEarn($seller_customer_id));
        $data['total_products']          = $this->Transaction_model->getTotalproduct($seller_customer_id)->total_products;
        $data['orders']                  = $this->Transaction_model->getSellerOrderTable($seller_customer_id);

       
        $this->load->view('layout', $data);
    }

    public function details($id_order)
    {        
        $seller_customer_id              = $this->session->userdata('seller_customer_id');
        $orderDetails                    = $this->Transaction_model->getSellerOrderTablebyID($id_order);
        $Deliveryaddress                 = $this->Address_model->getAddressDetails($orderDetails[0]->id_address_delivery);
        $data['ordersdetails']           = $orderDetails;
        $data['view']                    = 'Transactiondetails';  
        $data['seller_name']             = $this->Transaction_model->getSeller($seller_customer_id);
        $data['seller_gender']           = $this->Transaction_model->getGender($seller_customer_id);
        $data['Reference']               = $this->OrderreferenceGroup_model->getSellerReferenceName($id_order);       
        $data['ActualTotal']             = $this->Transaction_model->getActuaPriceTotal($id_order);
        $data['DiscountTotal']           = $this->Discount_model->getPriceTotalDiscount($id_order);
        $data['DiscountOffer']           = $this->Discount_model->getDiscountShip($id_order);
        $data['TotalTax']                = $this->Tax_model->getTotalTax($id_order);
        $data['address']                 = $this->Transaction_model->getAddress($id_order);

        $data['vendorDetails']           = $this->Vendor_model->getVendor($seller_customer_id);
        $data['OrderManifestDetails']    = $this->OrderManifestDetails_model->getOrderManifestDetails($id_order);
        $data['Deliveryaddress']         = $Deliveryaddress;
        $data['Shipdelightpincode']      = $this->ShipDelight_model->Shipdelightpincode($Deliveryaddress['postcode']);
        $data['Delhiverypincode']        = $this->Delhivery_model->Delhiverypincode($Deliveryaddress['postcode']);
            
        $this->load->view('layout', $data);
            
    }

    //Get the Order selected and pass to generate Shipdelight API(AWB) SOP(Single order Process)
    public function shipdelight()
    {

        $shipdelightData = [
                             'id_order' => $_POST['id_order'],
                           ];

        $this->ShipDelight_model->getShipDelight($shipdelightData);     
       
    }

    //Get the Order selected and pass to generate Delhivery API(AWB) SOP(Single order Process)
    public function delhivery()
    {

        $delhiveryData =  $_POST['id_order'];
       
        $this->Delhivery_model->getDelhivery($delhiveryData);     
       
    }
    
    //Get the IDs selected list to generate Shipdelight API(AWB)  BOP(Bulk order Process) 
    public function shipdelightbulk()
    {
        $id_orderlist = $_POST['id_order'];

        $this->ShipDelight_model->BulkgetShipDelight($id_orderlist);      

    }

    //Get the PDF url generate for Invoice PDF(Single Order)
    public function pdf()
    {
        $id_order    = $this->input->get('id_order');
        $check_id    = $this->OrderManifestDetails_model->getOrderManifestDetails($id_order);
       
        if($this->input->method() =='get' && $check_id['order_id'] )
        { 
            $id_order = $this->input->get('id_order');
           
            $this->pdf_model->getinvoicePDF($id_order);
        }
        else
        {
            echo "Please enter the correct order ID";
        }
    }
     
    //Get the PDF url generate for Invoice PDF (Bulk Order)
    public function invoicebulkpdf()
    {   
           $id_order_list  = $this->input->get('id_order');
       
           $this->pdf_model->getbulkinvoicePDF($id_order_list);
    }

     //Get the PDF url generate for Manifest PDF (Bulk Order)
    public function packslipbulkpdf()
    {
           $id_order_list  = $this->input->get('id_order');
       
           $this->pdf_model->getbulkpackslipPDF($id_order_list);
       
    }

    //Get the PDF url generate for Manifest PDF (Bulk Order)
    public function manifestbulkpdf()
    {
           $id_order_list  = $this->input->get('id_order');
      
           $this->pdf_model->getbulkmanifestPDF($id_order_list);       
    }

    public function manifestcustomdate()
    {   
           $startDate = $this->input->get('startDate');
           $endDate   = $this->input->get('endDate');
           $this->pdf_model->getManifestDatecustom($startDate, $endDate);
       
    }
    
    //CRON status for ready to ship to shipped
    public function cron()
    {
           //$this->Cron_model->readyshiptoshipped();
           $this->Cron_model->checkshipped();
          //print_r($this->getcronstatus());exit;
    }
    
    //Test the email
    public function email()
    {
           //$this->Cron_model->readyshiptoshipped();
           $orderid = '2145';
           $this->Lovzmemail_model->orderMail($orderid);
          //print_r($this->getcronstatus());exit;
    }
     //Test the SMS
    public function sms()
    {
           $orderid = '2147';
           $this->Lovzmesms->smsDetails($orderid); 
    }

    
}
