<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron_model extends CI_Model 
{
	public function __construct()
	{
		parent::__construct();
	}
   
    //Convert Ready to ship to Shipped
	public function readyshiptoshipped()
	{
		$sql = 'SELECT 
                     o.id_order,
                     o.current_state,
                     oc.tracking_number AS awb_no,
                     c.name as shipment_provider
                FROM 
                      ps_orders o  
                LEFT JOIN 
                      ps_order_carrier oc ON  
                      o.id_order = oc.id_order
                LEFT JOIN 
                      ps_carrier c ON 
                      c.id_carrier = oc.id_carrier  
                WHERE 
                      current_state = 14
                AND   tracking_number IS NOT NULL AND tracking_number !=""';
       
        $query = $this->db->query($sql);
        $result = $query->result_array(); 
        //print_r($result);exit; 

        $file = APPPATH.'/cron_log/ready_to_ship.txt'; 
        $ban = date('D Y-m-d h:i:s A')."\nOrder ->Shipment Provider -> Status\n";
        $open = fopen( $file, "a" );
        $write = fputs( $open, $ban );

        foreach($result as $order_info)
        {   
            $order_id = $order_info['id_order'];
            $current_state = $order_info['current_state'];
            $awb_no = $order_info['awb_no'];
            $shipment_provider = $order_info['shipment_provider'];
                               
            if($shipment_provider == "Shipdelight")
            { 

            	$Shipdelight_url = "https://crm.shipdelight.com/index.php/api/detailed_status_test1/trackAwb?awb=".$awb_no;
            
	            $ch = curl_init();
	            curl_setopt($ch, CURLOPT_URL, $Shipdelight_url);
	            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2); 
	            $result_json = curl_exec($ch);
	            $result_shipdelight = json_decode($result_json);
	            curl_close($ch);
                 //print_r($result_shipdelight);
                if(isset($result_shipdelight[0]->status->status))
                {
                    print_r($result_shipdelight);

                    if($result_shipdelight[0]->status->status == 'IN TRANIST')
                    {
                        $order_status = 'shipped';
                        $this->ShipDelight_model->insertorderhistory(4, $order_id);
                        $this->ShipDelight_model->updateorderstatus($order_id, 4);
                        $this->ShipDelight_model->updatesellerOrderStatus($order_id, 4 , $awb_no);
                    }
                    
                    $details = $order_id." -> ".$shipment_provider." -> ".$order_status." \n ";
                    $write = fputs( $open, $details );  
                }
                else
                {
                    echo "Invalid status";
                }
            }
           
        }
    }

    //Convert Shipped to Delivered
    public function checkshipped()
    {
        $sql = 'SELECT 
                     o.id_order,
                     o.current_state,
                     oc.tracking_number AS awb_no,
                     c.name as shipment_provider
                FROM 
                      ps_orders o  
                LEFT JOIN 
                      ps_order_carrier oc ON  
                      o.id_order = oc.id_order
                LEFT JOIN 
                      ps_carrier c ON 
                      c.id_carrier = oc.id_carrier  
                WHERE 
                      current_state = 4
                AND   tracking_number IS NOT NULL AND tracking_number !=""';
       
        $query = $this->db->query($sql);
        $result = $query->result_array(); 
        //print_r($result);exit; 

        $file = APPPATH.'/cron_log/shipped_status.txt'; 
        $ban = date('D Y-m-d h:i:s A')."\nOrder ->Shipment Provider -> Status\n";
        $open = fopen( $file, "a" );
        $write = fputs( $open, $ban );

        foreach($result as $order_info)
        {   
            $order_id = $order_info['id_order'];
            $current_state = $order_info['current_state'];
            $awb_no = $order_info['awb_no'];
            $shipment_provider = $order_info['shipment_provider'];
                            
            if($shipment_provider == "Shipdelight")
            { 
                
                $Shipdelight_url = "https://crm.shipdelight.com/index.php/api/detailed_status_test1/trackAwb?awb=".$awb_no;
            
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $Shipdelight_url);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2); 
                $result_json = curl_exec($ch);
                $result_shipdelight = json_decode($result_json);
                curl_close($ch);
                
                if(isset($result_shipdelight[0]->status->status))
                {

                   if ($result_shipdelight[0]->status->status == 'DELIVERED') {
                        $order_status = 'completed';
                        $this->ShipDelight_model->insertorderhistory(5, $order_id);
                        $this->ShipDelight_model->updateorderstatus($order_id, 5);
                        $this->ShipDelight_model->updatesellerOrderStatus($order_id, 5 , $awb_no);
                    }
                    elseif ($result_shipdelight[0]->status->status == 'RTO DELIVERED'){
                        $order_status = 'returned';
                        $this->ShipDelight_model->insertorderhistory(15, $order_id);
                        $this->ShipDelight_model->updateorderstatus($order_id, 15);
                        $this->ShipDelight_model->updatesellerOrderStatus($order_id, 15 , $awb_no);
                    }
                    
                    $details = $order_id." -> ".$shipment_provider." -> ".$order_status." \n ";
                    $write = fputs( $open, $details );  
                }
                else
                {
                    echo "Invalid status";
                }
            }
           
        }

    }
	
}

/* End of file cron_model.php */
/* Location: ./application/modules/Transaction/models/cron_model.php */