<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tax_model extends CI_Model 
{
	public function __construct()
	{
		parent::__construct();
	}

	//Get the total tax of order product from database table
	public function getTotalTax($id_order)
	{
		$SQL = "SELECT
			            ROUND(SUM(total_amount) ,2) AS Total_tax
				FROM 
				        ps_order_detail_tax odt

				LEFT JOIN ps_order_detail od ON
						od.id_order_detail = odt.id_order_detail
				LEFT JOIN ps_orders o ON 
				        o.id_order = od.id_order
				WHERE 
					    o.id_order =".$id_order."";

		$query = $this->db->query($SQL);
		$result = $query->row_array();
		return $result;
	}	

}

/* End of file Tax_model.php */
/* Location: ./application/modules/Transaction/models/Tax_model.php */