<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lovzmemail_model extends CI_Model 
{	
	private $mail = '';

	public function __construct()
	{
		parent::__construct();	

		$this->mail = $this->phpmailerlib->load();
		$mail = $this->mail;

		// Server settings
		//$mail->SMTPDebug = 2;                     // Enable verbose debug output
		$mail->isSMTP();                            // Set mailer to use SMTP
		$mail->Host        = 'smtp.gmail.com';  	// Specify main and backup SMTP servers
		$mail->SMTPAuth    = true;                  // Enable SMTP authentication
		$mail->Username    = SMTPEMAIL;         	// SMTP username
		$mail->Password    = SMTPPASS;           	// SMTP password
		$mail->SMTPSecure  = 'tls';              	// Enable TLS encryption, `ssl` also accepted
		$mail->Port        = 587;           		// port 587 for tls
		$mail->SMTPOptions = array(
								    'ssl' => array(
												    'verify_peer' => false,
												    'verify_peer_name' => false,
												    'allow_self_signed' => true
    						                      )
								  );
	}
    
	public function orderMail( $order )
	{	
        $id_order            = $order;
        $orderDetails        = $this->Transaction_model->getSellerOrderTablebyID($id_order);
 		$ShipDelightaddress  = $this->Address_model->getAddressDetails($orderDetails[0]->id_address_delivery);
        	
	    $email               = $orderDetails[0]->email;		
		$shop_name			 = $this->Shop->getShopName();
		$firstname			 = $ShipDelightaddress['firstname'];
		$lastname			 = $ShipDelightaddress['lastname'];
		$order_name			 = $orderDetails[0]->reference;		
		$history_url		 = $this->Shop->getSiteURL()."/order-history";
		$my_account_url		 = $this->Shop->getSiteURL()."/my-account";
		//$guest_tracking_url	= $this->Shop->getSiteURL()."/guest-tracking?id_order=".$order_name;
		$shop_url			 = $this->Shop->getSiteURL();

		//Recipients
		$mail = $this->mail;
		$mail->setFrom(SMTPEMAIL, 'LOVZme');
		$mail->addAddress( $email, $firstname.' '.$lastname);     // Add a recipient
		
		// Content
		$mail->CharSet  = 'UTF-8';
		// $mail->Encoding = 'base64';
		$mail->isHTML(true);                                  // Set email format to HTML
		$mail->Subject  = 'LOVZme Order confirmation (Order ID:'.$id_order.')';
		$mail->Body 	= "";
        
		$mail->Body    .= '
							<html>
								<head>
									<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
									<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
									<title>Message from '.$shop_name.'</title>									
									
									<style>	
									        @media only screen and (max-width: 300px){ 
											body {
												width:218px !important;
												margin:auto !important;
											}
											thead, tbody{width: 100%}
											.table {width:195px !important;margin:auto !important;}
											.logo, .titleblock, .linkbelow, .box, .footer, .space_footer{width:auto !important;display: block !important;}		
											span.title{font-size:20px !important;line-height: 23px !important}
											span.subtitle{font-size: 14px !important;line-height: 18px !important;padding-top:10px !important;display:block !important;}		
											td.box p{font-size: 12px !important;font-weight: bold !important;}
											.table-recap table, .table-recap thead, .table-recap tbody, .table-recap th, .table-recap td, .table-recap tr { 
												display: block !important; 
											}
											.table-recap{width: 200px!important;}
											.table-recap tr td, .conf_body td{text-align:center !important;}	
											.address{display: block !important;margin-bottom: 10px !important;}
											.space_address{display: none !important;}	
										}
								     @media only screen and (min-width: 301px) and (max-width: 500px) { 
											body {width:425px!important;margin:auto!important;}
											thead, tbody{width: 100%}
											.table {margin:auto!important;}	
											.logo, .titleblock, .linkbelow, .box, .footer, .space_footer{width:auto!important;display: block!important;}	
											.table-recap{width: 295px !important;}
											.table-recap tr td, .conf_body td{text-align:center !important;}
											.table-recap tr th{font-size: 10px !important}
											
										}
								     @media only screen and (min-width: 501px) and (max-width: 768px) {
											body {width:478px!important;margin:auto!important;}
											thead, tbody{width: 100%}
											.table {margin:auto!important;}	
											.logo, .titleblock, .linkbelow, .box, .footer, .space_footer{width:auto!important;display: block!important;}			
										}
								     @media only screen and (max-device-width: 480px) { 
											body {width:340px!important;margin:auto!important;}
											thead, tbody{width: 100%}
											.table {margin:auto!important;}	
											.logo, .titleblock, .linkbelow, .box, .footer, .space_footer{width:auto!important;display: block!important;}
											
											.table-recap{width: 295px!important;}
											.table-recap tr td, .conf_body td{text-align:center!important;}	
											.address{display: block !important;margin-bottom: 10px !important;}
											.space_address{display: none !important;}	
										}
							        </style>

								</head>
									<body style="-webkit-text-size-adjust:none;background-color:#fff;width:650px;font-family:Open-sans, sans-serif;color:#555454;font-size:13px;line-height:18px;margin:auto" >
								<table class="table table-mail" style="width: 100%; margin-top: 10px; -moz-box-shadow: 0 0 5px #afafaf; -webkit-box-shadow: 0 0 5px #afafaf; -o-box-shadow: 0 0 5px #afafaf; box-shadow: 0 0 5px #afafaf; filter: progid:DXImageTransform.Microsoft.Shadow(color=#afafaf,Direction=134,Strength=5);">
								<tbody>
								<tr>
								<td class="space" style="width: 20px; padding: 7px 0;"> </td>
								<td align="center" style="padding: 7px 0;">
								<table class="table" bgcolor="#ffffff" style="width: 100%;">
								<tbody>
								<tr>
								<td align="center" class="logo" style="border-bottom: 4px solid #11d1d3; padding: 7px 0;"><a title="LOVZme" href="https://www.lovzme.com/" style="color: #337ff1;"> <img src="https://www.lovzme.com/img/lovzme-logo-1560584795.jpg" alt="LOVZme" /> </a></td>
								</tr>
								<tr>
								<td align="center" class="titleblock" style="padding: 7px 0;"><span size="2" face="Open-sans, sans-serif" color="#555454" style="color: #555454; font-family: Open-sans, sans-serif; font-size: small;"> <span class="title" style="font-weight: 500; font-size: 28px; text-transform: uppercase; line-height: 33px;">Hi '.$firstname.' '.$lastname.',</span><br /> <span class="subtitle" style="font-weight: 500; font-size: 16px; text-transform: uppercase; line-height: 25px;">Your order has been ready to ship</span> </span></td>
								</tr>
								<tr>
								<td class="space_footer" style="padding: 0!important;"> </td>
								</tr>
								<tr>
								<td class="box" style="border: 1px solid #11d1d3; background-color: #f8f8f8; padding: 7px 0;">
								<table class="table" style="width: 100%;">
								<tbody>
								<tr>
								<td width="10" style="padding: 7px 0;"> </td>
								<td style="padding: 7px 0;">
								<p data-html-only="1" style="border-bottom: 1px solid #D6D4D4; margin: 3px 0 7px; text-transform: uppercase; font-weight: 500; font-size: 18px; padding-bottom: 10px;">Order '.$order_name.' - Ready to Ship</p>
								<span size="2" face="Open-sans, sans-serif" color="#555454" style="color: #555454; font-family: Open-sans, sans-serif; font-size: small;"><span style="color: #777;"> Your order with the reference <span style="color: #333;"><strong> '.$order_name.' </strong></span> has been ready to ship.<br /> Thank you for shopping with LOVZme ! </span> </span></td>
								<td width="10" style="padding: 7px 0;"> </td>
								</tr>
								</tbody>
								</table>
								</td>
								</tr>
								<tr>
								<td class="linkbelow" style="padding: 7px 0;"><span> You can review your order and download your invoice from the <a href="'.$history_url.'" style="color: #337ff1;">"Order history"</a> section of your customer account by clicking <a href="'.$my_account_url.'" style="color: #337ff1;">"My account"</a> on our shop. </span></td>
								</tr>
								<tr>
								<td class="space_footer" style="padding: 0!important;"> </td>
								</tr>
								<tr>
								<td class="footer" style="border-top:4px solid #ff4c90;padding:7px 0">
									<span><a href="'.$shop_url.'" style="color:#337ff1">'.$shop_name.'</a> All Rights Reserved by <a href="'.$shop_url.'" style="color:#337ff1">LOVZme&trade;</a>
									</span>
								</td>
								</tr>
								</tbody>
								</table>
								</td>
								<td class="space" style="width: 20px; padding: 7px 0;"> </td>
								</tr>
								</tbody>
								</table>
								</body>
							</html>';
		if( SMTP_ENABLE == TRUE )
		{
			$mail->send();
		}

	}
	

	public function testmail($email)
	{
		//Recipients
		$mail = $this->mail;
		$mail->setFrom('yindrasen77@gmail.com', 'LOVZme');
		$mail->addAddress( $email, $email);     // Add a recipient
		
		// Content
		$mail->CharSet = 'UTF-8';
		// $mail->Encoding = 'base64';
		$mail->isHTML(true);                                  // Set email format to HTML
		$mail->Subject  = 'Test mail to '.$email;
		$mail->Body 	= "This is the test mail ";
		$mail->send();
	}
}

/* End of file Lovzmemail.php */
/* Location: ./application/models/Lovzmemail.php */