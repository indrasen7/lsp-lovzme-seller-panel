<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OrderManifestDetails_model extends CI_Model 
{
	public function __construct()
	{
		parent::__construct();
	}

	//Check where the order of AWB is generated or not from manifest on button click of(Delhivery & Bluedart)
	public function getOrderManifestDetails($order_id)
	{   
		$SQL = "SELECT
			        *
				FROM 
				    ps_orders_manifests om
			    WHERE 
					om.order_id =".$order_id."";

		$query = $this->db->query($SQL);
		$result = $query->row_array();
		return $result;
	}	

}

/* End of file OrderManifestDetails_model.php */
/* Location: ./application/modules/Transaction/models/OrderManifestDetails_model.php */