<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pdf_model extends CI_Model 
{
	public function __construct()
	{
		parent::__construct();
	}

    //Get the Single Invoice PDF of Order 
	public function getinvoicePDF($id_order)
    {
        $seller_customer_id = $this->session->userdata('seller_customer_id');

    	$orderDetails           = $this->Transaction_model->getSellerOrderTablebyID($id_order);
        $Reference              = $this->OrderreferenceGroup_model->getSellerReferenceName($id_order);       
        $ActualTotal            = $this->Transaction_model->getActuaPriceTotal($id_order);
        $DiscountOffer          = $this->Discount_model->getDiscountShip($id_order);
        $TotalTax               = $this->Tax_model->getTotalTax($id_order);
        $OrderManifestDetails   = $this->OrderManifestDetails_model->getOrderManifestDetails($id_order);
        $vendorDetails          = $this->Vendor_model->getVendor($OrderManifestDetails['vendor_id']);
        $Deliveryaddress        = $this->Address_model->getAddressDetails($orderDetails[0]->id_address_delivery);
        $collectable_value      = $this->Discount_model->getPriceTotalDiscount($id_order);      
        $sellerEmail            = $this->ShipDelight_model->getSeller($seller_customer_id);
       
            // Create new PDF document
            $pdf = $this->tcpdflib->load();
            //set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('LOVZME');
            $pdf->SetTitle('Invoice');
            $pdf->SetSubject('Invoice');
            $pdf->SetKeywords('PDF, Invoice');
            //set margins
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT); 
            $pdf->SetPrintHeader(false);
            $pdf->SetPrintFooter(true);
            $pdf->SetFooterMargin(20);
            $pdf->setFooterFont(Array('times', '', 11));
            // set font
            $pdf->SetFont('times', '', 11); 
            // add a page
            $pdf->AddPage();
            $img_file = base_url().'assets/images/lovzme.png';           
           
            $block1 = '     <img src="'.$img_file.'"><br>
                            <b>'.$vendorDetails['company_name'].'</b><br>
                            '.$vendorDetails['address_1'].'<br>
                            '.$vendorDetails['address_2'].'<br>
                            '.$vendorDetails['city'].'<br>
                            PIN : '.$vendorDetails['pin'].'<br>
                            Phone : '.$vendorDetails['contact_no'].'<br>
                            Email : '.$sellerEmail['email'].'<br>
                            <b>GSTN :'.$vendorDetails['gstn'].'</b>  
            ';
    
            if( $OrderManifestDetails['mode'] == 'COD' || $OrderManifestDetails['mode'] == 'Cash on delivery (COD)' || $OrderManifestDetails['mode'] == 'Cash on delivery')
            {
                $block2_1 = '   <br><br><br><br><br><br>
                                <b>
                                AMOUNT TO BE COLLECTED<br>
                                Rs. '.($collectable_value['total_paid_discount'] + 0).'
                                </b>';
            }
    
            $block2 = ' <br><br><b>'.$OrderManifestDetails['mode'].' ORDER</b><br>
                        Shipment Provider: '.$OrderManifestDetails['shipment_provider'].'<br>
                        '.$block2_1;
    
            $pdf->SetFillColor(255, 255, 255);
            $pdf->writeHTMLCell(0, 65, 15, 15,$block1, 1, 1, 1, true, 'J', true);
            $pdf->writeHTMLCell(0, 65, 105, 15, $block2, 1, 1, 1, true, 'C', true);
    
            $style = array(
                'position' => '',
                'align' => 'C',
                'stretch' => false,
                'fitwidth' => true,
                'cellfitalign' => '',
                'border' => false,
                'hpadding' => 'auto',
                'vpadding' => 'auto',
                'fgcolor' => array(0,0,0),
                'bgcolor' => false,
                'text' => true,
                'font' => 'times',
                'fontsize' => 8,
                'stretchtext' => 1
            );
           
            $pdf->write1DBarcode($OrderManifestDetails['awb_no'], 'C39', '108', '35', '', 25, 0.4, $style, 'N');
    
            $html = '';
            $html .='';
            $block3 ='  <b> DELIVER TO :</b><br>
                        '.$Deliveryaddress['firstname'].''.$Deliveryaddress['lastname'].'<br>
                        '.preg_replace('/\s+/', '', $OrderManifestDetails['shipping_address']).'<br>
                        '.$Deliveryaddress['city'].'<br>
                        <b>'.$OrderManifestDetails['destination'].'</b><br>
                        '.($Deliveryaddress['state_name']).'<br>
                        '.$Deliveryaddresss['postcode'].'
                        Phone : '.$Deliveryaddress['phone'].'
                        ';
            $pdf->writeHTMLCell(69, 50, 15, 80, $block3, 0, 1, 0, true, 'J', true);
                        
            $block4 = ' <b>ORDER ID :</b><br>';
            $pdf->write1DBarcode($id_order, 'C39', '85', '85', '45', 25, 0.4, $style, 'N');
    
            $pdf->writeHTMLCell(0, 10, 95, 80, $block4, 0, 1, 0, true, 'J', true);
    
            $block5 = ' <b> AWB No. : '.$OrderManifestDetails['awb_no'].'</b><br>
                        Weight (kgs) : '.$OrderManifestDetails['weight'].'<br>
                        Dimensions (cms) : 8*5*5<br>
                        Order ID : '.$id_order.' <br>
                        Order Date : '.substr($OrderManifestDetails['created_at'],0,10).'<br>
                        Pieces : '.$ActualTotal['totalquantity'].'<br>
            ';    
            $pdf->writeHTMLCell(0, 10, 135 , 80, $block5, 0, 1, 0, true, 'J', true);
             
            $html .= '<div>
                        <table border="1" width="100%" cellspacing="0" cellpadding="2" style="margin-left:15%;margin-right:15%;">
                                                        <tr>
                                                            <td width="5%">Sr.</td>
                                                            <td width="12%">Item Code</td>
                                                            <td width="10%">EAN Code</td>
                                                            <td width="42%">Item Description</td>
                                                            <td width="10%">Quantity</td>
                                                            <td width="10%">Value</td>
                                                            <td width="10%">Total Amount</td>
                                                        </tr>
                                                    ';
            $j = 1;
    
            foreach($orderDetails as  $key =>$value)
            {
                $total_amount = floatval($value->unit_price_tax_excl);
                $html .= '                          <tr>
                                                        <td>'.$j.'</td>
                                                        <td>'.$value->product_id.'</td>
                                                        <td>'. (!empty($products['ean13']) ? $products['ean13'] : '--' ).'</td>
                                                        <td>'.$value->product_name.'</td>
                                                        <td>'.$value->product_quantity.'</td>
                                                        <td>'.$value->unit_price_tax_excl.'</td>
                                                        <td>'.$total_amount.'</td>
                                                    </tr>';
                $j++;
            }         

            $html .='                       <tr>
                                                <td colspan="4"></td>
                                                <td colspan="2">Order Subtotal</td>
                                                <td >'.$ActualTotal['total_paid_actual'].'</td>
                                            </tr>

                                            <tr>
                                                <td colspan="4"></td>
                                                <td colspan="2">GST Charges</td>
                                                <td align="center">'.$TotalTax['Total_tax'].'</td>
                                            </tr> 
    
                                            <tr>
                                                <td colspan="4"></td>
                                                <td colspan="2">Shipping Charges</td>
                                                <td>'.$DiscountOffer['Shipping'].'</td>
                                            </tr>
                                            
                                            <tr>
                                                <td colspan="4"></td>
                                                <td colspan="2">Discount</td>
                                                <td align="center">-'.$DiscountOffer['DiscountPrice'].'</td>
                                            </tr>  
                                            
                                            <tr>
                                                <td colspan="4"></td>
                                                <td colspan="2">Total</td>
                                                <td><b>'.$collectable_value['total_paid_discount'].'</b></td>
                                            </tr>   
                                        </table>
                                        </div>
                    ';
            $html .= '<br><br>This is computer generated document, hence does not require signature.<br>';
            $html .= 'Write us at support@lovzme.com<br>Return Address : '.$vendorDetails['company_name'].','.$vendorDetails['address_1'].','.$vendorDetails['address_2'].','.$vendorDetails['city'].','.$vendorDetails['pin'];            
            $pdf->SetFillColor(255, 255, 255);
            $pdf->writeHTMLCell(0, 50, 15, 80 , "", 1, 1, 0, true, 'C', true);
            $pdf->writeHTMLCell(0,145,15,130 , $html, 1, 1, 0, true, 'C', true);

            $filename = 'order_'.$id_order.'.pdf';           
            ob_clean();
            $pdf->Output($filename, 'I'); 
           
    }

    //Get the Invoice PDF of Order 
    public function getbulkinvoicePDF($id_order)
    {
            $id_order_list = $id_order;
            $id_order_array = explode(",", $id_order_list);

            // Create new PDF document
            $pdf = $this->tcpdflib->load();
            //set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('LOVZME');
            $pdf->SetTitle('Invoice');
            $pdf->SetSubject('Invoice');
            $pdf->SetKeywords('PDF, Invoice');
            //set margins
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT); 
            $pdf->SetPrintHeader(false);
            $pdf->SetPrintFooter(true);
            $pdf->SetFooterMargin(20);
            $pdf->setFooterFont(Array('times', '', 11));
            $pdf->SetAutoPageBreak(TRUE, 0);
            // set font
            $pdf->SetFont('times', '', 11); 

        foreach ($id_order_array as  $key =>$value) 
         {  
             
                $id_order = $value;
                        
        
            $seller_customer_id = $this->session->userdata('seller_customer_id');                                         

            $orderDetails           = $this->Transaction_model->getSellerOrderTablebyID($id_order);
            $Reference              = $this->OrderreferenceGroup_model->getSellerReferenceName($id_order);       
            $ActualTotal            = $this->Transaction_model->getActuaPriceTotal($id_order);
            $DiscountOffer          = $this->Discount_model->getDiscountShip($id_order);
            $TotalTax               = $this->Tax_model->getTotalTax($id_order);
            $OrderManifestDetails   = $this->OrderManifestDetails_model->getOrderManifestDetails($id_order);
            $vendorDetails          = $this->Vendor_model->getVendor($OrderManifestDetails['vendor_id']);
            $Deliveryaddress        = $this->Address_model->getAddressDetails($orderDetails[0]->id_address_delivery);
            $collectable_value      = $this->Discount_model->getPriceTotalDiscount($id_order);      
            $sellerEmail            = $this->ShipDelight_model->getSeller($seller_customer_id);        
            
            // add a page
            $pdf->AddPage();
            $img_file = base_url().'assets/images/lovzme.png';
           
            $block1 = '     <img src="'.$img_file.'"><br>
                            <b>'.$vendorDetails['company_name'].'</b><br>
                            '.$vendorDetails['address_1'].'<br>
                            '.$vendorDetails['address_2'].'<br>
                            '.$vendorDetails['city'].'<br>
                            PIN : '.$vendorDetails['pin'].'<br>
                            Phone : '.$vendorDetails['contact_no'].'<br>
                            Email : '.$sellerEmail['email'].'<br>
                            <b>GSTN :'.$vendorDetails['gstn'].'</b>  ';
    
            if( $OrderManifestDetails['mode'] == 'COD' || $OrderManifestDetails['mode'] == 'Cash on delivery (COD)' || $OrderManifestDetails['mode'] == 'Cash on delivery')
            {
                $block2_1 = '   <br><br><br><br><br><br>
                                <b>
                                AMOUNT TO BE COLLECTED<br>
                                Rs. '.($collectable_value['total_paid_discount'] + 0).'
                                </b>';
            }
    
            $block2 = ' <br><br><b>'.$OrderManifestDetails['mode'].' ORDER</b><br>
                        Shipment Provider: '.$OrderManifestDetails['shipment_provider'].'<br>
                        '.$block2_1;
    
            $pdf->SetFillColor(255, 255, 255);
            $pdf->writeHTMLCell(0, 65, 15, 15,$block1, 1, 0, 1, true, 'J', true);
            $pdf->writeHTMLCell(0, 65, 105, 15, $block2, 1, 1, 1, true, 'C', true);
    
            $style = array(
                'position' => '',
                'align' => 'C',
                'stretch' => false,
                'fitwidth' => true,
                'cellfitalign' => '',
                'border' => false,
                'hpadding' => 'auto',
                'vpadding' => 'auto',
                'fgcolor' => array(0,0,0),
                'bgcolor' => false,
                'text' => true,
                'font' => 'times',
                'fontsize' => 8,
                'stretchtext' => 1
            );
           
            $pdf->write1DBarcode($OrderManifestDetails['awb_no'], 'C39', '108', '35', '', 25, 0.4, $style, 'N');
    
            $html = '';
            $html .='';
            $block3 ='  <b> DELIVER TO :</b><br>
                        '.$Deliveryaddress['firstname'].''.$Deliveryaddress['lastname'].'<br>
                        '.preg_replace('/\s+/', '', $OrderManifestDetails['shipping_address']).'<br>
                        '.$Deliveryaddress['city'].'<br>
                        <b>'.$OrderManifestDetails['destination'].'</b><br>
                        '.($Deliveryaddress['state_name']).'<br>
                        '.$Deliveryaddresss['postcode'].'
                        Phone : '.$Deliveryaddress['phone'].'
                        ';
            $pdf->writeHTMLCell(70, 50, 15, 80, $block3, 0, 1, 0, true, 'J', true);
                        
            $block4 = ' <b>ORDER ID :</b><br>';
            $pdf->write1DBarcode($id_order, 'C39', '85', '85', '45', 25, 0.4, $style, 'N');
    
            $pdf->writeHTMLCell(0, 10, 95, 80, $block4, 0, 1, 0, true, 'J', true);
    
            $block5 = ' <b> AWB No. : '.$OrderManifestDetails['awb_no'].'</b><br>
                        Weight (kgs) : '.$OrderManifestDetails['weight'].'<br>
                        Dimensions (cms) : 8*5*5<br>
                        Order ID : '.$id_order.' <br>
                        Order Date : '.substr($OrderManifestDetails['created_at'],0,10).'<br>
                        Pieces : '.$ActualTotal['totalquantity'].'<br>
            ';
    
            $pdf->writeHTMLCell(0, 10, 135, 80, $block5, 0, 1, 0, true, 'J', true);
             

            $html .= '<div>
                        <table border="1" width="100%" cellspacing="0" cellpadding="3" style="margin-left:15%;margin-right:15%;">
                                                        <tr>
                                                            <td width="5%">Sr.</td>
                                                            <td width="12%">Item Code</td>
                                                            <td width="10%">EAN Code</td>
                                                            <td width="42%">Item Description</td>
                                                            <td width="10%">Quantity</td>
                                                            <td width="10%">Value</td>
                                                            <td width="10%">Total Amount</td>
                                                        </tr>
                                                    ';
            $j = 1;
    
            foreach($orderDetails as  $key =>$value)
            {
                $total_amount = floatval($value->unit_price_tax_excl);
                $html .= '                          <tr>
                                                        <td>'.$j.'</td>
                                                        <td>'.$value->product_id.'</td>
                                                        <td>'. (!empty($products['ean13']) ? $products['ean13'] : '--' ).'</td>
                                                        <td>'.$value->product_name.'</td>
                                                        <td>'.$value->product_quantity.'</td>
                                                        <td>'.$value->unit_price_tax_excl.'</td>
                                                        <td>'.$total_amount.'</td>
                                                    </tr>';
                $j++;
            }         

            $html .='                       <tr>
                                                <td colspan="4"></td>
                                                <td colspan="2">Order Subtotal</td>
                                                <td >'.$ActualTotal['total_paid_actual'].'</td>
                                            </tr>

                                            <tr>
                                                <td colspan="4"></td>
                                                <td colspan="2">GST Charges</td>
                                                <td align="center">'.$TotalTax['Total_tax'].'</td>
                                            </tr> 
    
                                            <tr>
                                                <td colspan="4"></td>
                                                <td colspan="2">Shipping Charges</td>
                                                <td>'.$DiscountOffer['Shipping'].'</td>
                                            </tr>
                                            
                                            <tr>
                                                <td colspan="4"></td>
                                                <td colspan="2">Discount</td>
                                                <td align="center">-'.$DiscountOffer['DiscountPrice'].'</td>
                                            </tr>  
                                            
                                            <tr>
                                                <td colspan="4"></td>
                                                <td colspan="2">Total</td>
                                                <td><b>'.$collectable_value['total_paid_discount'].'</b></td>
                                            </tr>   
                                        </table>
                                        </div>
                    ';
            $html .= '<br><br>This is computer generated document, hence does not require signature.<br>';
            $html .= 'Write us at support@lovzme.com<br>Return Address : '.$vendorDetails['company_name'].','.$vendorDetails['address_1'].','.$vendorDetails['address_2'].','.$vendorDetails['city'].','.$vendorDetails['pin'];            
            $pdf->SetFillColor(255, 255, 255);
            $pdf->writeHTMLCell(0, 50, 15, 80 , "", 1, 1, 0, true, 'C', true);
            $pdf->writeHTMLCell(0,145,15,130 , $html, 1, 1, 0, true, 'C', true);              
       
             }
         
            ob_end_clean();
            $filename = 'order_'.$id_order.'.pdf';      
            $pdf->Output($filename, 'I');             
                      
    }

    //Get the Pack-Ship PDF of Order 
    public function getbulkpackslipPDF($id_order)
    {
            $id_order_list = $id_order;
            $id_order_array = explode(",", $id_order_list);

            $pdf = $this->tcpdflib->load();
            // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('LOVZME');
            $pdf->SetTitle('Pack-Slip');
            $pdf->SetSubject('Pack-Slip');
            $pdf->SetKeywords('PDF, Pack-Slip');

            $pdf->SetPrintHeader(false);
            $pdf->SetPrintFooter(false);
           
            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // set auto page breaks
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

             // set font
            $pdf->SetFont('times', '', 11);

            // add a page
            $pdf->AddPage();
           
            $table = '<table width="100%">';
                       
            
            // $no =1;
            foreach($id_order_array  as  $key =>$value)
            {  
                $id_order = $value;
                //print_r($id_order);

                 $seller_customer_id     = $this->session->userdata('seller_customer_id');                                         
                 $orderDetails           = $this->Transaction_model->getSellerOrderTablebyID($id_order);
                 $ActualTotal            = $this->Transaction_model->getActuaPriceTotal($id_order);
                 $OrderManifestDetails   = $this->OrderManifestDetails_model->getOrderManifestDetails($id_order);
                 $vendorDetails          = $this->Vendor_model->getVendor($OrderManifestDetails['vendor_id']);
                 $Deliveryaddress        = $this->Address_model->getAddressDetails($orderDetails[0]->id_address_delivery);
                 $collectable_value      = $this->Discount_model->getPriceTotalDiscount($id_order);      
                 $sellerEmail            = $this->ShipDelight_model->getSeller($seller_customer_id);

                $img_file = base_url().'assets/images/lovzme-logo-1518760531.png';
                
                $params = $pdf->serializeTCPDFtagParameters(array($OrderManifestDetails['awb_no'],'C39', '', '','', 25, 0.4, array('position'=>' ', 'align' => 'C','border'=>false, 'padding'=>2, 'fgcolor'=>array(0,0,0), 'text'=>true, 'font'=>'times', 'fontsize'=>8, 'stretchtext'=>1), 'N'));

                $table .=  '    <tr  rowspan="2" >
                                    <td  border="1"><br>
                                        <img src="'.$img_file.'"><br>
                                        <b>'.$vendorDetails['company_name'].'</b><br>
                                        '.$vendorDetails['address_1'].'<br>
                                        '.$vendorDetails['address_2'].'<br>
                                        '.$vendorDetails['city'].'<br>
                                        PIN : '.$vendorDetails['pin'].'<br>
                                        Phone : '.$vendorDetails['contact_no'].'<br>
                                        Email : '.$sellerEmail['email'].'<br>
                                        <b>GSTN :'.$vendorDetails['gstn'].'</b>          
                                    </td>
                                    <td border="1" align="center"><br>                                                       
                                               <br><b>'.$OrderManifestDetails['mode'].' </b><br>
                                                Declared value : '.$OrderManifestDetails['collectable'].'<br><br>
                                           <td align="center" >';
                                            $table .= '<tcpdf method="write1DBarcode" params="'.$params.'" />';
                                            $table .='</td>
                                              <b>AMOUNT TO BE COLLECTED<br>Rs. '.($collectable_value['total_paid_discount'] + 0).'</b>
                                    </td>
                                </tr>
                                <tr rowspan="2" >
                                    <td border="1" ><br>
                                                    DELIVER TO :<br>
                                                    <b>'.$Deliveryaddress['firstname'].''.$Deliveryaddress['lastname'].'</b><br>
                                                    '.preg_replace('/\s+/','',$OrderManifestDetails['shipping_address']).'<br>
                                                    '.$Deliveryaddress['city'].'<br>
                                                    <b>'.$OrderManifestDetails['destination'].'</b><br>
                                                    Phone : '.$Deliveryaddress['phone'].'
                                    </td> 
                                    <td border="1" ><br>
                                                    <b>AWB No. : '.$OrderManifestDetails['awb_no'].'</b><br>
                                                    No of Pack : '.$ActualTotal['totalquantity'].'<br>
                                                    Weight (kgs) : '.$OrderManifestDetails['weight'].'<br>
                                                    Dimensions(cms):8*5*5<br>
                                                    Order ID : '.$id_order.'
                                    </td>      
                                </tr>
                                <tr rowspan="2">
                                    <td  border="1" ><br>
                                                     Pickup Date: '.substr($OrderManifestDetails['pickup_date'],0,10).' <br>
                                    </td> 
                                    <td  border="1" align="center" ><br>
                                                     ORDER ID : <b>'.$id_order.'</b><br>
                                                     Item Type : Clothes<br>
                                                     <!--Product: <font size="9"  style="white-space: nowrap;width: 50px;overflow: hidden;text-overflow: ellipsis;border: 1px solid #000000;"> '.preg_replace('/\s+/', '',$OrderManifestDetails['items']).'</font>-->
                                    </td> 
                                </tr>                                   
                                    
                                <tr rowspan="2" >
                                    <td border="1" align="center" colspan="2" ><br/>
                                           This is computer generated document, hence does not require signature.<br>
                                           Write us at support@lovzme.com</td>
                                           <br>
                                    </tr>
                                <tr colspan="2">
                                   <br><br>
                                </tr>
                               ';            
               
            }
            $table .= '</table >';

            $pdf->writeHTMLCell(0, 0, '', '' , $table, 0, 0, 0, true, 'J', true);

            // move pointer to last page
            $pdf->lastPage();

            // ---------------------------------------------------------
            ob_clean();
            $filename = 'Pack-Slip.pdf';
            //Close and output PDF document
            $pdf->Output($filename, 'I');



            // Create new PDF document
        //     $pdf = $this->tcpdflib->load();
        //     //set document information
        //     $pdf->SetCreator(PDF_CREATOR);
        //     $pdf->SetAuthor('LOVZME');
        //     $pdf->SetTitle('Pack-Slip');
        //     $pdf->SetSubject('Invoice');
        //     $pdf->SetKeywords('PDF, Pack-Slip');
        //     //set margins
        //     $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT); 
        //     $pdf->SetPrintHeader(false);
        //     $pdf->SetPrintFooter(true);
        //     $pdf->SetFooterMargin(20);
        //     $pdf->setFooterFont(Array('times', '', 11));
        //     $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_FOOTER);
        //     // set font
        //     $pdf->SetFont('times', '', 11); 

        // foreach ($id_order_array as  $key =>$value) 
        // {               
        //     $id_order = $value;
                                
        //     $seller_customer_id     = $this->session->userdata('seller_customer_id');                                         
        //     $orderDetails           = $this->Transaction_model->getSellerOrderTablebyID($id_order);
        //     $ActualTotal            = $this->Transaction_model->getActuaPriceTotal($id_order);
        //     $OrderManifestDetails   = $this->OrderManifestDetails_model->getOrderManifestDetails($id_order);
        //     $vendorDetails          = $this->Vendor_model->getVendor($OrderManifestDetails['vendor_id']);
        //     $Deliveryaddress        = $this->Address_model->getAddressDetails($orderDetails[0]->id_address_delivery);
        //     $collectable_value      = $this->Discount_model->getPriceTotalDiscount($id_order);      
        //     $sellerEmail            = $this->ShipDelight_model->getSeller($seller_customer_id);
            
        //     // add a page
        //     $pdf->AddPage();

        //     $img_file = base_url().'assets/images/lovzme-logo-1518760531.png';
           
        //     $block1 = '     <img src="'.$img_file.'"><br>
        //                     <b>'.$vendorDetails['company_name'].'</b><br>
        //                     '.$vendorDetails['address_1'].'<br>
        //                     '.$vendorDetails['address_2'].'<br>
        //                     '.$vendorDetails['city'].'<br>
        //                     PIN : '.$vendorDetails['pin'].'<br>
        //                     Phone : '.$vendorDetails['contact_no'].'<br>
        //                     Email : '.$sellerEmail['email'].'<br>
        //                     <b>GSTN :'.$vendorDetails['gstn'].'</b>  ';
    
        //     if( $OrderManifestDetails['mode'] == 'COD' || $OrderManifestDetails['mode'] == 'Cash on delivery (COD)' || $OrderManifestDetails['mode'] == 'Cash on delivery')
        //     {
        //         $block2_1 = '   <br><br><br><br><br>
        //                         <b>
        //                         AMOUNT TO BE COLLECTED<br>
        //                         Rs. '.($collectable_value['total_paid_discount'] + 0).'
        //                         </b>';
        //     }
    
        //     $block2 = ' <br><br><b>'.$OrderManifestDetails['mode'].' </b><br>
        //                 Declared value : '.$OrderManifestDetails['collectable'].'<br><br>
        //                 '.$block2_1;
    
        //     $pdf->SetFillColor(255, 255, 255);
        //     $pdf->writeHTMLCell(0, 60, 15, 15,$block1, 1, 1, 1, true, 'J', true);
        //     $pdf->writeHTMLCell(0, 65, 105, 15, $block2, 1, 1, 1, true, 'C', true);
    
        //     $style = array(
        //         'position' => '',
        //         'align' => 'C',
        //         'stretch' => false,
        //         'fitwidth' => true,
        //         'cellfitalign' => '',
        //         'border' => false,
        //         'hpadding' => 'auto',
        //         'vpadding' => 'auto',
        //         'fgcolor' => array(0,0,0),
        //         'bgcolor' => false,
        //         'text' => true,
        //         'font' => 'times',
        //         'fontsize' => 8,
        //         'stretchtext' => 1
        //     );
          
        //     $pdf->write1DBarcode($OrderManifestDetails['awb_no'], 'C39', '108', '35', '', 25, 0.4, $style, 'N');
    
        //     $block3 ='  DELIVER TO :<br>
        //                 <b>'.$Deliveryaddress['firstname'].''.$Deliveryaddress['lastname'].'</b><br>
        //                 '.preg_replace('/\s+/', '',$OrderManifestDetails['shipping_address']).'<br>
        //                 '.$Deliveryaddress['city'].'<br>
        //                 <b>'.$OrderManifestDetails['destination'].'</b><br>
        //                 Phone : '.$Deliveryaddress['phone'].'
        //                 ';
        //     $pdf->writeHTMLCell(68, 45, 15, 75, $block3, 1, 1, 1, true, 'J', true);
                        
        //     $block4 = ' ORDER ID : <b>'.$id_order.'</b><br>Product: <font size="9">'.preg_replace('/\s+/', '',$OrderManifestDetails['items']).'</font>';
        
    
        //     $pdf->writeHTMLCell(62, 45, 82, 75, $block4, 1, 1, 1, true, 'J', true);
    
        //     $block5 = ' <b> AWB No. : '.$OrderManifestDetails['awb_no'].'</b><br>
        //                 Pickup Date : '.substr($OrderManifestDetails['created_at'],0,10).'<br>
        //                 No of Pack : '.$ActualTotal['totalquantity'].'<br>
        //                 Weight (kgs) : '.$OrderManifestDetails['weight'].'<br>
        //                 Dimensions(cms):8*5*5<br>
        //                 Order ID : '.$id_order.'';
    
        //     $pdf->writeHTMLCell(0, 15, 144, 81, $block5, 0, 0, 0, true, 'J', true);
        //     $pdf->writeHTMLCell(0, 40, 15,  80, "", 1, 1, 0, true, 'J', true);
        //     $pdf->SetAutoPageBreak(true ,$block5);

        //     // move pointer to last page
        //     // $pdf->lastPage();
             
        //      }
         
        //     ob_end_clean();
        //     $filename = 'Pack_slip.pdf'; 
        //     $pdf->Output($filename, 'I');   

    }
    
    //Get the ManifestDate Custom of Order (From date to date)
    public function getManifestDatecustom($startDate,$endDate)
    {
        //print_r($startDate);
        $sql = "SELECT 
                       order_id
                FROM 
                      ps_orders_manifests 
                WHERE 
                      (`created_at`
                BETWEEN 
                      '$startDate 00:00:01'
                AND   
                      '$endDate 23:59:00')";
        //print_r($sql);
        $query = $this->db->query($sql);
        $result = $query->result();

        foreach ($result as $key => $value) 
        {
            $id_orders[]= $value->order_id;
        }
    
      
        $id_order = implode(",", $id_orders);
        
        $this->getbulkmanifestPDF($id_order);
             
    }

    //Get the (Manifest PDF and Custom date)  of Order 
    public function getbulkmanifestPDF($id_order)
    {

        $id_order_list = $id_order;
        $id_order_array = explode(",", $id_order_list);

        $pdf = $this->tcpdflib->load();
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('LOVZME');
        $pdf->SetTitle('Manifest');
        $pdf->SetSubject('Manifest');
        $pdf->SetKeywords('PDF, Manifest');
       
        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        // set font
        $pdf->SetFont('times', '', 11);

        // add a page
        $pdf->AddPage();

        // set cell padding
        $pdf->setCellPaddings(1, 1, 1, 1);

        // set cell margins
        $pdf->setCellMargins(1, 1, 1, 1);

        // set color for background
        $pdf->SetFillColor(255, 255, 127);

        $title = <<<EOD
        <h2>Manifest Order</h2>
        EOD;
        $pdf->writeHTMLCell(0, 0, '','' ,$title, 0, 1, 0, true, 'C', true);
        $table = '<table border="1" cellpadding="0" cellspacing="0" width="100%">';
        
        $table .= ' <tr style="background-color:#ccc;">            
                        <!--<th style="border: 1px solid #000;">No</th>-->                       
                        <th style="border: 1px solid #000;width:58px;font-weight:bold;">Order-ID</th>
                        <th style="border: 1px solid #000;width:145px;font-weight:boldfont-weight:bold;">Order-Items</th>
                        <th style="border: 1px solid #000;width:135px;font-weight:bold;">S-Address</th>
                        <th style="border: 1px solid #000;font-weight:bold;">P-Mode</th>
                        <th style="border: 1px solid #000;width:75px;font-weight:bold;">AWB-No</th>                                
                        <th style="border: 1px solid #000;font-weight:bold;">Logistic</th>                                
                        <th style="border: 1px solid #000;width:60px;font-weight:bold;">Signature</th>
                    </tr>';
        // $no =1;
        foreach($id_order_array  as  $key =>$value)
        {  
            $id_order = $value;
            //print_r($id_order); 

            $seller_customer_id     = $this->session->userdata('seller_customer_id');

            $orderDetails           = $this->Transaction_model->getSellerOrderTablebyID($id_order);             
            $ActualTotal            = $this->Transaction_model->getActuaPriceTotal($id_order);
            $TotalTax               = $this->Tax_model->getTotalTax($id_order);
            $OrderManifestDetails   = $this->OrderManifestDetails_model->getOrderManifestDetails($id_order);
            $vendorDetails          = $this->Vendor_model->getVendor($OrderManifestDetails['vendor_id']);
            $Deliveryaddress        = $this->Address_model->getAddressDetails($orderDetails[0]->id_address_delivery);  
            $sellerEmail            = $this->ShipDelight_model->getSeller($seller_customer_id);

            $table .=  '<tr>
                        <!--<td style="border: 1px solid #000;">$no++</td>-->
                        <td style="border: 1px solid #000;">'.$OrderManifestDetails['order_id'].'</td>
                        <td style="border: 1px solid #000;font-size:12px;">'.rtrim($OrderManifestDetails['items'],' | ').'</td>
                        <td style="border: 1px solid #000;">'.$OrderManifestDetails['shipping_address'].'</td>
                        <td style="border: 1px solid #000;">'.$OrderManifestDetails['mode'].'</td>
                        <td style="border: 1px solid #000;">'.$OrderManifestDetails['awb_no'].'</td>
                        <td style="border: 1px solid #000;">'.$OrderManifestDetails['shipment_provider'].'</td>
                        <td style="border: 1px solid #000;"> </td>
                     </tr>';
           
        }                        
        $table .= '</table>';

        $title1 = '<h4 style="text-align:right;">*'.$vendorDetails['company_name'].'</h4>'; 
       
        $pdf->writeHTMLCell(0, 0, '','' ,$title1, 0, 1, 0, true, 'C', true);

        $pdf->writeHTMLCell(0, 0, '', '' , $table, 0, 1, 0, true, 'C', true);

        // move pointer to last page
        $pdf->lastPage();

        // ---------------------------------------------------------
        ob_clean();
        $filename = 'Manifest.pdf';
        //Close and output PDF document
        $pdf->Output($filename, 'I');

    }
		

}

/* End of file Pdf_model.php */
/* Location: ./application/modules/Transaction/models/Pdf_model.php */