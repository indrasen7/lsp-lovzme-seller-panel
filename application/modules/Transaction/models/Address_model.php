<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Address_model extends CI_Model 
{
	    public function __construct()
		{
			parent::__construct();
		}
        //Get the address and customer name for Shipdelight API
		public function getAddressDetails($id_address)
		{
		
			$SQL ="SELECT  a.*, 
			               s.name as state_name
                         
					FROM
						ps_address a
					LEFT JOIN ps_state s ON  
						 a.id_state = s.id_state					
			        WHERE 
			             a.id_address =".$id_address."";

            $query = $this->db->query($SQL);
			$data =$query->row_array();
            return $data;
		}

}

/* End of file Address_model.php */
/* Location: ./application/modules/Transaction/models/Address_model.php */