<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ShipDelight_model extends CI_Model 
{
	private $ShipdelightApiKey;
	private $ShipdelightUrl;

	public function __construct()
	{
		parent::__construct();

		$ShipdelightApiKey  = 'LO123';
		$ShipdelightUrl     = 'https://crm.shipdelight.com/index.php/';

		$this->ShipdelightApiKey  = $ShipdelightApiKey;
		$this->ShipdelightUrl     = $ShipdelightUrl;
	}
    
    //Get the seller email for invoice PDF
    public function getSeller($seller_customer_id)
    {
            $sql ="SELECT DISTINCT
                          wms.business_email AS email
                   FROM
                          ps_wk_mp_seller wms
                   WHERE 
                          seller_customer_id = ".$seller_customer_id."";                       
            
            $query = $this->db->query($sql);
            $result = $query->row_array(); 
            return $result; 
    }
    
    //Single order shipping Generation from shipdelight API
	public function getShipDelight( $data )
	{  

	    $seller_customer_id  = $this->session->userdata('seller_customer_id');
 
 		$id_order            = $data['id_order']; 		 			
        $vendorDetails       = $this->Vendor_model->getVendor($seller_customer_id);
 		$orderDetails        = $this->Transaction_model->getSellerOrderTablebyID($id_order);
 		$ShipDelightaddress  = $this->Address_model->getAddressDetails($orderDetails[0]->id_address_delivery);
        $orderproductPrice   = $this->Transaction_model->getActuaPriceTotal($id_order);
        $collectable_value   = $this->Discount_model->getPriceTotalDiscount($id_order); 		   
                   
        $orderID 			   = $orderDetails[0]->id_order;
        $productQuantity 	   = $orderproductPrice['totalquantity'];
        $productPrice          = $orderproductPrice['total_paid_actual'];
        $totalPrice            = $collectable_value['total_paid_discount'];
        $productWeight 		   = $orderDetails[0]->product_weight;
        $invoiceDate 		   = $orderDetails[0]->invoice_date;

        $customerName          = $ShipDelightaddress['firstname'];
        $customerLastname      = $ShipDelightaddress['lastname'];
        $customerAddress1      = $ShipDelightaddress['address1'];
        $customerAddress2      = $ShipDelightaddress['address2'];
        $customerCity          = $ShipDelightaddress['city'];
        $customerPincode       = $ShipDelightaddress['postcode'];
        $customerState         = $ShipDelightaddress['state_name'];
        $customerPhone         = $ShipDelightaddress['phone'];
            
        $vendorID              = $vendorDetails['seller_id'];
        $vendorName            = $vendorDetails['company_name'];
        $vendorAddress1        = $vendorDetails['address_1'];
        $vendorAddress2        = $vendorDetails['address_2'];
        $vendorCity            = $vendorDetails['city'];
        $vendorPincode         = $vendorDetails['pin'];
        $vendorState      	   = $vendorDetails['state'];
        $vendorPhone       	   = $vendorDetails['contact_no'];
                  
        $iso_order_date        = date(DATE_ISO8601, strtotime($invoiceDate));
        $payment_mode          = $orderDetails[0]->payment;


        foreach($orderDetails as $key =>$value)
        {
            $product_ids[]   = $value->product_id;
                
            $product_names[] = $value->product_name;               
        }          

            $items_name = implode(", ",str_replace("'"," ",$product_names));
            $items_id = implode(", ",str_replace("'"," ",$product_ids));
    

        if($payment_mode != 'Cash on delivery (COD)')
        {
            $payment_mode = 'PPD';
            $collectable_value = 0;
        }
        else
        {
            $payment_mode = 'COD';
            $collectable_value = $totalPrice;
        }

        if( $payment_mode == 'CCAVENUE MCPG' )
        {
            $payment_mode = 'PPD';
            $collectable_value = 0;
        }
        
        if( $payment_mode == 'paytm' )
        {
            $payment_mode = 'PPD';
            $collectable_value = 0;
        }

        //Order Shipping generation AWB shipdelight API  
        $url = $this->ShipdelightUrl."alltracking/create_shipment/doUpload";
        $Parameters = array(
								'api_key' => $this->ShipdelightApiKey,                       //ALWAYS USE THIS KEY
								'transaction_id' => '',	             
								'order_no' => $orderID,					                             //MANDATORY FIELD
								'consignee_first_name' => $customerName,                     //MANDATORY FIELD
								'consignee_last_name' => $customerLastname,	
								'consignee_address1' => $customerAddress1,	                 //MANDATORY FIELD
								'consignee_address2' => $customerAddress2,			
								'destination_city' => $customerCity,			                  //MANDATORY FIELD
								'destination_pincode' => $customerPincode,		              //MANDATORY FIELD
								'state' => $customerState,				                            //MANDATORY FIELD
								'telephone1' => $customerPhone,			                        //MANDATORY FIELD
								'telephone2' => '',	
								'vendor_name' => $vendorName,		                            //ALWAYS USE THIS //NAME	
						        'vendor_address' => $vendorAddress1.' '.$vendorAddress2,	  //MANDATORY FIELD
								'vendor_city' => $vendorCity,				                        //MANDATORY FIELD
								'pickup_pincode' => $vendorPincode,			                    //MANDATORY FIELD
								'vendor_phone1' => $vendorPhone,		                        //MANDATORY FIELD
								'pay_type' => $payment_mode,					                      //MANDATORY FIELD
								'item_description' =>$items_name,				                    //MANDATORY FIELD				     
								'qty' => $productQuantity,						                      //MANDATORY FIELD
								'collectable_value' => $collectable_value,			            //MANDATORY FIELD
								'product_value' => $productPrice,				                    //MANDATORY FIELD
								'actual_weight' => $productWeight,				                  //MANDATORY FIELD
								'volumetric_weight' => '1',
								'length' => '8',
								'breadth' => '5',
								'height' => '5'
                            );
       
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $Parameters);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $awb_json = curl_exec($ch);
        curl_close($ch); 
        $result = json_decode($awb_json);
        
        $AWBno = $result->response[0]->awbno;
       
        $ShipName = $result->response[0]->Courier;
        
        $pickup_date =  date("Y-m-d H:i:s");
        $day = date('D',strtotime($pickup_date));
            if($day == 'Sun')
            {
                $pickup_date = date('Y-m-d', strtotime(' +1 day'));
            }
          

        $insertData = array(
                                'order_id' => $id_order,
                                'awb_no'=> $AWBno,
                                'vendor_id' => $vendorID, /*$user->ID*/
                                'shipment_provider' => $ShipName,
                                'customer_name'=> $customerName.''.$customerLastname,
                                'shipping_address'=> $customerAddress1.''.$customerAddress2,
                                'pin_code'=> $customerPincode,
                                'items_id'=> $items_id,
                                'items'=>rtrim($items_name,'|'),
                                'weight'=> $productWeight,
                                'declared_value'=> $totalPrice,
                                'collectable'=> $collectable_value,
                                'mode'=> $payment_mode,
                                'destination'=> $customerPincode,
                                'pickup_date'=> $pickup_date,
                                'token_number'=>'4556',
                                'created_at'=>date("Y-m-d H:i:s")
                            );

        $this->db->insert('ps_orders_manifests', $insertData);
        
        $carriername = $ShipName;
        $carrierdetails  = $this->Transaction_model->getcarrier($carriername);
        $id_carrier  =   $carrierdetails[0]->id_carrier;

        $current_state = 14;        
        
        $this->updateorderstatus($id_order,$current_state);               

        $this->updateOrderCarrier($id_order,$id_carrier,$AWBno);

        $this->updatesellerOrderStatus($id_order,$current_state,$AWBno);

        $adminorderhistory = array(
                                    'id_employee' => '0',
                                    'id_order'=> $id_order,
                                    'id_order_state' => $current_state, 
                                    'date_add'=>date("Y-m-d H:i:s")
                                  );

        $this->db->insert('ps_order_history', $adminorderhistory);
  
        $seller_customer_id = $this->session->userdata('seller_customer_id');

        $sellerorderhistory = array(
                                
                                    'id_order'=> $id_order,
                                    'id_seller' => $seller_customer_id,
                                    'id_order_state' => $current_state, 
                                    'date_add'=>date("Y-m-d H:i:s")
                                   );

        $this->db->insert('ps_wk_mp_seller_order_history', $sellerorderhistory);  
 		
        //Email and SMS for customer 
        $orderid = $id_order;
        $this->Lovzmemail_model->orderMail($orderid);
        $this->Lovzmesms->smsDetails($orderid);
 	}

    //Update order status from process to ready to ship (ADMIN)
    public function updateorderstatus($id_order,$current_state)
    {
        $SQL = "UPDATE 
                        `ps_orders` 
                SET 
                        `current_state` = $current_state 
                WHERE
                        `id_order` = $id_order";
               
        if( $this->db->query($SQL))
        {
            return true;
        }
        else
        {
            return false;
        }
       
    }
    
    //Update order Carrier for change in admin backend status (id_carrier ,awbno) (ADMIN)
    public function updateOrderCarrier($id_order,$id_carrier,$AWBno)
    {
        $SQL1 = "UPDATE 
                        `ps_order_carrier` 
                SET 
                         id_carrier = $id_carrier , tracking_number = $AWBno
                WHERE
                        `id_order` = $id_order";
       
        $SQL2 = "UPDATE 
                        `ps_orders` 
                SET 
                        `id_carrier` = $id_carrier 
                WHERE
                        `id_order` = $id_order";

        if( $this->db->query($SQL1) && $this->db->query($SQL2) )
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    //Update Seller order Carrier for change in admin backend status (current_state ,awbno) (SELLER)
    public function updatesellerOrderStatus($id_order,$current_state,$AWBno)
    {
        $SQL = "UPDATE 
                        `ps_wk_mp_seller_order_status` 
                SET 
                         current_state = $current_state , tracking_number = $AWBno
                WHERE
                        `id_order` = $id_order";
        
        if( $this->db->query($SQL))
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    //Insert order status (CRON status)
    public function insertorderhistory($id_order_state,$id_order)
    {
       
        $historydata1 =      array(
                                    'id_employee' => '0',
                                    'id_order'=> $id_order,
                                    'id_order_state' => $id_order_state, 
                                    'date_add'=>date("Y-m-d H:i:s")
                                  );

        $this->db->insert('ps_order_history', $historydata1);
  
        $seller_customer_id = $this->session->userdata('seller_customer_id');
       
        $historydata2 =      array(
                                
                                    'id_order'=> $id_order,
                                    'id_seller' => $seller_customer_id,
                                    'id_order_state' => $id_order_state, 
                                    'date_add'=>date("Y-m-d H:i:s")
                                   );

        $this->db->insert('ps_wk_mp_seller_order_history', $historydata2);  
    }      

    //Bulk order shipping Generation from shipdelight API
    public function BulkgetShipDelight( $data )
    {
         $orderid  = explode(",", $data);         

         foreach ($orderid as $OrderIDs) 
         {  
             $OrderIDs = [ 
                            'id_order' => $OrderIDs,
                         ];
                         
            $this->getShipDelight($OrderIDs);
         }
    }
    
    //Check the pincode serviceable or not hide button
    public function Shipdelightpincode($pincode)
    {
        //Serviceable Pincode Check  API
        $url = $this->ShipdelightUrl."alltracking/check_pincode/pincode_detail";
        $Parameters = array(
                                'api_key' => $this->ShipdelightApiKey,       //ALWAYS USE THIS KEY
                                'pincode' => $pincode,                      //SIX DIGIT PINCODE          
                            );

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $Parameters);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);
        $pincode_result = json_decode($result);
        curl_close($ch);

        $pincode_result = (array)$pincode_result;

        if( isset($pincode_result) && isset($pincode_result[0]) )
        {
            if($pincode_result[0]->city != null || $pincode_result[0]->state != null)
            {
            // echo "no error";
            return true;
            }
            else
            {
            // echo "error here";
            return false;
            }
        }
        else
        {
            // echo "its error";
            return false;
        }

    } 
   
}

/* End of file ShipDelight_model.php */
/* Location: ./application/modules/Transaction/models/ShipDelight_model.php */