<?php
	class Transaction_model extends CI_Model
	{   
		public function __construct()
		{
			parent::__construct();
		}

		//Get the selLer name form database table
		public function getSeller($seller_customer_id)
		{
			$this->db->where('seller_customer_id',$seller_customer_id);
			$this->db->select('CONCAT(seller_firstname," "'.' , seller_lastname) AS name');
			$this->db->from('ps_wk_mp_seller');
			$query = $this->db->get();
			return $query->result()[0]->name;
		}

		//Get the seller gender form database table
		public function getGender($id_customer)
		{
			$this->db->where('id_customer',$id_customer);
			$this->db->select('id_gender');
			$this->db->from('ps_customer');
			$query = $this->db->get();
			$data =$query->result();
			return $data[0]->id_gender;
		}

        //Get the count customer from database 
		public function getTotalcustomer($seller_customer_id)
		{
			$sql ="SELECT DISTINCT
			            
			             COUNT(CONCAT(cu.firstname,' ', cu.lastname)) AS customer_name
			            
					FROM
						ps_orders o
					LEFT JOIN ps_wk_mp_seller_order_history soh ON 
					     o.id_order = soh.id_order
                    LEFT JOIN ps_customer cu ON 
                     	 o.id_customer = cu.id_customer
					LEFT JOIN ps_order_state_lang osl ON  
						 o.current_state = osl.id_order_state 
					LEFT JOIN ps_wk_mp_seller wms ON 
						 wms.id_seller = soh.id_seller
			        WHERE 
			             wms.seller_customer_id = ".$seller_customer_id." ";
			 
			 $query = $this->db->query($sql);		
			 $data = $query->result();
			 return $data[0]->customer_name;
		}

        //Get the total order from database
		public function getTotalorder($seller_customer_id)
		{
			$sql = "SELECT
			               COUNT(soh.id_mp_order_detail) AS total_orders
						
					FROM
						 ps_wk_mp_seller_order_detail soh

					WHERE soh.seller_customer_id = ".$seller_customer_id." ";
             
             $query = $this->db->query($sql);		
			 $data = $query->result();
			 return $data[0];
		}

        //Get the total earn of seller from database
		public function getSellerTotalEarn($seller_customer_id)
		{
			$sql = "SELECT
                           SUM(soh.seller_amount) AS seller_amount
                    FROM
                           ps_wk_mp_seller_order_detail soh
                    WHERE  
                           soh.seller_customer_id = ".$seller_customer_id." ";

			 $query = $this->db->query($sql);		
			 $data = $query->result();
			 return $data[0]->seller_amount;
		}

        //Get the total products of seller from database
		public function getTotalproduct($seller_customer_id)
		{
			$sql = "SELECT
			               SUM(soh.quantity) AS total_products
						
					FROM
						 ps_wk_mp_seller_order_detail soh

					WHERE soh.seller_customer_id = ".$seller_customer_id."
					";
            	
			 $query = $this->db->query($sql);		
			 $data = $query->result();
			 return $data[0];

		}
		
        //Get the seller order table form database table
		public function getSellerOrderTable($seller_customer_id)
		{
			$SQL = "SELECT DISTINCT
			             o.id_order,
			             o.reference AS reference,
			             CONCAT(cu.firstname,' ', cu.lastname) AS customer_name,
			             ROUND(o.total_paid ,2)AS total_paid,
			             osl.name AS order_status,
			             REPLACE(SUBSTRING(o.payment,1,23),'|','') AS payment,
						 DATE(o.delivery_date) AS order_date,
						 om.awb_no,
						 IF(ISNULL(om.awb_no), '', 'disabled') AS checkbox_status
					FROM
						ps_orders o
					LEFT JOIN ps_wk_mp_seller_order_history soh ON 
					     o.id_order = soh.id_order
                    LEFT JOIN ps_customer cu ON 
                     	 o.id_customer = cu.id_customer
					LEFT JOIN ps_order_state_lang osl ON  
						 o.current_state = osl.id_order_state 
					LEFT JOIN ps_wk_mp_seller wms ON 
						 wms.id_seller = soh.id_seller
					LEFT JOIN ps_orders_manifests om ON 
                          om.order_id = o.id_order
			        WHERE 
			             wms.seller_customer_id = ".$seller_customer_id."
			        ORDER BY o.id_order DESC";
		
            return $this->db->query($SQL)->result(); 
			
		}

		//Get the carrier id by carrier name (ADMIN)
	    public function getcarrier($carriername)
	    {
	        $SQL = "SELECT 
	                        * 
	                FROM ps_carrier ca 
	                WHERE ca.`name` 
	                LIKE '%$carriername%' 
	                LIMIT 0,1 ";
	            
	        $query = $this->db->query($SQL);
	        $result = $query->result(); 
	        return $result; 

	    }
         
		//Get the seller order details form database table
		public function getSellerOrderTablebyID($id_order)
		{
			$SQL = "SELECT DISTINCT
			             o.*,
			             cu.email,
			             od.product_id,
			             od.product_name,
			             ROUND(od.unit_price_tax_excl , 2) AS unit_price_tax_excl,
			             ROUND(t.rate ,0) AS TaxPercentage,                        
			             ROUND(od.unit_price_tax_incl , 2) AS unit_price_tax_incl,
                         od.product_quantity,
                         SUM(od.product_quantity) AS total_product_quantity,
                         od.product_weight,
                         osl.name AS order_status,
                         ROUND(od.total_price_tax_excl, 2) AS total_price_tax_excl                       
			            			            			            						
					FROM
						ps_orders o
					LEFT JOIN ps_wk_mp_seller_order_history soh ON 
					     o.id_order = soh.id_order
                    LEFT JOIN ps_customer cu ON 
                     	 o.id_customer = cu.id_customer
					LEFT JOIN ps_order_state_lang osl ON  
						 o.current_state = osl.id_order_state 
					LEFT JOIN ps_wk_mp_seller wms ON 
						 wms.id_seller = soh.id_seller
				    LEFT JOIN ps_order_detail od ON 
						 od.id_order = o.id_order
				    LEFT JOIN ps_order_detail_tax odt ON 
						 odt.id_order_detail = od.id_order_detail
					LEFT JOIN ps_tax t ON
                          t.id_tax = odt.id_tax
			        WHERE 
			             o.id_order = ".$id_order."
			        GROUP BY 
			             od.id_order_detail";
		   
            $query = $this->db->query($SQL);
			$result = $query->result(); 
			return $result;				
		}

		

		//Get the shipping/invoice address of customer order details
		public function getAddress($id_order)
		{
			$sql = "SELECT DISTINCT
			             a.address1,
                         a.address2,
                         a.postcode,
                         s.name AS state,
                         a.city,
                         a.phone

                    FROM 
                        ps_orders o 

                    LEFT JOIN ps_address a ON
                          a.id_address = o.id_address_delivery
                    LEFT JOIN ps_state s ON
                          s.id_state = a.id_state
                    WHERE
                         o.id_order = ".$id_order."";

            $query = $this->db->query($sql);
			$result = $query->row_array(); 
			return $result;	
			
		}

		//Get the sum total of product order Actual price
		public function getActuaPriceTotal($id_order)
		{
			$sql = "SELECT DISTINCT
			             ROUND(o.total_products, 2) AS total_paid_actual,
			             SUM(od.product_quantity) as totalquantity

			        FROM
			             ps_orders o

			        LEFT JOIN ps_order_detail od ON 
						 od.id_order = o.id_order

			        WHERE
			             o.id_order =".$id_order."";

			$query = $this->db->query($sql);
			$result = $query->row_array();
			return $result;
		}	
		
	}

?>
