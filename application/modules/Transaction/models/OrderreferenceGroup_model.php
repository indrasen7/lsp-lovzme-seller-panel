<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OrderreferenceGroup_model extends CI_Model 
{   
	public function __construct()
	{
		parent::__construct();
	}

    //Get the Reference name for order details unique
	public function getSellerReferenceName($id_order)
	{
		$SQL = "SELECT DISTINCT			      
			              o.reference as reference			                            			            
			            			            						
				FROM
						  ps_orders o
				LEFT JOIN ps_wk_mp_seller_order_history soh ON 
					     o.id_order = soh.id_order
                LEFT JOIN ps_customer cu ON 
                     	 o.id_customer = cu.id_customer
				LEFT JOIN ps_order_state_lang osl ON  
						 o.current_state = osl.id_order_state 
				LEFT JOIN ps_wk_mp_seller wms ON 
						 wms.id_seller = soh.id_seller
				LEFT JOIN ps_order_detail od ON 
						 od.id_order = o.id_order
					
			    WHERE 
			             o.id_order = ".$id_order."
			    GROUP BY
					     o.reference";
		  
        $query = $this->db->query($SQL);
	    $result = $query->row_array(); 
		return $result;				
	}
	
}

/* End of file OrderreferenceGroup_model.php */
/* Location: ./application/modules/Transaction/models/OrderreferenceGroup_model.php */