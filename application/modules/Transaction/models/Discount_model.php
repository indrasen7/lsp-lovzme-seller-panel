<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Discount_model extends CI_Model 
{ 
	public function __construct()
	{
		parent::__construct();
	}

	//Get the sum total of product order with discount
	public function getPriceTotalDiscount($id_order)
	{
		$sql = "SELECT DISTINCT
			            ROUND(o.total_paid, 2) AS total_paid_discount
			    FROM
			            ps_orders o
			    WHERE
			            o.id_order =".$id_order."";

		$query = $this->db->query($sql);
		$result = $query->row_array();
		return $result;
	}
	
	//Get the discount Offer name in order details
	public function getDiscountShip($id_order)
	{
		$sql = "SELECT DISTINCT
			              ocr.name AS discount_offer,
			              ROUND(o.total_discounts_tax_excl, 2) AS DiscountPrice,
			              ROUND(o.total_shipping_tax_incl , 2) AS Shipping,
			              ROUND(t.rate ,0) AS TaxPercentage

			        FROM
			              ps_orders o
                                        
			        LEFT JOIN ps_order_cart_rule ocr ON
                          ocr.id_order = o.id_order
                    LEFT JOIN ps_order_detail od ON 
						 od.id_order = o.id_order
                    LEFT JOIN ps_order_detail_tax odt ON 
						 odt.id_order_detail = od.id_order_detail
					LEFT JOIN ps_tax t ON
                          t.id_tax = odt.id_tax
                    WHERE
			             o.id_order =".$id_order."";

		$query = $this->db->query($sql);
		$result = $query->row_array();
		return $result;
	}	

}

/* End of file Discount_model.php */
/* Location: ./application/modules/Transaction/models/Discount_model.php */