<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends CI_Model 
{
	public function __construct()
	{
		parent::__construct();	
	}
	
	public function getShopName()
	{
		$sql = "SELECT * FROM ps_configuration WHERE name = 'PS_SHOP_NAME';";
		$query = $this->db->query($sql);
		$result = $query->result_array()[0]['value'];
		return $result;		
	}

	public function getCarrierByID($id_carrier)
	{
		$sql = "SELECT `name` FROM ps_carrier WHERE `id_carrier` = $id_carrier;";
		$query = $this->db->query($sql);
		$result = $query->result_array()[0]['name'];
		return $result;
	}

	public function getSiteURL()
	{
		$sql = "SELECT `value` FROM ps_configuration WHERE name = 'PS_SHOP_DOMAIN';";
		$query = $this->db->query($sql);
		$result = $query->result_array()[0]['value'];
		return $result;
	}

	public function getShopMaint()
	{
		$sql = "SELECT value FROM ps_configuration WHERE name = 'PS_SHOP_ENABLE';";
		$query = $this->db->query($sql);
		$result = $query->result_array()[0]['value'];

		if( $result != 1 )
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}

/* End of file Shop.php */
/* Location: ./application/models/Shop.php */