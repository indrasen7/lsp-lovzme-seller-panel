<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Delhivery_model extends CI_Model 
{
	private $DelhiveryApiKey;
	private $DelhiveryUrl;

	public function __construct()
	{
		parent::__construct();

		$DelhiveryApiKey = 'f96c7887bb926c3afabc195b52d9a35c9eb0524f';
		$DelhiveryUrl = 'https://test.delhivery.com/';

		$this->DelhiveryApiKey = $DelhiveryApiKey;
		$this->DelhiveryUrl = $DelhiveryUrl;
	}

    //Get the seller email for invoice PDF
    public function getSeller($seller_customer_id)
    {
            $sql ="SELECT DISTINCT
                          wms.business_email AS email
                   FROM
                          ps_wk_mp_seller wms
                   WHERE 
                          seller_customer_id = ".$seller_customer_id."";                       
            
            $query = $this->db->query($sql);
            $result = $query->row_array(); 
            return $result; 
    }

	public function getDelhivery( $data )
	{  

        $seller_customer_id  = $this->session->userdata('seller_customer_id');
            // print_r($data);
        $id_order            = $data;                   
        $vendorDetails       = $this->Vendor_model->getVendor($seller_customer_id);
        $orderDetails        = $this->Transaction_model->getSellerOrderTablebyID($id_order);
        $Delhiveryaddress    = $this->Address_model->getAddressDetails($orderDetails[0]->id_address_delivery);
        $orderproductPrice   = $this->Transaction_model->getActuaPriceTotal($id_order);
        $collectable_value   = $this->Discount_model->getPriceTotalDiscount($id_order);            
                   
        $orderID               = $orderDetails[0]->id_order;
        $productQuantity       = $orderproductPrice['totalquantity'];
        $productPrice          = $orderproductPrice['total_paid_actual'];
        $totalPrice            = $collectable_value['total_paid_discount'];
        $productWeight         = $orderDetails[0]->product_weight;
        $invoiceDate           = $orderDetails[0]->invoice_date;

        $customerName          = $Delhiveryaddress['firstname'];
        $customerLastname      = $Delhiveryaddress['lastname'];
        $customerAddress1      = $Delhiveryaddress['address1'];
        $customerAddress2      = $Delhiveryaddress['address2'];
        $customerCity          = $Delhiveryaddress['city'];
        $customerPincode       = $Delhiveryaddress['postcode'];
        $customerState         = $Delhiveryaddress['state_name'];
        $customerPhone         = $Delhiveryaddress['phone'];
            
        $vendorID              = $vendorDetails['seller_id'];
        $vendorName            = $vendorDetails['delhivery_username'];
        $vendorAddress1        = $vendorDetails['address_1'];
        $vendorAddress2        = $vendorDetails['address_2'];
        $vendorCity            = $vendorDetails['city'];
        $vendorPincode         = $vendorDetails['pin'];
        $vendorGstn            = $vendorDetails['gstn'];
        $vendorState           = $vendorDetails['state'];
        $vendorPhone           = $vendorDetails['contact_no'];
                  
        $iso_order_date        = date(DATE_ISO8601, strtotime($invoiceDate));
        $payment_mode          = $orderDetails[0]->payment;


        foreach($orderDetails as $key =>$value)
        {
            $product_ids[]   = $value->product_id;
                
            $product_names[] = $value->product_name;               
        }          

            $items_name = implode(", ",str_replace("'"," ",$product_names));
            $items_id = implode(", ",str_replace("'"," ",$product_ids));
    

        if($payment_mode != 'Cash on delivery (COD)')
        {
            $payment_mode = 'PPD';
            $collectable_value = 0;
        }
        else
        {
            $payment_mode = 'COD';
            $collectable_value = $totalPrice;
        }

        if( $payment_mode == 'CCAVENUE MCPG' )
        {
            $payment_mode = 'PPD';
            $collectable_value = 0;
        }
        
        if( $payment_mode == 'paytm' )
        {
            $payment_mode = 'PPD';
            $collectable_value = 0;
        }

       
        $url = $this->DelhiveryUrl."cmu/push/json/?token=".$this->DelhiveryApiKey;

        $params = array(); // this will contain request meta and the package feed
        $package_data = array(); // package data feed
        $shipments = array();
        $pickup_location = array();
        /////////////start: building the package feed/////////////////////
        $shipment = array();
        $shipment['waybill'] = ''; // waybill number
        $shipment['name'] = $customerName.''.$customerLastname; // consignee name
        $shipment['order'] = $orderID; // client order number $order_id
        $shipment['products_desc'] = rtrim($items_name,',');
        $shipment['order_date'] = $iso_order_date; // ISO Format
        $shipment['payment_mode'] = $payment_mode;//'COD';
        $shipment['total_amount'] = $collectable_value; // in INR
        $shipment['cod_amount'] = $collectable_value; // amount to be collected, required for COD
        $shipment['add'] = $customerAddress1.''.$customerAddress2;//'M25, Nelson Marg, GBP City Phase 1'; // consignee address
        $shipment['city'] = $customerCity;//'Gurgaon';
        $shipment['state'] = $customerState;//'Haryana';
        $shipment['country'] = 'India';//'India';
        $shipment['phone'] = $customerPhone;//'9741119727';
        $shipment['pin'] = $customerPincode;//'110038';
        $shipment['quantity'] = $productQuantity; // quanitity of quantity
        $shipment['seller_name']= $vendorName; //name of seller
        $shipment['seller_add']= $vendorAddress1.' '.$vendorAddress2;//'A46 NEHRU NAGAR, Delhi NCR'; // add of seller
        $shipment['seller_cst'] = ''; //cst number of seller
        $shipment['seller_tin'] = $vendorGstn; //tin number of seller
        $shipment['seller_inv']= $orderID; // invoice number of shipment
        $shipment['seller_inv_date']= $iso_order_date; // ISO Format
        // pickup location information //
        $pickup_location['add'] = $vendorAddress1.' '.$vendorAddress2;
        $pickup_location['city'] = $vendorCity;
        $pickup_location['country'] = 'India';
        $pickup_location['name'] = $vendorName; // Use client warehouse name
        $pickup_location['phone'] = $vendorPhone;
        $pickup_location['pin'] = $vendorPincode;
        $pickup_location['state'] = $vendorState;
        $shipments = array($shipment);
        $package_data['shipments'] = $shipments;
        $package_data['pickup_location'] = $pickup_location;
        $params['format'] = 'json';
        $params['data'] =json_encode($package_data);
       
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($params));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        $result_json = curl_exec($ch);       
        curl_close($ch);
        $result = json_decode($result_json);
        // echo "<pre>";
        // var_dump($result);
        // echo "<pre>";
        $status = $result->packages[0]->status;

        if( $status == 'Fail' )
        {                               
            $errors = $result->packages[0]->remarks;
            foreach ($errors as $error)
                {
                    $error_msg = $error;
                }
        //print_r($errors);
        exit();
        }    
        else /*IF ELSE FOR SUCCESS-FAILED API RESPONSE*/
        {
            //PICKUP SHIPMENT CODE
            $pickup['pickup_time'] = "13:15:00";
            $pickup['pickup_date'] = date('Y-m-d', strtotime(' +1 day'));
            $pickup['pickup_location'] = $vendorName;//'LOVZME';//$Warehouse_name;
            $pickup['expected_package_count'] = "1";
            $data =json_encode($pickup);

            $pickup_url = $DelhiveryUrl."fm/request/new/";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $pickup_url);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2); 
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Authorization: Token '.$this->DelhiveryApiKey.''));
            $pickup_json = curl_exec($ch);
            curl_close($ch);   
            $pickup_result = json_decode($pickup_json); 

            $AWBno = $result->packages[0]->waybill;
            $des_area =$result->packages[0]->sort_code;
                
            if( $pickup_result->pr_exist == 'true' )
            {
                $pickup_date =  $pickup['pickup_date'];
                $day = date('D',strtotime($pickup_date));
                    if($day == 'Sun')
                    {
                           $pickup_date = date('Y-m-d', strtotime(' +2 day'));
                    }
            }
            else
            {
                    $pickup_date = $pickup_result->pickup_date;
                       
            }

            $token_number = $pickup_result->pickup_id;

            $insertData = array(
                                    'order_id' => $id_order,
                                    'awb_no'=> $AWBno,
                                    'vendor_id' => $vendorID, /*$user->ID*/
                                    'shipment_provider' => $ShipName,
                                    'customer_name'=> $customerName.''.$customerLastname,
                                    'shipping_address'=> $customerAddress1.''.$customerAddress2,
                                    'pin_code'=> $customerPincode,
                                    'items_id'=> $items_id,
                                    'items'=>rtrim($items_name,'|'),
                                    'weight'=> $productWeight,
                                    'declared_value'=> $totalPrice,
                                    'collectable'=> $collectable_value,
                                    'mode'=> $payment_mode,
                                    'destination'=> $des_area,
                                    'pickup_date'=> $pickup_date,
                                    'token_number'=> $token_number,
                                    'created_at'=>date("Y-m-d H:i:s")
                                );

            $this->db->insert('ps_orders_manifests', $insertData);
	    }

 	}

    //Check the pincode serviceable or not hide button
    public function Delhiverypincode($pincode)
    {
            //Serviceable Pincode Check  API
            $url = "https://track.delhivery.com/c/api/pin-codes/json/?token=".$this->DelhiveryApiKey."&filter_codes=".$pincode;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2); //1 to check the existence of a common name in the SSL peer certificate.
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); //This option tells cURL to verify the authenticity of the SSL cert on the server.
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            $pincode_json = curl_exec($ch);
            $pincode_result = json_decode($pincode_json); 
            curl_close($ch);            
            // echo "<pre>";
            //print_r($pincode_result->delivery_codes[0]->postal_code->pickup = Y);
            // echo "<pre>";
            // $pincode_result = (array)$pincode_result;
            //print_r($pincode_result);
            if( isset($pincode_result->delivery_codes) && isset($pincode_result->delivery_codes[0] ) )
            {
                if($pincode_result->delivery_codes[0]->postal_code->pickup == "Y")
                {
                 //echo "no error";
                   return true;
                }
                else
                {
                   //echo "error here";
                   return false;
                }
            }
            else
            {
                // echo "its error";
                return false;
            }

    } 
	

}

/* End of file Delhivery_model.php */
/* Location: ./application/modules/Transaction/models/Delhivery_model.php */