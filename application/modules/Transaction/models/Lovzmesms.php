<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lovzmesms extends CI_Model 
{
	private $sms_key;
	private $sender;

	public function __construct()
	{
		parent::__construct();

		$this->sms_key = SMS_API_KEY;
		$this->sender = "LOVZME";
	}

	public function sendSMS($params)
	{
		$url = "http://www.smsalert.co.in/api/push.json?";

		if( SMS_GATEWAY_ENABLE == TRUE )
		{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url.$params);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
			$result = curl_exec($ch);
			curl_close($ch);
		}
	}

	public function smsDetails($order)
	{
		$id_order            = $order;
        $orderDetails        = $this->Transaction_model->getSellerOrderTablebyID($id_order);
 		$ShipDelightaddress  = $this->Address_model->getAddressDetails($orderDetails[0]->id_address_delivery);
        
 		$shop_name			 = $this->Shop->getShopName();
		$firstname			 = $ShipDelightaddress['firstname'];
		$phone               = $ShipDelightaddress['phone'];

 		// $msg = "Order Received: Hello ".$ShipDelightaddress['firstname'].", we have received your order ".$id_order." amounting to ".$orderDetails[0]->total_paid." We will send you an update when your order is shipped.";
 		$msg = "Hello ".$firstname.", status of your #".$id_order." with ".$shop_name." has been changed to: Ready to Ship .";
		
		$params = http_build_query([
					'apikey'	=> $this->sms_key,
					'sender'	=> $this->sender,
					'mobileno'	=> $phone,
					'text'		=> $msg
			]);

		$this->sendSMS($params);

	}

}

/* End of file Lovzmesms.php */
/* Location: ./application/models/Lovzmesms.php */