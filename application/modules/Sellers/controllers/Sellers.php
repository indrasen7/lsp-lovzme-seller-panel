<?php
     defined('BASEPATH') OR exit('No direct script access allowed');
			 // ini_set('max_execution_time', 0); 
			 // ini_set('memory_limit','2048M');
	class Sellers extends MY_Controller 
	{

		public function __construct()
		{
					parent::__construct();
					$this->load->model('Sellers_model');
					$this->load->library('pagination'); // loaded codeigniter pagination liberary
					$this->load->library('datatable'); // loaded my custom 'serverside datatable' library			
        }
        
        public function index()
        {   
		            $seller_customer_id = $this->session->userdata('seller_customer_id');
                                        
                    $data['title']       	= 'LOVZme : Sellers';
					$data['view']      	 	= 'Sellers';
					$data['sellerInfo'] 	= $this->Sellers_model->getsellerInfo($seller_customer_id);
					$data['seller_name'] 	= $this->Sellers_model->getSeller($seller_customer_id);
					$data['seller_gender'] 	= $this->Sellers_model->getGender($seller_customer_id);
					$data['orders']         = $this->Sellers_model->getSellerTable($seller_customer_id);
					$data['products']       = $this->Sellers_model->getSellerProductTable($seller_customer_id);
					$data['total_products'] = $this->Sellers_model->getTotalproduct($seller_customer_id);               
					$data['total_orders']   = $this->Sellers_model->getTotalorder($seller_customer_id);
    
			        $this->load->view('layout', $data);
        }

	}