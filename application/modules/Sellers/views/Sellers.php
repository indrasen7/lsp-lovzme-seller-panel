
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><i class="fa fa-user"></i>
        Profile
         <small>Seller</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('Dashboard');?>"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Seller List</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
                      <?php 
                        if ($seller_gender==1) 
                        {
                          echo ' <img  class="profile-user-img img-responsive img-circle" style="margin: 0 auto;width:110px;padding:3px;border:3px solid #d2d6de;" src=" '.base_url().'assets/dist/img/male.png"/>';
                        } 
                        else 
                        {
                          echo ' <img  class="profile-user-img img-responsive img-circle" style="margin: 0 auto;width:110px;padding:3px;border:3px solid #d2d6de;" src=" '.base_url().'assets/dist/img/girl.png"/>';
                        }
                      ?> 
              <h4 class="profile-username text-center"><?=$seller_name;?></h4>
              <h5 class="profile-username text-center">Active</h5>
              <h4  class="profile-username text-center">
                      <?php if($sellerInfo['active']==1)
                        {
                            echo "<span class='glyphicon glyphicon-ok icon-success'style='font-size:15px;color:green'></span>";
                        }
                        else
                        {
                            echo "<span class='glyphicon glyphicon-remove icon-remove'style='font-size:15px;color:red'></span>";
                        } 
                      ?> 
              </h4>  
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Contact Details</h3>
            </div>
            <div class="box-body">
              <strong><i class="fa fa-at margin-r-5"></i> Email</strong>
              <p class="text-muted">
              <a href="mailto:<?=$sellerInfo['email']?>"> <?=$sellerInfo['email'];?> </a>
              </p>
              <hr>
              <strong><i class="fa fa-mobile"></i> Phone</strong>
              <p class="text-muted"><?=$sellerInfo['phone'];?> </p>
              <hr>
              <strong><i class="fa fa-map-marker margin-r-5"></i> Address</strong>                       
              <p class="text-muted"><?=$sellerInfo['address_1'];?> </p>
              <p class="text-muted"><?=$sellerInfo['address_2'];?> </p>
              <hr>
              <strong><i class="fa fa-star"></i> Rating</strong>
              <span class="pull-right-container">
              <span class="label label-primary pull-right">4+</span>
            </span>              
            </div>
            <!-- /.box-body -->
            <!-- /.box-header -->
            <div class="box-body">
                    
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#settings" data-toggle="tab">Additional Details</a></li>
              <li><a href="#2" data-toggle="tab">Orders</a></li>
              <li><a href="#3" data-toggle="tab">Products</a></li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="settings">
                <div class="box-body">
                   <div class="col-md-6">
                        <strong><i class="fa fa-location-arrow margin-r-5"></i> City</strong>
                        <p class="text-muted"><?=$sellerInfo['city'];?></p>
                        <hr>
                         <strong><i class="fa fa-map-marker margin-r-5"></i> State</strong>
                        <p class="text-muted"><?=$sellerInfo['state'];?></p>
                        <hr> 
                        <strong><i class="fa fa-shopping-cart"></i> Total Orders</strong>
                        <p class="text-muted"><?=$total_orders;?></p>
                        <hr>
                        <strong><i class="fa fa-star"></i> Total Products</strong>
                        <p class="text-muted"><?=$total_products;?></p>
                        </span>
                    </div>             
                 </div>
              </div>
            <div class="tab-pane" id="2">
              <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap.css">  
                <section class="content">                   
                    <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped ">
                      <thead>
                          <tr>
                            <th>Order Id</th>
                            <th>Customer</th>
                            <th>Total Paid</th>
                            <th>Payment</th>                                                            
                            <th>Status</th>
                            <th>Order Date</th>                                                                          
                          </tr>
                      </thead>
                      <tbody>                            
                      <?php foreach($orders as $row): ?>                  
                          <tr>
                            <td><?= $row->id; ?></td>
                            <td><?= $row->customer_name; ?></td>
                            <td><i class="fa fa-inr"></i>&nbsp;<?= $row->total_paid; ?></td>                          
                            <td><?= $row->payment; ?></td>
                            <td><?= $row->order_status; ?></td>
                             <td><?= Date('m/d/Y',strtotime($row->order_date)); ?></td>                           
                          </tr>
                      <?php endforeach; ?>
                      </tbody>                          
                    </table>
                    </div>
                  <!-- /.box-body -->
                </section>  

                <!-- DataTables -->
                <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
                <script src="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
                <script>
                     $(function () {
                        $("#example1").DataTable({
                          "lengthMenu": [10, 25],
                        });
                      });
                </script>      

            </div>

            <div class="tab-pane" id="3">           
               <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap.css">  
                <section class="content">             
                <div class="box-body table-responsive">
                  <table id="example2" class="table table-bordered table-striped ">
                  <thead>
                    <tr>
                      <th>Date</th>
                      <th>Product Name</th>
                      <th>Quantity</th>
                    </tr>
                  </thead>
                  <tbody>                     
                      <?php foreach($products as $row): ?>                     
                      <tr>                     
                          <td><?= Date('m/d/Y',strtotime($row->product_date)); ?></td>
                          <td><?= $row->product_name; ?></td>
                          <td><?= $row->quantity; ?></td>                     
                      </tr>
                      <?php endforeach; ?>
                  </tbody>                    
                  </table>
                </div>
                <!-- /.box-body -->
                </section>  

                <!-- DataTables -->
                <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
                <script src="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
                <script>
                $(function () {
                  $("#example2").DataTable({
                    "lengthMenu": [10, 25],
                  });
                  });
                </script>        

            </div>              
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
 <script>
    $('#Sellers').addClass('active');
 </script>
