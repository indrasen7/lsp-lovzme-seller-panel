<?php

	class Sellers_model extends CI_Model
	{
		//Get the seller name form database table
		public function getSeller($seller_customer_id)
		{
			$this->db->where('seller_customer_id',$seller_customer_id);
			$this->db->select('CONCAT(seller_firstname," "'.' , seller_lastname) AS name');
			$this->db->from('ps_wk_mp_seller');
			$query = $this->db->get();
			return $query->result()[0]->name;
		}

		//Get the seller gender form database table
		public function getGender($id_customer)
		{
			$this->db->where('id_customer',$id_customer);
			$this->db->select('id_gender');
			$this->db->from('ps_customer');
			$query = $this->db->get();
			return $query->result()[0]->id_gender;
		}

		//Get the SellerInfo from database 
		public function getsellerInfo($seller_customer_id)
		{
			$SQL = "SELECT DISTINCT 
			             wms.seller_customer_id AS id,
			             wms.business_email AS email,
			             wms.active,
			             wms.phone AS phone,
			             wms.city AS city,
						 sl.state AS state,
						 sl.address_1 AS address_1,
						 sl.address_2 AS address_1,
						 wms.address AS address_2
				    FROM 
						 ps_wk_mp_seller wms

				    LEFT JOIN ps_lovzmevendor_list sl ON 
				         wms.seller_customer_id = sl.seller_id
				    WHERE
						 wms.seller_customer_id =".$seller_customer_id." ";
			
			$query = $this->db->query($SQL);
		
			$data = $query->row_array();
			return $data;
		}

		//Get the total products of seller from database
		public function getTotalproduct($seller_customer_id)
		{
			$sql = "SELECT
			               SUM(soh.quantity) AS total_products
						
					FROM
						 ps_wk_mp_seller_order_detail soh

					WHERE soh.seller_customer_id = ".$seller_customer_id."
					";
            	
			 $query = $this->db->query($sql);		
			 $data = $query->result();
			 return $data[0]->total_products;

		}

		//Get the total order from database
		public function getTotalorder($seller_customer_id)
		{
			$sql = "SELECT
			               COUNT(soh.id_mp_order_detail) AS total_orders
						
					FROM
						 ps_wk_mp_seller_order_detail soh

					WHERE soh.seller_customer_id = ".$seller_customer_id." ";
             
             $query = $this->db->query($sql);		
			 $data = $query->result();
			 return $data[0]->total_orders;
		}

		//Get the seller order table form database table
		public function getSellerTable($seller_customer_id)
		{
			$SQL = "SELECT DISTINCT
			             o.id_order AS id,
			             o.reference AS reference,
			             CONCAT(cu.firstname,' ', cu.lastname) AS customer_name,
			             ROUND(o.total_paid ,2)AS total_paid,
			             osl.name AS order_status,
			             REPLACE(SUBSTRING(o.payment,1,23),'|','') AS payment,
						 DATE(o.delivery_date) AS order_date
					FROM
						ps_orders o
					LEFT JOIN ps_wk_mp_seller_order_history soh ON 
					     o.id_order = soh.id_order
                    LEFT JOIN ps_customer cu ON 
                     	 o.id_customer = cu.id_customer
					LEFT JOIN ps_order_state_lang osl ON  
						 o.current_state = osl.id_order_state 
					LEFT JOIN ps_wk_mp_seller wms ON 
						 wms.id_seller = soh.id_seller
			        WHERE 
			             wms.seller_customer_id = ".$seller_customer_id." ";
		
            return $this->db->query($SQL)->result(); 			
		}
		
		//Get the seller product table form database table
		public function getSellerProductTable($seller_customer_id)
		{
			$SQL = "SELECT DISTINCT
			             DATE(sod.date_add) AS product_date, 
			             sod.product_name , 
			             sod.quantity
					FROM
						ps_wk_mp_seller_order_detail sod
					
			        WHERE 
			             sod.seller_customer_id = ".$seller_customer_id." ";
		
            return $this->db->query($SQL)->result(); 
			
		}
		
	}

?>