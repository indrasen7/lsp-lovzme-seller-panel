
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><i class="fa fa-shopping-cart"></i>
        Shop
         <small>Seller</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('Dashboard');?>"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Shop</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
                <?php 
                    if ($seller_gender==1) 
                    {
                      echo ' <img  class="profile-user-img img-responsive img-circle" style="margin: 0 auto;width:110px;padding:3px;border:3px solid #d2d6de;" src=" '.base_url().'assets/dist/img/Shop-icon.png"/>';
                    } 
                    else 
                    {
                      echo ' <img  class="profile-user-img img-responsive img-circle" style="margin: 0 auto;width:110px;padding:3px;border:3px solid #d2d6de;" src=" '.base_url().'assets/dist/img/girl.png"/>';
                    }
                  ?> 
              <h4 class="profile-username text-center"><?=$seller_name;?></h4>
              <h5 class="profile-username text-center">Active</h5>
              <h4  class="profile-username text-center">
                      <?php if($sellerInfo['active']==1)
                        {
                            echo "<span class='glyphicon glyphicon-ok icon-success'style='font-size:15px;color:green'></span>";
                        }
                        else
                        {
                            echo "<span class='glyphicon glyphicon-remove icon-remove'style='font-size:15px;color:red'></span>";
                        } 
                      ?> 
              </h4>  
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Contact Details</h3>
            </div>
            <div class="box-body">
              <strong><i class="fa fa-at margin-r-5"></i> Email</strong>
              <p class="text-muted">
              <a href="mailto:<?=$sellerInfo['email']?>"> <?=$sellerInfo['email'];?> </a>
              </p>
              <hr>
              <strong><i class="fa fa-mobile"></i> Phone</strong>
              <p class="text-muted"><?=$sellerInfo['phone'];?> </p>
              <hr>
              <strong><i class="fa fa-location-arrow margin-r-5"></i> City</strong>
              <p class="text-muted"><?=$sellerInfo['city'];?></p>
              <hr>
              <strong><i class="fa fa-map-marker margin-r-5"></i> State</strong>
              <p class="text-muted"><?=$sellerInfo['state'];?></p>
              <hr> 
              <strong><i class="fa fa-map-marker margin-r-5"></i> Address</strong>                       
              <p class="text-muted"><?=$sellerInfo['address_1'];?> </p>
              <p class="text-muted"><?=$sellerInfo['address_2'];?> </p>
              <hr>
              <strong><i class="fa fa-star"></i> Rating</strong>
              <span class="pull-right-container">
              <span class="label label-primary pull-right">4+</span>
            </span>              
            </div>
            <!-- /.box-body -->
           
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#settings" data-toggle="tab">Additional Details</a></li>
              <li><a href="#2" data-toggle="tab">Products</a></li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="settings">
                <div class="box-body">
                   <div class="col-md-6">
                        <strong><i class="fa fa-star"></i> Number of Products</strong>
                        <p class="text-muted"><?=$shop_products;?></p>
                        </span>
                    </div>             
                 </div>
              </div>
            <div class="tab-pane" id="2">
               <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap.css">  
                <section class="content">                   
                    <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped ">
                      <thead>
                          <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Total</th>                                                            
                            <th>Quantity</th>
                            <th>Status</th>                                                                          
                          </tr>
                      </thead>
                      <tbody>
                      <?php foreach($products as $row): ?>                                 
                        <tr>
                          <td><?= $row->id; ?></td>
                          <td><?= $row->name; ?></td>
                          <td><i class="fa fa-inr"></i>&nbsp;<?= $row->price; ?></td>                          
                          <td><?= $row->quantity;?></td>
                          <td><?php if($row->status)
                                {
                                  echo "<center><span style='background-color:#00a65ac2;border-color:#4cbb6c;border-radius:3px;color:#fff;font-size:12px;padding:3px 5px;'>Approved</span></center>";
                                }
                                else
                                {
                                  echo "<span class='glyphicon glyphicon-remove icon-remove'style='font-size:15px;color:red'></span>";
                                } 
                              ?>                             
                           </td>
                        </tr>
                      <?php endforeach; ?>
                      </tbody>                          
                    </table>
                    </div>
                  <!-- /.box-body -->
                   <div class="box-footer clearfix">
                      <a href="<?php echo base_url('Products');?>" class="btn btn-sm bg-purple btn-flat pull-right">View All Products</a>
                    </div> 
                </section>  

                 <!-- DataTables -->
                <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
                <script src="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
                <script>
                     $(function () {
                        $("#example1").DataTable({
                          "lengthMenu": [10, 25],
                        });
                      });
                </script>  
                  
            </div>
            
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
 <script>
    $('#Shop').addClass('active');
 </script>
