<?php 
     defined('BASEPATH') OR exit('No direct script access allowed');
      ini_set('max_execution_time', 0); 
      // ini_set('memory_limit','2048M');
	 class Shop extends MY_Controller
	 {
	 	public function __construct()
	 	{
	 		 parent::__construct();
	 		 $this->load->model('Shop_model');
	 		 $this->load->library('pagination'); // loaded codeigniter pagination liberary
	 	}
	 	
	 	public function index()
	 	{
	 		$seller_customer_id = $this->session->userdata('seller_customer_id');
	 		$id_seller          = $this->session->userdata('id_seller');

	 		$data['title']          =  'LOVZme : Shop';
	 		$data['view']           =  'Shop';
	 		$data['sellerInfo']     =   $this->Shop_model->getSellerInfo($seller_customer_id);
	 		$data['seller_name']    =   $this->Shop_model->getSeller($seller_customer_id);
	 		$data['seller_gender']  =   $this->Shop_model->getGender($seller_customer_id);
	 		$data['shop_products']  =   $this->Shop_model->getProducts($id_seller);
	 		$data['products']       =   $this->Shop_model->getSellerProductsTable($id_seller);

	 		$this->load->view('layout', $data);   
	 	}
	 	
	 }