<?php 

    class Shop_model  extends CI_Model
   {
	    //Get the seller name form database table
		public function getSeller($seller_customer_id)
		{
			$this->db->where('seller_customer_id',$seller_customer_id);
			$this->db->select('CONCAT(seller_firstname," "'.' , seller_lastname) AS name');
			$this->db->from('ps_wk_mp_seller');
			$query = $this->db->get();
			return $query->result()[0]->name;
		}

		//Get the seller gender form database table
		public function getGender($id_customer)
		{
			$this->db->where('id_customer',$id_customer);
			$this->db->select('id_gender');
			$this->db->from('ps_customer');
			$query = $this->db->get();
			return $query->result()[0]->id_gender;
		}

		//Get the seller Info form database table
		public function getSellerInfo($seller_customer_id)
		{
			$SQL =  "SELECT DISTINCT 
			             wms.seller_customer_id AS id,
			             wms.business_email AS email,
			             wms.active,
			             wms.phone AS phone,
			             wms.city AS city,
						 sl.state AS state,
						 sl.address_1 AS address_1,
						 sl.address_2 AS address_1,
						 wms.address AS address_2
				    FROM 
						 ps_wk_mp_seller wms

				    LEFT JOIN ps_lovzmevendor_list sl ON 
				         wms.seller_customer_id = sl.seller_id
				    WHERE
						 wms.seller_customer_id =".$seller_customer_id." ";
			
			$query = $this->db->query($SQL);		
			$data = $query->row_array();
			return $data;
		}

		//Get the seller product form database table
		public function getProducts($id_seller)
		{
			$SQL = "SELECT 
			             COUNT(sp.id_mp_product) AS shop_products
			        FROM 
			             ps_wk_mp_seller_product sp
                    WHERE 
                         sp.id_seller =".$id_seller." ";

            $query = $this->db->query($SQL);		
			return $query->result()[0]->shop_products;
						
		}
		
		//Get the seller Products table form database table
		public function getSellerProductsTable($id_seller)
		{
			$SQL = "SELECT  DISTINCT                 
						pl.id_product AS id,	            
	                    pl.name  AS name,
	                    ROUND(p.price ,0)AS price,
						wsp.quantity AS quantity, 
						p.active AS status
                   
                FROM  ps_wk_mp_seller_product wsp
                
                LEFT JOIN ps_product p ON 
					  p.id_product = wsp.id_ps_product
                         
				LEFT JOIN ps_product_lang pl ON 
                      wsp.id_ps_product = pl.id_product	
                         
                LEFT JOIN ps_image_shop image_shop ON 
                      (image_shop.`id_product` = p.`id_product` 
                AND    image_shop.`cover` = 1 
                AND    image_shop.`id_shop` = 1)

                LEFT JOIN ps_image pi ON 
                      (p.`id_product` = pi.`id_product`) 

                LEFT JOIN ps_configuration conf ON 
                      (conf.`name` = 'PS_SHOP_DOMAIN')
                WHERE 
                      pi.cover = 1
			    AND
                      wsp.id_seller = ".$id_seller."
			    AND   description is not NULL";
		
            return $this->db->query($SQL)->result(); 			
		}
    }
?>
