<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Dashboard extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Dashboard_model');
    }

    public function index()
    {
            $seller_customer_id = $this->session->userdata('seller_customer_id');
                 
            $data['title']             = 'LOVZme : Dashboard';
            $data['view']              = 'Dashboard';          
            $data['customer_name']     = $this->Dashboard_model->get_customer_name($seller_customer_id)->customer_name;
            $data['total_orders']      = $this->Dashboard_model->get_order_count($seller_customer_id)->total_orders;
            $data['seller_amount']     = round($this->Dashboard_model->get_seller_amount($seller_customer_id)->seller_amount);
            $data['total_products']    = $this->Dashboard_model->get_total_product($seller_customer_id)->total_products;
            $data['orders']            = $this->Dashboard_model->get_order_details($seller_customer_id);
            $data['seller_name']       = $this->Dashboard_model->getSeller($seller_customer_id);
            $data['seller_gender']     = $this->Dashboard_model->getGender($seller_customer_id);
                                                     
            $this->load->view('layout', $data);
    }

}
