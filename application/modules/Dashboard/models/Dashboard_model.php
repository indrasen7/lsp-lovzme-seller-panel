<?php
	class Dashboard_model extends CI_Model
	{		
		public function __construct()
		{
			parent::__construct();
		}

		//Get the seller name form database table
		public function getSeller($seller_customer_id)
		{
			$this->db->where('seller_customer_id',$seller_customer_id);
			$this->db->select('CONCAT(seller_firstname," "'.' , seller_lastname) AS name');
			$this->db->from('ps_wk_mp_seller');
			$query = $this->db->get();
			return $query->result()[0]->name;
		}

		//Get the seller gender form database table
		public function getGender($id_customer)
		{
			$this->db->where('id_customer',$id_customer);
			$this->db->select('id_gender');
			$this->db->from('ps_customer');
			$query = $this->db->get();
			return $query->result()[0]->id_gender;
		}

		//Get the Seller name from database   				
		public function get_customer_name($seller_customer_id)
		{
			$sql ="SELECT DISTINCT
			            
			             COUNT(CONCAT(cu.firstname,' ', cu.lastname)) AS customer_name
			            
					FROM
						ps_orders o
					LEFT JOIN ps_wk_mp_seller_order_history soh ON 
					     o.id_order = soh.id_order
                    LEFT JOIN ps_customer cu ON 
                     	 o.id_customer = cu.id_customer
					LEFT JOIN ps_order_state_lang osl ON  
						 o.current_state = osl.id_order_state 
					LEFT JOIN ps_wk_mp_seller wms ON 
						 wms.id_seller = soh.id_seller
			        WHERE 
			             wms.seller_customer_id = ".$seller_customer_id." ";
			 
			 $query = $this->db->query($sql);		
			 $data = $query->result();
			 return $data[0];
		}

        //Get total order to display front page in dashboard
		public function get_order_count($seller_customer_id)
		{
			$sql = "SELECT
			               COUNT(soh.id_mp_order_detail) AS total_orders
						
					FROM
						 ps_wk_mp_seller_order_detail soh

					WHERE soh.seller_customer_id = ".$seller_customer_id." ";
             
             $query = $this->db->query($sql);		
			 $data = $query->result();
			 return $data[0];
		}
        
        //Get seller total Earn from database
		public function get_seller_amount($seller_customer_id)
		{
			$sql = "SELECT
                           SUM(soh.seller_amount) AS seller_amount
                    FROM
                           ps_wk_mp_seller_order_detail soh
                    WHERE  
                           soh.seller_customer_id = ".$seller_customer_id." ";

			 $query = $this->db->query($sql);		
			 $data = $query->result();
			 return $data[0];
		}
        
        //Get total product to display front page in dashboard
		public function get_total_product($seller_customer_id)
		{
			$sql = "SELECT
			               SUM(soh.quantity) AS total_products
						
					FROM
						 ps_wk_mp_seller_order_detail soh

					WHERE soh.seller_customer_id = ".$seller_customer_id."
					";
            	
			 $query = $this->db->query($sql);		
			 $data = $query->result();
			 return $data[0];

		}

        //Get the seller order table form database table
		public function get_order_details($seller_customer_id)
		{

			$SQL = "SELECT DISTINCT
			             o.id_order AS id,
			             o.reference AS reference,
			             CONCAT(cu.firstname,' ', cu.lastname) AS customer_name,
			             ROUND(o.total_paid ,2)AS total_paid,
			             osl.name AS order_status,
			             REPLACE(SUBSTRING(o.payment,1,23),'|','') AS payment,
						 DATE(o.delivery_date) AS order_date
					FROM
						ps_orders o
					LEFT JOIN ps_wk_mp_seller_order_history soh ON 
					     o.id_order = soh.id_order
                    LEFT JOIN ps_customer cu ON 
                     	 o.id_customer = cu.id_customer
					LEFT JOIN ps_order_state_lang osl ON  
						 o.current_state = osl.id_order_state 
					LEFT JOIN ps_wk_mp_seller wms ON 
						 wms.id_seller = soh.id_seller
			        WHERE 
			             wms.seller_customer_id = ".$seller_customer_id."
			        ORDER BY o.id_order DESC 
			        LIMIT 0,12";
		
            return $this->db->query($SQL)->result(); 
			
		}

		public function dbtest()
		{
			$query = $this->db->select('firstname')->get('ps_customer');
			$result = $query->row_array();
			$result =$this->db2->count_all_results('ps_orders');
			return $result;
		}
		
	}

?>
