<!-- Main content -->
    <section class="content-header">
      <h1><i class="fa fa-home"></i>
        Dashboard
        <small>Seller</small>
      </h1>
      <ol class="breadcrumb">
        <li class="active">Dashboard</li>
      </ol>     
    </section>
    <section class="content">
	<div class="callout callout-info">
        <h4>Hi , <?=$seller_name;?>! <img src="<?php echo base_url();?>assets/dist/img/Wavings.png" style="vertical-align:sub";/> <em>We're glad you're here.</em></h4>
        <p>From your Dashboard, You have the ability to view of your recent order activity and update your product information.</p>
      </div>
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua" 
                  style="background-color: #f39f86;
                         background-image: linear-gradient(315deg, #f9d976 0%,  #ffb29b 74%);">
            <i class="ion ion-ios-people-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">HAPPY CUSTOMERS </span>
              <span class="info-box-number"><?= $customer_name; ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-shopping-cart"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">TOTAL ORDERS</span>
              <span class="info-box-number"><?=$total_orders;?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-money" aria-hidden="true"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">TOTAL EARN</span>
              <span class="info-box-number"><?= $seller_amount; ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-list-alt"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">PRODUCT LISTED</span>
              <span class="info-box-number"><?= $total_products; ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
           
          <!-- TABLE: LATEST ORDERS -->
          <div class="box">
             <div class="box-header">
                <h3 class="box-title">Recent Orders</h3>
             </div>                                         
              <div class="box-body table-responsive"> 
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>OrderID</th>
                    <th>Reference</th>
                    <th>Customer</th>
                    <th>Total</th>
                    <th>Status</th>
                    <th>Payment</th>
                    <th>O-Date</th>                   
                  </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($orders as $row){?>
                    <tr>
                      <td><?= $row->id;?></td>
                      <td><span class="label label-danger"><?= $row->reference;?><span></td>
                      <td><?= $row->customer_name;?></td>
                      <td><?= $row->total_paid;?></td>
                      <td><?= $row->order_status;?></td>
                      <td><?= $row->payment;?></td>
                      <td><?= $row->order_date;?></td>
                    </tr>
                    <?php }?>
                  </tbody>
                </table>             
              </div>
			      <!-- /.box-body -->
			      <div class="box-footer clearfix">
              <a href="<?php echo base_url('Transaction');?>" class="btn btn-sm btn-info btn-flat pull-right">View All Orders</a>
            </div> 
            <!-- /.box-footer -->
            <!-- /.box-body -->
            </div>
            <!-- /.box -->                    
            <!-- DataTables -->
            <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
            <script src="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
            
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->          
      </div>
      <!-- /.row -->    
    </section>
    <!-- /.content --> 
<script>
  $("#Dashboard").addClass('active');
</script>
