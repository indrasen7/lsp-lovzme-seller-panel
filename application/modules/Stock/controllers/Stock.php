<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stock extends MY_Controller 
{

	  public function __construct()
	  {
		        parent::__construct();
            $this->load->model('Stock_model');
		
	  }

	  public function index()
    {
            $seller_customer_id = $this->session->userdata('seller_customer_id');
                 
            $data['title']             = 'LOVZme : Stock';
            $data['view']              = 'Stock';          
            $data['customer_name']     = $this->Stock_model->get_customer_name($seller_customer_id)->customer_name;
            $data['seller_name']       = $this->Stock_model->getSeller($seller_customer_id);
            $data['seller_gender']     = $this->Stock_model->getGender($seller_customer_id);
                                                     
            $this->load->view('layout', $data);
    }
    
    //Update stock generate CSV file
    public function stockupdate()
    {

           $filename = 'Stock_'.date('Y-m-d').'.csv'; 
           header('Content-Description: File Transfer'); 
           header('Content-Type: application/csv');
           header('Content-Type: application/force-download; charset=UTF-8');
           header('Content-Disposition: attachment; filename='.$filename);           
           header('Cache-Control: no-store, no-cache');
           
           $id_customer_seller = $this->session->userdata('seller_customer_id');
           // get data 
           $seller_data = $this->Stock_model->getStockupdate($id_customer_seller);

           // file creation 
           $file = fopen('php://output', 'w');
         
           $header = array("Product ID", "Child Product ID", "Reference", "Stock");

           fputcsv($file, $header);

           foreach ($seller_data as $key=>$line)
           { 
             fputcsv($file,$line); 
           }
           fclose($file); 
           exit; 
    }
    
    //Upload CSV file in database
    public function csvstockimport()   
    { 
      $count = 0;
      $file_data = fopen($_FILES['userfile']['tmp_name'],'r') or die("can't open file");
        while($data = fgetcsv($file_data, 1024, ","))
        {   
            $count++;
            if($count == 1)
            {
                continue;
            }
            if (array(null) !== $data) 
            {
                        $product_parent_id = trim($data[0]);
                        $product_child_id = trim($data[1]);
                        $reference = trim($data[2]);
                        $quantity = trim($data[3]);                       

                        $sql_1 = 'UPDATE ps_stock_available SET quantity="'.$quantity.'" WHERE id_product_attribute="'.$product_child_id.'"';
                        $sql_2 = 'UPDATE ps_stock_available SET quantity = quantity + "'.$quantity.'" WHERE id_product="'.$product_parent_id.'" AND id_product_attribute = 0';
                        $sql_3 = 'UPDATE ps_stock_available SET physical_quantity="'.$quantity.'" WHERE id_product_attribute="'.$product_child_id.'"';
                        $sql_4 = 'UPDATE ps_stock_available SET physical_quantity = physical_quantity + "'.$quantity.'" WHERE id_product="'.$product_parent_id.'" AND id_product_attribute = 0';
                        $sql_5 = 'UPDATE ps_product_attribute SET quantity="'.$quantity.'" WHERE id_product_attribute="'.$product_child_id.'"';
                        $sql_6 = 'UPDATE ps_product SET quantity= quantity + "'.$quantity.'" WHERE id_product="'.$product_parent_id.'"';

                        $this->db->query($sql_1);
                        $this->db->query($sql_2);
                        $this->db->query($sql_3);
                        $this->db->query($sql_4);
                        $this->db->query($sql_5);
                        $this->db->query($sql_6);
            }     
        }
        fclose($file_data) or die("can't close file");
        $data['success']="success";
        return $data;
    }


}

/* End of file Stock.php */
/* Location: ./application/modules/Stock/controllers/Stock.php */