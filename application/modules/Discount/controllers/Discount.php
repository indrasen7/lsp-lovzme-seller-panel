<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Discount extends MY_Controller 
{

	public function __construct()
	{
		        parent::__construct();
            $this->load->model('Discount_model');		
	}

	public function index()
  {
          $seller_customer_id = $this->session->userdata('seller_customer_id');
                 
          $data['title']             = 'LOVZme : Discount';
          $data['view']              = 'Discount';          
          $data['customer_name']     = $this->Discount_model->get_customer_name($seller_customer_id)->customer_name;
          $data['seller_name']       = $this->Discount_model->getSeller($seller_customer_id);
          $data['seller_gender']     = $this->Discount_model->getGender($seller_customer_id);
                                                     
            $this->load->view('layout', $data);
  }
  
  //Generate CSV file and get the csv
  public function discountupdate()
  {
           $filename = 'Discount_'.date('Y-m-d').'.csv'; 
           header('Content-Description: File Transfer'); 
           header('Content-Type: application/csv');
           header('Content-Type: application/force-download; charset=UTF-8');
           header('Content-Disposition: attachment; filename='.$filename);           
           header('Cache-Control: no-store, no-cache');
           
           $id_customer_seller = $this->session->userdata('seller_customer_id');

           // get data 
           $seller_data = $this->Discount_model->getstockupdate($id_customer_seller);

           // file creation 
           $file = fopen('php://output', 'w');
         
           $header = array("Product ID", "Reference(SKU)", "Discount ID", "Selling Price", "MRP(For Reference Only)");

           fputcsv($file, $header);

           foreach ($seller_data as $key=>$line)
           { 
             fputcsv($file,$line); 
           }
           fclose($file); 
           exit; 

  }

  //Upload the CSV file for upadte discount
  public function csvdiscountimport()
  {
      $count = 0;
      $file_data = fopen($_FILES['disfile']['tmp_name'],'r') or die("can't open file");
        while($data = fgetcsv($file_data, 1024, ","))
        {   
            $count++;
            if($count == 1)
            {
                continue;
            }
            if (array(null) !== $data) 
            {
                                 
                  $product_discount_id = trim($data[2]);
                  $check_blank_discount = trim($data[3]);
                    if($check_blank_discount > 0)
                      {
                        $product_discount = (trim($data[4])-trim($data[3]))/trim($data[4]);
                      }
                      else
                      {
                        $product_discount = 0;
                      }

                  $test_1 = 'UPDATE ps_specific_price SET `reduction` = "'.$product_discount.'" WHERE `id_specific_price`="'.$product_discount_id.'"';
                       
                  $this->db->query($test_1);
                      
            }     
        }
        fclose($file_data) or die("can't close file");
        $data['success']="success";
        return $data;
  }

}

/* End of file Discount.php */
/* Location: ./application/modules/Discount/controllers/Discount.php */