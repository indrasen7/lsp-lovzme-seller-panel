<!-- Main content -->
    <section class="content-header">
      <h1><i class="fa fa-align-center"></i>
        Discount
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>Dashboard"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Discount</li>
      </ol>     
    </section>
    <section class="content">            
                <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs">
                    <li class="active"><a href="#settings" data-toggle="tab">Discount Details</a></li>                   
                  </ul>
                  <div class="tab-content">
                    <div class="active tab-pane" id="settings">
                      <div class="box-body">
                          <div class="col-md-3">                            
                              <h4>Export Product(.csv) File : </h4>               
                          </div>
                          <div class="col-md-2">
                             <a href="<?= base_url() ?>Discount/discountupdate" class="btn btn-primary">Export Current Discount</a>
                          </div>

                          <form method="post" id="import_csv_discount" enctype="multipart/form-data">
                              <div class="col-md-3">                            
                                  <h4>Upload Discount(.csv) File : </h4>                   
                              </div>
                              <div class="col-md-3">
                                   <input type="file"  name="disfile" id="disfile" required accept=".csv"><br>
                                     <button type="submit" name="import_csv_discount" class="btn btn-primary" id="import_csv_discount_btn">Update Discount</button>                                              
                              </div>
                              <div class="col-md-1">
                                 <div id="loading"></div>      
                              </div>
                          </form>
                       </div>
                    </div>                           
                  </div>                 
                </div>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
                <script>
                    $(document).ready(function(){
                        $('#import_csv_discount').on('submit', function(event){
                        event.preventDefault();
                        var image = "<?php echo base_url(). 'assets/images/Spinner.gif'; ?>";
                        $('#loading').html('<img src='+image+' width="80" height="80" align="center"/>');
                        $.ajax({
                          url:"<?php echo base_url(); ?>Discount/csvdiscountimport",
                          method:"POST",
                          data:new FormData(this),
                          contentType:false,
                          cache:false,
                          processData:false,
                          beforeSend:function(){
                            $('#import_csv_discount_btn').html('Updating...Please wait');
                          },
                          success:function(data)
                          {
                            $('#loading').html("").hide();
                            $('#import_csv_discount')[0].reset();
                            $('#import_csv_discount_btn').attr('disabled', false);
                            $('#import_csv_discount_btn').html('File Update Done Successfully');
                          }
                        })
                      });                      
                    });
                </script>                             
          </div>
        </div>  
      </div>
    </section>
<script>
  $("#Discount").addClass('active');
</script>
