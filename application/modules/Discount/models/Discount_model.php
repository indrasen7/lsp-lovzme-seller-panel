<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Discount_model extends CI_Model 
{
	public function __construct()
		{
			parent::__construct();
		}

		//Get the seller name form database table
		public function getSeller($seller_customer_id)
		{
			$this->db->where('seller_customer_id',$seller_customer_id);
			$this->db->select('CONCAT(seller_firstname," "'.' , seller_lastname) AS name');
			$this->db->from('ps_wk_mp_seller');
			$query = $this->db->get();
			return $query->result()[0]->name;
		}

		//Get the seller gender form database table
		public function getGender($id_customer)
		{
			$this->db->where('id_customer',$id_customer);
			$this->db->select('id_gender');
			$this->db->from('ps_customer');
			$query = $this->db->get();
			return $query->result()[0]->id_gender;
		}

		//Get the Seller name from database   				
		public function get_customer_name($seller_customer_id)
		{
			$sql ="SELECT DISTINCT
			            
			             COUNT(CONCAT(cu.firstname,' ', cu.lastname)) AS customer_name
			            
					FROM
						ps_orders o
					LEFT JOIN ps_wk_mp_seller_order_history soh ON 
					     o.id_order = soh.id_order
                    LEFT JOIN ps_customer cu ON 
                     	 o.id_customer = cu.id_customer
					LEFT JOIN ps_order_state_lang osl ON  
						 o.current_state = osl.id_order_state 
					LEFT JOIN ps_wk_mp_seller wms ON 
						 wms.id_seller = soh.id_seller
			        WHERE 
			             wms.seller_customer_id = ".$seller_customer_id." ";
			 
			 $query = $this->db->query($sql);		
			 $data = $query->result();
			 return $data[0];
		}
        
        //Get the stock data 
		public function getstockupdate($id_customer_seller)
		{
			$SQL = "SELECT
					        ANY_VALUE(pspa.`id_product`),
					        mkp.`reference`, 
					        ANY_VALUE(psspr.`id_specific_price`), 
					        ANY_VALUE(round((1-psspr.`reduction`)*(mkp.`price`*(MAX(pspt.`rate`+100)/100)))),
					        round(mkp.`price`*(MAX(pspt.`rate`+100)/100),0)
					FROM 
					        `ps_product_attribute` pspa
					        LEFT JOIN `ps_wk_mp_seller_product` wksp ON pspa.`id_product` = wksp.`id_ps_product` 
					        LEFT JOIN `ps_wk_mp_seller` wks ON wks.`id_seller` = wksp.`id_seller` 
					        LEFT JOIN `ps_product` mkp ON pspa.`id_product` = mkp.`id_product`
					        LEFT JOIN `ps_specific_price` psspr ON pspa.`id_product` = psspr.`id_product`
					        LEFT JOIN `ps_tax_rule` psptr ON mkp.`id_tax_rules_group` = psptr.`id_tax_rules_group` 
					        LEFT JOIN `ps_tax` pspt ON psptr.`id_tax` = pspt.`id_tax`
					        WHERE wks.`seller_customer_id` = ".$id_customer_seller." GROUP BY pspa.`id_product`";
			 //print_r($sql);
	        $query = $this->db->query($SQL);
	        return $query->result_array();
		}

	

}

/* End of file Discount_model.php */
/* Location: ./application/modules/Discount/models/Discount_model.php */